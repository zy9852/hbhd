(function($, window, document, undefined) {
    'use strict';

    //共用弹窗框
    $('.exmodal').click(function (e) {
        //alert('aaa');
        var $this = $(this);
        var title = $this.data('title');
        var sure  = $this.data('sure');
        var none  = $this.data('none');
        var target  = $this.data('target').substr(1);
        if(title){
            $('.modal-body p').text(title);
        }
        if(sure){
            $('.modal-footer .sure').text(sure);
        }
        if(none){
            $('.modal-footer .none').text(none);
        }
        if(target){
            $('.exalert').attr('id',target);
        }

        $(".modal-body span").html($this.find('input').clone());
        $('.modal-footer,.modal-footer .sure,.modal-footer .none').show();
    });

})(jQuery, window, document);
