var EcommerceOrders = function () {
    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            language:'zh-CN',
            autoclose: true
        });

        // $(".date-picker").datetimepicker({
        //     autoclose: true,
        //     isRTL: App.isRTL(),
        //     language:'zh-CN',
        //     format: "yyyy-mm-dd hh:ii:ss"
        // });

    }
    //商品列表
    var productlist = function () {
        var grid    = new Datatable();
        var ajaxUrl       = $('#ajaxUrl').val();
        var ajaxUrlAction = $('#ajaxUrlAction').val();
        //alert(ajaxUrl);
        grid.init({
            src: $("#datatable_products"),
            onSuccess: function (grid) {
                //alert("asdsdf");
                //console.log(grid);
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            loadingMessage: '加载中...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "全部"] // change per page values here
                ],
                "pageLength": 50, // default record count per page
                "ajax": {
                    "url": ajaxUrl, // ajax source
                    "type": "POST"
                },
                "order": [
                    [0, ""]
                ]
            }
        });
        // handle group actionsubmit button click
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                //alert(grid.getSelectedRows());
                $actioname = action.val();
                if( $actioname== 'pingou' ){
                    bootbox.confirm("该选项将会把拼购商品导入到数据库中，点击确认继续执行?", function(result) {
                        if(result){
                            $.ajax({
                                url: ajaxUrlAction,
                                type:'POST',
                                data: "ids="+grid.getSelectedRows()+'&actionname='+action.val(),
                                beforeSend:function(XMLHttpRequest){
                                    App.alert({
                                        type: 'info',
                                        icon: 'warning',
                                        message: '导入数据需要较长时间，请勿进行任何操作，正在处理中....',
                                        container: grid.getTableWrapper(),
                                        place: 'prepend'
                                    });
                                    return true;
                                },
                                success: function(data){

                                    if(data == 1){
                                        App.alert({
                                            type: 'success',
                                            icon: 'warning',
                                            message: '处理完成，您可以点击<a href="#代言联盟">这里</a>查看',
                                            container: grid.getTableWrapper(),
                                            place: 'prepend'
                                        });
                                        //window.location.reload();
                                    }
                                }
                            });
                        }
                    });
                }else if( $actioname== 'days' ){
                    bootbox.confirm("该选项将会把商品导入到优品数据库中，点击确认继续执行?", function(result) {
                        if(result){
                            $.ajax({
                                url: ajaxUrlAction,
                                type:'POST',
                                data: "ids="+grid.getSelectedRows()+'&actionname='+action.val(),
                                beforeSend:function(XMLHttpRequest){
                                    App.alert({
                                        type: 'info',
                                        icon: 'warning',
                                        message: '请勿进行任何操作，正在处理中',
                                        container: grid.getTableWrapper(),
                                        place: 'prepend'
                                    });
                                    return true;
                                },
                                success: function(data){

                                    if(data == 1){
                                        App.alert({
                                            type: 'success',
                                            icon: 'warning',
                                            message: '处理完成，您可以点击<a href="#代言联盟">这里</a>查看',
                                            container: grid.getTableWrapper(),
                                            place: 'prepend'
                                        });
                                        //window.location.reload();
                                    }
                                }
                            });
                        }
                    });
                }else if( $actioname== 'freeship' ){
                    bootbox.confirm("该选项将会把商品导入到包邮库中，点击确认继续执行?", function(result) {
                        if(result){
                            $.ajax({
                                url: ajaxUrlAction,
                                type:'POST',
                                data: "ids="+grid.getSelectedRows()+'&actionname='+action.val(),
                                beforeSend:function(XMLHttpRequest){
                                    App.alert({
                                        type: 'info',
                                        icon: 'warning',
                                        message: '请勿进行任何操作，正在处理中',
                                        container: grid.getTableWrapper(),
                                        place: 'prepend'
                                    });
                                    return true;
                                },
                                success: function(data){

                                    if(data == 1){
                                        App.alert({
                                            type: 'success',
                                            icon: 'warning',
                                            message: '处理完成，您可以点击<a href="#代言联盟">这里</a>查看',
                                            container: grid.getTableWrapper(),
                                            place: 'prepend'
                                        });
                                        //window.location.reload();
                                    }
                                }
                            });
                        }
                    });
                }else{

                }
                // grid.setAjaxParam("customActionType", "group_action");
                // grid.setAjaxParam("customActionName", action.val());
                // grid.setAjaxParam("id", grid.getSelectedRows());
                // grid.getDataTable().ajax.reload();
                // grid.clearAjaxParams();
                //return false;
            } else if (action.val() == "") {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: '请先选择要批量处理的数据和操作方式',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: '未找到记录',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
        });
        $(document).on('click', '.filter-submit', function(e) {
            //alert('sss');
            e.preventDefault();
            //grid.setAjaxParam("action", tableOptions.filterApplyAction);
            // get all typeable inputs
            $('textarea.form-filter, select.form-filter, input.form-filter:not([type="radio"],[type="checkbox"])', "#productfilter").each(function() {
                grid.setAjaxParam($(this).attr("name"), $(this).val());
            });
            // get all checkboxes
            $('input.form-filter[type="checkbox"]:checked', "#productfilter").each(function() {
                grid.addAjaxParam($(this).attr("name"), $(this).val());
            });
            // get all radio buttons
            $('input.form-filter[type="radio"]:checked', "#productfilter").each(function() {
                grid.setAjaxParam($(this).attr("name"), $(this).val());
            });
            grid.getDataTable().ajax.reload();
        });
        $(document).on('click', '.filter-cancel', function(e) {
            e.preventDefault();
            $('textarea.form-filter, select.form-filter, input.form-filter', "#productfilter").each(function() {
                $(this).val("");
            });
            $('input.form-filter[type="checkbox"]', "#productfilter").each(function() {
                $(this).attr("checked", false);
            });
            grid.clearAjaxParams();
            grid.getDataTable().ajax.reload();
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            productlist();
        }
    };
}();
jQuery(document).ready(function() {
    EcommerceOrders.init();
});