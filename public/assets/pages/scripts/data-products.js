var EcommerceOrders = function () {
    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            language: 'zh-CN',
            autoclose: true
        });
    }
    //商品列表
    var productlist = function () {
        var grid = new Datatable();
        var ajaxUrl = $('#ajaxUrl').val();
        var ajaxUrlAction = $('#ajaxUrlAction').val();
        //alert(ajaxUrl);
        grid.init({
            src: $("#datatable_products"),
            onSuccess: function (grid) {},
            onError: function (grid) {},
            loadingMessage: '加载中...',
            dataTable: {
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "全部"] // change per page values here
                ],
                "pageLength": 20, // default record count per page
                "ajax": {
                    "url": ajaxUrl, // ajax source
                    "type": "POST"
                },
                "order": [
                    [0, ""]
                ]
            }
        });
        // handle group actionsubmit button click
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                //alert(grid.getSelectedRows());
                $.ajax({
                    url: ajaxUrlAction,
                    type: 'POST',
                    data: "ids=" + grid.getSelectedRows() + '&actionname=' + action.val(),
                    beforeSend: function (XMLHttpRequest) {
                        App.alert({
                            type: 'info',
                            icon: 'warning',
                            message: '请勿进行任何操作，正在处理中',
                            container: grid.getTableWrapper(),
                            place: 'prepend'
                        });
                        return true;
                    },
                    success: function (data) {

                        if (data == 1) {
                            App.alert({
                                type: 'success',
                                icon: 'warning',
                                message: '处理完成，为您刷新页面',
                                container: grid.getTableWrapper(),
                                place: 'prepend'
                            });
                            window.location.reload();
                        }
                    }
                });

            } else if (action.val() == "") {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: '请先选择要批量处理的数据和操作方式',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: '未找到记录',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
        });
        $(document).on('click', '.filter-submit', function (e) {

            e.preventDefault();

            $('textarea.form-filter, select.form-filter, input.form-filter:not([type="radio"],[type="checkbox"])', "#productfilter").each(function () {
                grid.setAjaxParam($(this).attr("name"), $(this).val());
            });
            // get all checkboxes
            $('input.form-filter[type="checkbox"]:checked', "#productfilter").each(function () {
                grid.addAjaxParam($(this).attr("name"), $(this).val());
            });
            // get all radio buttons
            $('input.form-filter[type="radio"]:checked', "#productfilter").each(function () {
                grid.setAjaxParam($(this).attr("name"), $(this).val());
            });
            grid.getDataTable().ajax.reload();
        });
        $(document).on('click', '.filter-cancel', function (e) {
            e.preventDefault();
            $('textarea.form-filter, select.form-filter, input.form-filter', "#productfilter").each(function () {
                $(this).val("");
            });
            $('input.form-filter[type="checkbox"]', "#productfilter").each(function () {
                $(this).attr("checked", false);
            });
            grid.clearAjaxParams();
            grid.getDataTable().ajax.reload();
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            productlist();
        }
    };
}();
jQuery(document).ready(function () {
    EcommerceOrders.init();
});