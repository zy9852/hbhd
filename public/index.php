<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]

// 定义应用目录
define('APP_PATH', __DIR__ . '/../application/');
define('EXTEND_PATH','../extend/');
define('UPLOAD_PATH','https://jd3.kissneck.com');

define('APP_ID','wx34e590df69bb0396');
define('APP_SCR','c62610b43ba0758fddf10aa6261cffe4');

// 加载框架引导文件
require __DIR__ . '/../thinkphp/start.php';
