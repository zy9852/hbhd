<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.kissneck.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 可待科技-磊子 <sxfyxl@126.com>
// +----------------------------------------------------------------------

// 应用公共文件
use think\Session;
use think\Db;

// 检测用户名
function get_checkUser($str)
{
    $member = model('Member');
    $username = $str;
    $name = $member::get(['name' => $username]);
    if ($name) {
        return 'false';
    } else {
        return 'true';
    }
}

// 检测邮箱
function get_checkEmail($str, $auth = "")
{
    $member = model('Member');
    $email = $str;
    $useremail = $member::get(['email' => $email]);
    if (!empty($auth) && $auth !== '0') {
        $data = $member::get(['login_auth' => $auth]);
        if ($data['email'] != $email) {
            return 'false';
        } else {
            return 'true';
        }
    } else {
        if ($useremail) {
            return 'false';
        } else {
            return 'true';
        }
    }
}

// 检测用户权限
function get_checkRole($auth)
{
    $member = model('Member');
    //$data = Db::name('member')->where('login_auth',$auth)->find();
    $data = $member::get(['login_auth' => $auth]);
    if ($data) {
        return $data['role'];
    } else {
        return 0;
    }
}



// 验证码检测
function get_checkcaptcha($str)
{
    $captcha = new \think\captcha\Captcha();
    $code = $str;
//    $code = $captcha->authcode(strtoupper($code));
    $secode = Session::get('verify_captcha');
    if ($secode['verify_code'] != $code) {
        return 'false';
    } else {
        return 'true';
    }
}

//订单状态判断
function get_orderstatus($v)
{
    switch ($v) {
        case '1':
            return '<i class="fa fa-check" aria-hidden="true" title="完成"></i>';
            break;

        default:
            return '<i class="fa fa-times" aria-hidden="true" title="失败"></i>';
            break;
    }
}

//订单支付类型
function get_ordertype($v)
{
    switch ($v) {
        case 'alipay':
            return '支付宝';
            break;

        default:
            return '微信';
            break;
    }
}

//订单状态判断
function get_orderbuytime($v)
{
    if (!empty($v)) {
        return date('Y-m-d H:i:s', $v);
    } else {
        return '暂无';
    }
}

function get_hosturl($u)
{
    $http = parse_url($u, PHP_URL_SCHEME);
    $url = $http . '://' . parse_url($u, PHP_URL_HOST);
    return $url;
}

//用户等级
function get_userrole($v)
{
    switch ($v) {
        case '1':
            return '管理员';
            break;
        case '2':
            return '普通用户';
            break;

        default:
            return '暂无';
            break;
    }
}

//用户状态判断
function get_usertatus($v)
{
    switch ($v) {
        case '1':
            return '<i class="fa fa-check" aria-hidden="true" title="完成"></i>';
            break;

        default:
            return '<i class="fa fa-times" aria-hidden="true" title="失败"></i>';
            break;
    }
}


//申请
function get_status($v)
{
	switch ($v) {
		case '3':
			return '<span class="font-red">未通过</span>';
			break;
		case '2':
			return '<span class="font-green">已通过</span>';
			break;
		case '1':
			return '<span>审核中</span>';
			break;
		default:
			return '<span>审核中</span>';
			break;
	}
}

function getcidcount($v)
{
	return sizeof(explode(',',$v));
}

