<?php

namespace app\admin\controller;

use think\Db;
use think\Session;

class Index extends Base
{
    public function index()
    {
        $active = "dashboard";
	    $this->assign('active', $active);
        return $this->fetch();
    }
}
