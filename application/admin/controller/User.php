<?php

namespace app\admin\controller;

use think\Db;
use think\view;
use think\Session;

class User extends Base
{
    //管理员列表
    public function index()
    {
        $active = "userlist";
        $this->assign('active', $active);
        $option = model('option');
        $logo = $option->alias('o')
            ->where('meta_key', 'web_logo')
            ->join('y_media i', 'o.meta_value=i.id')
            ->value('pic1');
        $title = $option->where('meta_key', 'web_title')->value('meta_value');

        $list = Db::name('admin')->where('is_delete', 1)->select();
        //获取用户权限
        $uid = Session::get('uid');
        $power = Db::name('admin')->where('id', $uid)->value('power');
        // 模板变量赋值
        $this->assign('logo', $logo);
        $this->assign('title', $title);
        $this->assign('list', $list);
        $this->assign('power', $power);
        return $this->fetch();
    }

    //修改
    public function saveUserMsg()
    {
        $data = input('post.');
        if ($data['password'] == "") {
            unset($data['password']);
        } else {
            $data['password'] = md5($data['password']);
        }
        $res = DB::name('admin')->where('id',$data['id'])->update($data);
        if ($res) {
            $msg = ['code' => 200, 'msg' => '编辑成功'];
            echo json_encode($msg);
            exit;
        } else {
            $msg = ['code' => 400, 'msg' => '数据无变化'];
            echo json_encode($msg);
            exit;
        }
    }
}
