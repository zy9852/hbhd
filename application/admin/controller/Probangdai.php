<?php
namespace app\admin\controller;

use think\Session;
use think\Db;
use think\Env;
use think\Request;

//订单列表
class Probangdai extends Base
{
	//订单列表
	public function index()
	{
		$active = "prolistlist";
		$type = input('type');
		return view('product/index', ['active' => $active, 'type' => $type]);
	}

	//列表
	public function list_pro()
	{
		$data = input();
		$type = input('type');
		$limit_start = $data['start'];
		$limit_length = $data['length'];
		$u_name = input('nickname');
		$startTime = input('reg_start');
		$endTime = input('reg_end');


		$where = '';
		$value = [];

		if (!empty($u_name)) {
			$where .= ' AND nickname = :nickname';
			$value['nickname'] = trim($u_name);
		}

		if (!empty($startTime) && !empty($endTime)) {
			$where .= ' AND createTime > :aa';
			$value['aa'] = strtotime($startTime);
			$where .= ' AND createTime <= :bb';
			$value['bb'] = strtotime($endTime);
		}


		if (empty($type)) {

			$list = DB::name('food')->where('1=1' . $where . '', $value)->where('pro_status', 1)->limit($limit_start, $limit_length)->order('createTime', 'desc')->select();
			$total = DB::name('food')->where('1=1' . $where . '', $value)->where('pro_status', 1)->count();
		}
		//打车出行
		if ($type == 1) {
			$list = DB::name('trip')->where('1=1' . $where . '', $value)->where('pro_status', 1)->limit($limit_start, $limit_length)->order('createTime', 'desc')->select();
			$total = DB::name('trip')->where('1=1' . $where . '', $value)->where('pro_status', 1)->count();
		}
		//寄取快递
		if (($type == 2)) {

			$list = DB::name('express')->where('1=1' . $where . '', $value)->where('pro_status', 1)->limit($limit_start, $limit_length)->order('createTime', 'desc')->select();
			$total = DB::name('express')->where('1=1' . $where . '', $value)->where('pro_status', 1)->count();
		}
		/* 超市代购 */
		if (($type == 3)) {

			$list = DB::name('market_shopping')->where('1=1' . $where . '', $value)->where('pro_status', 1)->limit($limit_start, $limit_length)->order('createTime', 'desc')->select();
			$total = DB::name('market_shopping')->where('1=1' . $where . '', $value)->where('pro_status', 1)->count();
		}

		/*圖書 書店 */
		if (($type == 4)) {

			$list = DB::name('bookstore')->where('1=1' . $where . '', $value)->where('pro_status', 1)->limit($limit_start, $limit_length)->order('createTime', 'desc')->select();
			$total = DB::name('bookstore')->where('1=1' . $where . '', $value)->where('pro_status', 1)->count();
		}

		/* 旅行代購 */
		if (($type == 5)) {

			$list = DB::name('shopping')->where('1=1' . $where . '', $value)->where(['pro_status' => 1, 'shopping_status' => 1])->limit($limit_start, $limit_length)->order('createTime', 'desc')->select();
			$total = DB::name('shopping')->where('1=1' . $where . '', $value)->where(['pro_status' => 1, 'shopping_status' => 1])->count();
		}

		/* 化妆护肤 */
		if (($type == 6)) {

			$list = DB::name('shopping')->where('1=1' . $where . '', $value)->where(['pro_status' => 1, 'shopping_status' => 3])->limit($limit_start, $limit_length)->order('createTime', 'desc')->select();
			$total = DB::name('shopping')->where('1=1' . $where . '', $value)->where(['pro_status' => 1, 'shopping_status' => 3])->count();
		}

		/* 转账存.取 */
		if (($type == 7)) {

			$list = DB::name('money')->where('1=1' . $where . '', $value)->where(['pro_status' => 1])->limit($limit_start, $limit_length)->order('createTime', 'desc')->select();
			$total = DB::name('money')->where('1=1' . $where . '', $value)->where(['pro_status' => 1])->count();
		}

		/* 帮我办事 */
		if (($type == 8)) {

			$list = DB::name('shopping')->where('1=1' . $where . '', $value)->where(['pro_status' => 1, 'shopping_status' => 5])->limit($limit_start, $limit_length)->order('createTime', 'desc')->select();
			$total = DB::name('shopping')->where('1=1' . $where . '', $value)->where(['pro_status' => 1, 'shopping_status' => 5])->count();
		}

		/* 其它代购 */
		if (($type == 9)) {

			$list = DB::name('shopping')->where('1=1' . $where . '', $value)->where(['pro_status' => 1, 'shopping_status' => 2])->limit($limit_start, $limit_length)->order('createTime', 'desc')->select();
			$total = DB::name('shopping')->where('1=1' . $where . '', $value)->where(['pro_status' => 1, 'shopping_status' => 2])->count();
		}
		if (count($list) <= 0) {
			$iTotalRecords = 0;
		}
		$iTotalRecords = sizeof($list);
		$iDisplayLength = intval($limit_length);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart = intval($limit_start);
		$sEcho = intval($data['draw']);
		$records = array();
		$records["data"] = array();
		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;

		for ($i = 0; $i < $end; $i++) {
			$id = $list[$i]['id'];
			$nickname = $list[$i]['nickname'];
			$proname = isset($list[$i]['proname']) ? $list[$i]['proname'] : '';
			$price = isset($list[$i]['proprice']) ? $list[$i]['proprice'] : '';
			$shop = isset($list[$i]['pro_shop']) ? $list[$i]['pro_shop'] : '';
			$arrive_time = isset($list[$i]['arrive_time']) ? date('Y-m-d H:m:s', $list[$i]['arrive_time']) : '';
			$triptime = isset($list[$i]['trip_time']) ? date('Y-m-d H:m:s', $list[$i]['trip_time']) : '';
			$back_time = isset($list[$i]['back_time']) ? date('Y-m-d H:m:s', $list[$i]['back_time']) : '';
			$begin_pos = isset($list[$i]['begin_pos']) ? $list[$i]['begin_pos'] : '';
			$back_pos = isset($list[$i]['back_pos']) ? $list[$i]['back_pos'] : '';
			$express_price = isset($list[$i]['express_price']) ? $list[$i]['express_price'] : '';
			$fee = isset($list[$i]['fee']) ? $list[$i]['fee'] : '';
			$express_status = isset($list[$i]['express_status']) ? $list[$i]['express_status'] : '';
			$shopname = isset($list[$i]['shopname']) ? $list[$i]['shopname'] : '';
			$bookstore = isset($list[$i]['bookstore_name']) ? $list[$i]['bookstore_name'] : '';
			$bookstatus = isset($list[$i]['book_status']) ? $list[$i]['book_status'] : '';
			$travel_pos = isset($list[$i]['travel_pos']) ? $list[$i]['travel_pos'] : '';
			$travel_back_pos = isset($list[$i]['travel_back_pos']) ? $list[$i]['travel_back_pos'] : '';
			$drawmoney = isset($list[$i]['drawmoney']) ? $list[$i]['drawmoney'] : ''; //存取金额
			$shopping_status = isset($list[$i]['shopping_status']) ? $list[$i]['shopping_status'] : '';
			$collect_time = isset($list[$i]['collect_time']) ? date('Y-m-d H:m:s', $list[$i]['collect_time']) : '';
			$bean = isset($list[$i]['bean']) ? $list[$i]['bean'] : '';
			$endTime = isset($list[$i]['order_end_time']) ? date('Y-m-d H:m:s', $list[$i]['order_end_time']) : '';
			$create_time = isset($list[$i]['createTime']) ? date('Y-m-d H:m:s', $list[$i]['createTime']) : '';

			if (!$type) {
				$records["data"][] = array(
					'<label class=" mt-checkbox mt-checkbox-single mt-checkbox-outline m-checkbox--solid m-checkbox--brand"><input name="id[]" type="checkbox" class="m-checkable " value="' . $id . '"/><span></span></label>',
					'<span>' . $nickname . '</span>',
					'<span>' . $proname . '</span>',
					'<span>' . $price . '</span>',
					'<span>' . $shop . '</span>',
					'<span>' . $arrive_time . '</span>',
					'<span>' . $bean . '</span>',
					'<span>' . $endTime . '</span>',
					'<span>' . $create_time . '</span>',
				);
			} else if ($type == 1) {

				$records["data"][] = array(
					'<label class=" mt-checkbox mt-checkbox-single mt-checkbox-outline m-checkbox--solid m-checkbox--brand"><input name="id[]" type="checkbox" class="m-checkable " value="' . $id . '"/><span></span></label>',
					'<span>' . $nickname . '</span>',
					'<span>' . $proname . '</span>',
					'<span>' . $triptime . '</span>',
					'<span>' . $back_time . '</span>',
					'<span>' . $begin_pos . '</span>',
					'<span>' . $back_pos . '</span>',
					'<span>' . $endTime . '</span>',
					'<span>' . $create_time . '</span>',
				);
			} elseif ($type == 2) {
				$records["data"][] = array(
					'<label class=" mt-checkbox mt-checkbox-single mt-checkbox-outline m-checkbox--solid m-checkbox--brand"><input name="id[]" type="checkbox" class="m-checkable " value="' . $id . '"/><span></span></label>',
					'<span>' . $nickname . '</span>',
					'<span>' . $proname . '</span>',
					'<span>' . $express_price . '</span>',
					'<span>' . $bean . '</span>',
					'<span>' . $fee . '</span>',
					'<span>' . get_fee_Status($express_status) . '</span>',
					'<span>' . $endTime . '</span>',
					'<span>' . $create_time . '</span>',
				);
			} elseif ($type == 3) {
				$records["data"][] = array(
					'<label class=" mt-checkbox mt-checkbox-single mt-checkbox-outline m-checkbox--solid m-checkbox--brand"><input name="id[]" type="checkbox" class="m-checkable " value="' . $id . '"/><span></span></label>',
					'<span>' . $nickname . '</span>',
					'<span>' . $proname . '</span>',
					'<span>' . $shopname . '</span>',
					'<span>' . $arrive_time . '</span>',
					'<span>' . $price . '</span>',
					'<span>' . $bean . '</span>',
					'<span>' . $endTime . '</span>',
					'<span>' . $create_time . '</span>',
				);
			} elseif ($type == 4) {
				$records["data"][] = array(
					'<label class=" mt-checkbox mt-checkbox-single mt-checkbox-outline m-checkbox--solid m-checkbox--brand"><input name="id[]" type="checkbox" class="m-checkable " value="' . $id . '"/><span></span></label>',
					'<span>' . $nickname . '</span>',
					'<span>' . $proname . '</span>',
					'<span>' . $bookstore . '</span>',
					'<span>' . $price . '</span>',
					'<span>' . $bean . '</span>',
					'<span>' . get_book_Status($bookstatus) . '</span>',
					'<span>' . $endTime . '</span>',
					'<span>' . $create_time . '</span>',
				);
			/* 	5 旅行代购 6 化妆品代购 8 帮我办事 9 其它代购  */
			} else if ($type == 5 || $type == 6 || $type == 8 || $type == 9) {
				$records["data"][] = array(
					'<label class=" mt-checkbox mt-checkbox-single mt-checkbox-outline m-checkbox--solid m-checkbox--brand"><input name="id[]" type="checkbox" class="m-checkable " value="' . $id . '"/><span></span></label>',
					'<span>' . $nickname . '</span>',
					'<span>' . $proname . '</span>',
					'<span>' . $arrive_time . '</span>',
					'<span>' . $price . '</span>',
					'<span>' . $bean . '</span>',
					'<span>' . $travel_pos . '</span>',
					'<span>' . $travel_back_pos . '</span>',
					'<span>' . $create_time . '</span>',
				);

			} elseif ($type == 7) {
				$records["data"][] = array(
					'<label class=" mt-checkbox mt-checkbox-single mt-checkbox-outline m-checkbox--solid m-checkbox--brand"><input name="id[]" type="checkbox" class="m-checkable " value="' . $id . '"/><span></span></label>',
					'<span>' . $nickname . '</span>',
					'<span>' . $proname . '</span>',
					'<span>' . $drawmoney . '</span>',
					'<span>' . $bean . '</span>',
					'<span>' . get_money_Status($shopping_status) . '</span>',
					'<span>' . $collect_time . '</span>',
					'<span>' . $endTime . '</span>',
					'<span>' . $create_time . '</span>',
				);
			}
		}

		if (isset($data["customActionType"]) && $data["customActionType"] == "group_action") {

			$records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
			$records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
		}

		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $total;
		$records["recordsFiltered"] = $total;
		echo json_encode($records);
	}

	//批量操作
	public function action_pro()
	{
		$action = input();
		$dellist = explode(',', $action['ids']);
		$type = input('type');
		if ($action['actionname'] == 'del') {
			if (!isset($type)) {
				$r = DB::name('food')->where('pro_status', 1)->where('id', 'IN', $dellist)->delete();
			} elseif ($type == 1) {
				$r = DB::name('trip')->where('pro_status', 1)->where('id', 'IN', $dellist)->delete();
			} elseif ($type == 2) {
				$r = DB::name('express')->where('pro_status', 1)->where('id', 'IN', $dellist)->delete();
			} elseif ($type == 3) {
				$r = DB::name('market_shopping')->where('pro_status', 1)->where('id', 'IN', $dellist)->delete();
			} elseif ($type == 4) {
				$r = DB::name('bookstore')->where('pro_status', 1)->where('id', 'IN', $dellist)->delete();
			} else if ($type == 5) {
				$r = DB::name('shopping')->where(['shopping_status' => 1, 'pro_status' => 1])->where('id', 'IN', $dellist)->delete();
			} elseif ($type == 6) {
				$r = DB::name('shopping')->where(['shopping_status' => 3, 'pro_status' => 1])->where('id', 'IN', $dellist)->delete();
			} elseif ($type == 7) {
				$r = DB::name('money')->where('pro_status', 1)->where('id', 'IN', $dellist)->delete();
			} elseif ($type == 8) {
				$r = DB::name('shopping')->where(['shopping_status' => 5, 'pro_status' => 1])->where('id', 'IN', $dellist)->delete();
			} elseif ($type == 9) {
				$r = DB::name('shopping')->where(['shopping_status' => 2, 'pro_status' => 1])->where('id', 'IN', $dellist)->delete();
			}

			if ($r) {
				echo 1;
			}
		}
	}

}







