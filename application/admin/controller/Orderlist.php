<?php

/**
 * @Author: zhiwei wu
 * @Date:   2018-12-03 16:29:14
 * @Last Modified by:   可待科技
 * @Last Modified time: 2018-12-13 12:09:01
 */
namespace app\admin\controller;

use think\Session;
use think\Db;
use think\Env;
use think\Request;
//列表
class Orderlist extends Base
{
    //页面
    public function index()
    {
        $active = "orderlist";
        $type = input('type');
        return view('orderlist/index', ['active' => $active, 'type' => $type]);
    }

    //显示订单列表
    public function ord_list()
    {
        $data = input();
        //得到limit参数
        $limit_start = $data['start'];
        $limit_length = $data['length'];
        $n_name = trim(input('nickname'));
        $title = input('title');
        $order_num = input('order_num');
        $status = input('status');
        $startTime = input('reg_start');
        $endTime = input('reg_end');

        //检索
        $where = '';
        $value = [];

        if (!empty($n_name)) {
            $where .= ' AND nickname = :nickname';
            $value['nickname'] = $n_name;
        }

        if (!empty($order_num)) {
            $where .= ' AND order_num = :order_num';
            $value['order_num'] = trim($order_num);
        }

        if (!empty($startTime) && !empty($endTime)) {
            $where .= ' AND create_time > :reg_start';
            $value['reg_start'] = strtotime($startTime);
            $where .= ' AND create_time <= :reg_end';
            $value['reg_end'] = strtotime($endTime);
        }

        if (!empty($title)) {
            $where .= ' AND title = :title';
            $value['title'] = trim($title);
        }

        if (!empty($status) && $status != "none") {
            $where .= ' AND status = :status';
            $value['status'] = trim($status);
        }

        /* 帮我带订单 */
        if ($data['type']) {
            $list = DB::name('order')->where('1=1' . $where . '', $value)->where('pro_status', $data['type'])->limit($limit_start, $limit_length)->order('id', 'desc')->select();
            $total = DB::name('order')->where('1=1' . $where . '', $value)->where('pro_status', $data['type'])->count();
        }

        $iTotalRecords = sizeof($list);
        $iDisplayLength = intval($limit_length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($limit_start);
        $sEcho = intval($data['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = 0; $i < $end; $i++) {
            $id = $list[$i]['id'];
            $username = $list[$i]['nickname'];
            $proname = $list[$i]['title'];
            $order_num = $list[$i]['order_num'];
            $price = $list[$i]['proprice'];
            $ordtime = date('Y-m-d H:m:s', $list[$i]['create_time']);
            $bean = $list[$i]['bean'];
            $ordStatus = $list[$i]['status'];
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="m-checkable" value="' . $id . '"/><span></span></label>',
                '<span>' . $username . '</span>',
                '<span>' . $proname . '</span>',
                '<span>' . $order_num . '</span>',
                '<span>' . $price . '元</span>',
                '<span>' . $bean . '</span>',
                '<span>' . get_order_Status($ordStatus) . '</span>',
                '<span>' . $ordtime . '</span>',
                '<a href="../a/editord.html?oid=' . $id . '&type=' . $data['type'] . '" class="action' . $id . ' btn red btn-outline sbold">
					<i class="fa fa-pencil"></i>&nbsp;编辑/查看
				</a>'
            );
        }
        if (isset($data["customActionType"]) && $data["customActionType"] == "group_action") {

            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";

        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $total;
        $records["recordsFiltered"] = $total;
        return json($records);
    }

    //编辑.查看订单

    public function editord()
    {
        $t = input('type');
        if (request()->isGet()) {
            $active = "editord";
            $id = input('oid');
            $list = DB::name('order')->where(['id' => $id])->find();
            if ($list) {
                $list['img'] = config('website') . $list['img'];
                $list['order_end_time'] = date('Y-m-d H:m:s', $list['order_end_time']);
                $list['receivingtime'] = date('Y-m-d H:m:s', $list['receivingtime']);
                $list['finishtime'] = date('Y-m-d H:m:s', $list['finishtime']);
                $list['shiptime'] = date('Y-m-d H:m:s', $list['shiptime']);
                $list['create_time'] = date('Y-m-d H:m:s', $list['create_time']);

            } else {
                $list = [];
            }
            return view('orderlist/edit', ['active' => $active, 'list' => $list, 't' => $t]);
        }
        $data = request()->param();
        if (!$data['title']) {
            echo 3;
        }
        $id = $data['id'];
        unset($data['id']);
        if ($data['img']) {
            if (substr($data['img'], 0, 4) == 'http') {
                $data['img'] = substr($data['img'], strpos($data['img'], '/upload'));
            } else {
                $data['img'] = base64_to_img($data['img']);
            }
        }
        $r = DB::name('order')->where(['id' => $id])->update($data);
        if ($r) {
            echo 1;
        } else {
            echo 2;
        }
    }
    

    //批量删除操作
    public function ord_action()
    {
        $action = input();
        if ($action['actionname'] == "del") {

            $dellist = explode(',', $action['ids']);
            $countdel = count($dellist);
            $i = 0;
            foreach ($dellist as $v) {
                if ($action['type'] == 1) {
                    Db::name('order')->where(['id' => $v, 'pro_status' => 1])->delete();
                } elseif ($action['type'] == 2) {
                    Db::name('order')->where(['id' => $v, 'pro_status' => 2])->delete();
                }
                $i++;
            }
            if ($countdel == $i) {
                echo 1;
            }
        //导出操作
        } else if ($action['actionname'] == "doexport") {
            import('PHPExcel.PHPExcel', EXTEND_PATH);
            $objPHPExcel = new \PHPExcel();
            $store_ids = explode(',', $action['ids']);
            try {
                $objPHPExcel->getActiveSheet()->SetCellValue('A1', '订单编号');
                $objPHPExcel->getActiveSheet()->SetCellValue('B1', '订单用户名');
                $objPHPExcel->getActiveSheet()->SetCellValue('C1', '物品描述');
                $objPHPExcel->getActiveSheet()->SetCellValue('D1', '订单号');
                $objPHPExcel->getActiveSheet()->SetCellValue('E1', '订单价格');
                $objPHPExcel->getActiveSheet()->SetCellValue('F1', '福豆');
                $objPHPExcel->getActiveSheet()->SetCellValue('G1', '订单类型');
                $objPHPExcel->getActiveSheet()->SetCellValue('H1', '订单状态');
                $objPHPExcel->getActiveSheet()->SetCellValue('I1', '下单时间');
                $objPHPExcel->getActiveSheet()->SetCellValue('J1', '接单时间');
                $objPHPExcel->getActiveSheet()->SetCellValue('K1', '订单完成时间');
                $objPHPExcel->getActiveSheet()->SetCellValue('L1', '导出时间');
                if ($action['type'] == 1) {

                    $store_ids = DB::name('order')->where('id', 'in', $action['ids'])->where('pro_status', 1)->select();
                    $t = '帮我带';
                } elseif ($action['type'] == 2) {

                    $store_ids = DB::name('order')->where('id', 'in', $action['ids'])->where('pro_status', 2)->select();
                    $t = '帮你带';
                }
                if (count($store_ids) > 0) {
                    foreach ($store_ids as &$v) {
                        $v['uid'] = DB::name('member')->where('id', $v['uid'])->value('nickname');
                        $v['pro_status'] = ($v['pro_status'] == 1) ? '帮我带' : '帮你带';
                        $v['status'] = get_order_Status($v['status']);
                        $v['create_time'] = date('Y-m-d H:m:s', $v['create_time']);
                        $v['shiptime'] = date('Y-m-d H:m:s', $v['shiptime']);
                        $v['receivingtime'] = date('Y-m-d H:m:s', $v['receivingtime']);
                        $v['time'] = date('Y-m-d H:m:s', time());
                        $v['price'] = $v['price'] . '元';
                    }
                }

                $cell_counter = 1;
                foreach ($store_ids as $shopinfo) {
                    $cell_counter++;
                    $objPHPExcel->setActiveSheetIndex(0);
                    $objPHPExcel->getActiveSheet()->SetCellValue('A' . $cell_counter, $shopinfo['id']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $cell_counter, $shopinfo['uid']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $cell_counter, $shopinfo['title']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('D' . $cell_counter, $shopinfo['order_num']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('E' . $cell_counter, $shopinfo['price']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('F' . $cell_counter, $shopinfo['bean']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('G' . $cell_counter, $shopinfo['pro_status']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('H' . $cell_counter, $shopinfo['status']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('I' . $cell_counter, $shopinfo['create_time']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('J' . $cell_counter, $shopinfo['shiptime']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('K' . $cell_counter, $shopinfo['receivingtime']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('L' . $cell_counter, $shopinfo['time']);
                }

                for ($col = 'A'; $col !== 'L'; $col++) {
                    $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                }

                $objPHPExcel->getActiveSheet()->setTitle($t . "订单表");

                $upload_dir = ROOT_PATH . 'public/excel';
                $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

                $filesname = time() . '.xlsx';
                $newpath = $upload_dir . '/' . $filesname;
                $objWriter->save($newpath);
                echo '1|' . config('website') . '/excel/' . $filesname;

            } catch (Exception $e) {
                echo "导出失败";
            }
        }
    }

}







