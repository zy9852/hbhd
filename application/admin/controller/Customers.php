<?php

namespace app\admin\controller;

use think\Db;
use think\Session;
use think\view;
use think\Request;
//用户列表
class Customers extends Base
{   

    //列表
    public function index()
    {
        $active = "custlist";

        $list = Db::name('member')->order('create_time desc')->select();
        if ($list) {
            foreach ($list as &$v) {
                $v['time'] = date('Y-m-d H:m:s', time());
                $v['create_time'] = date('Y-m-d H:m:s', $v['create_time']);
            }
        }
        return view('', ['active' => $active, 'list' => $list]);
    }


    //编辑用户
    public function saveUserMsg()
    {
        $data = input('post.');

        if (strlen($data['password']) == 32) {
            $data['password'] = $data['password'];
        } else {
            $data['password'] = md5($data['password']);
        }
        $r = DB::name('member')->where('id', $data['id'])->update($data);
        if ($r) {
            $msg = ['code' => 1, 'msg' => '编辑成功'];
            echo json_encode($msg);
            exit;
        } else {
            $msg = ['code' => 2, 'msg' => '编辑失败'];
            echo json_encode($msg);
            exit;
        }
    }

    //查看用户地址
    public function location()
    {
        $active = "locationlist";
        $id = input('id');
        $info = DB::name('memberLocation')->where('uid', $id)->order('id desc')->select();
        if (count($info) > 0) {
            $list = $info;
        } else {
            $list = 1;
        }
        return view('customers/u_address', ['active' => $active, 'list' => $list]);
    }

    //删除用户
    public function deluser()
    {
        $id = input('param.id');
        $res = Db::name('member')->where('id', $id)->delete();
        if ($res) {
            $msg = ['code' => 200, 'msg' => '已删除'];
            echo json_encode($msg);
            exit;
        } else {
            $msg = ['code' => 400, 'msg' => '删除失败'];
            echo json_encode($msg);
            exit;
        }
    }
  
    //新增部分, 升级用户
    public function upuser()
    {
        $id = input('param.id');
        $res = Db::name('member')->where('id', $id)->update(['check' => 2]);
        if ($res) {
            $msg = ['code' => 200, 'msg' => '已通过'];
            echo json_encode($msg);
            exit;
        } else {
            $msg = ['code' => 400, 'msg' => '操作失败'];
            echo json_encode($msg);
            exit;
        }
    }

    //添加用户
    public function addUserMsg()
    {
        $data = input('post.');
        // $data['createTime'] = strtotime('now');
        $uid = Session::get('uid');
        $r = DB::name('member')->where(['username' => $data['username']])->find();
        //验证手机号
        $rs = db::name('member')->where(['phone' => $data['phone']])->find();
        if ($rs) {
            $msg = ['code' => 300, 'msg' => '该账号已存在'];
            echo json_encode($msg);
            exit;
        }

        $data['password'] = md5($data['password']);
        $data['check'] = 2;
        $res = DB::name('member')->insert($data);
        if ($res) {
            $msg = ['code' => 200, 'msg' => '添加成功'];
            echo json_encode($msg);
            exit;
        } else {
            $msg = ['code' => 400, 'msg' => '添加失败'];
            echo json_encode($msg);
            exit;
        }
    }

}
