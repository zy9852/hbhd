<?php

namespace app\admin\controller;

use think\Controller;
use think\Session;
use think\Db;

class Login extends Controller
{
    public function index()
    {
        $option = model('option');
        $title = $option->where('meta_key', 'web_title')->value('meta_value');
        $uid = session('uid');
        if ($uid) {
            $this->redirect('admin/index/index');
        }
        return view('base/login', ['active' => "login", 'title' => $title]);
    }

    //检测登录
    public function checkLogin()
    {
        $data = input('post.');
        if (!captcha_check($data['verifyCode'])) {
            $msg = ['code' => 400, 'msg' => '验证码错误！'];
            echo json_encode($msg);
            exit;
        }
        unset($data['verifyCode']);
        $info = Db::name("admin")->where(['account' => $data['account'], 'password' => md5($data['password']), 'is_delete' => 1, 'status' => 1])->find();
        if (!empty($info)) {
            Session::set('uid', $info['id']);
            Session::set('realname', $info['realname']);
            // 保存登录信息
            $data['last_login_time'] = time();
            Db::name("admin")->where('id', $info['id'])->update(['last_login_time' => $data['last_login_time']]);
            $msg = ['code' => 200, 'msg' => '登录成功！'];
            echo json_encode($msg);
            exit;
        } else {
            $msg = ['code' => 500, 'msg' => '账号或者密码不存在！'];
            echo json_encode($msg);
            exit;
        }
    }

    //登出处理
    public function logout()
    {
        Session::delete('uid');
        $this->redirect('admin/base/login');
    }

}