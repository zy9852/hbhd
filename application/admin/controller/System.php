<?php

/**
 * @Author: zhiwei wu
 * @Date:   2018-12-03 16:29:14
 * @Last Modified by:   可待科技
 * @Last Modified time: 2018-12-03 21:01:42
 */
namespace app\admin\controller;

use think\Session;
use think\Db;
use think\Env;

//上传Banner
class System extends Base
{
    public function photo()
    {
        if (request()->isGet()) {
            $active = "photo";
            $pic = Db::name('media')->where('type', 'Banner')->find();
            return view('system/photo', ['active' => $active, 'pic' => $pic]);
        }
        if (request()->isPost()) {
            $data = input();
            if (!isset($data['pic'])) {
                echo 2;
                exit;
            }
            $thumb = $data['pic'];
            if (substr($thumb, 0, 4) == 'http') {
                $thumbname = $thumb;
            } else {
                $thumbname = config('website') . base64_to_img($thumb);
            }
            $row = DB::name('media')->where('type', 'Banner')->find();
            if ($row) {
                DB::name('media')->where('id', $data['id'])->update(['pic1' => $thumbname]);
            } else {
                DB::name('media')->insert(['pic1' => $thumbname, 'type' => "Banner"]);
            }
            echo 1;
            exit;
        }
    }
    

    //福豆设置
    public function bean()
    {
        if (request()->isGet()) {
            $active = "bean";
            $bean = Db::name('option')->where(['meta_key' => 'bean'])->find();
            $service = Db::name('option')->where(['meta_key' => 'service'])->find();
            return view('system/bean', ['active' => $active, 'bean' => $bean, 'service' => $service]);
        }
        if (request()->isPost()) {
            $data = input();
            if (empty($data['bean']) || empty($data['service'])) {
                echo 2;
                exit;
            }
            $r = DB::name('option')->where('meta_key', 'bean')->find();
            $s = DB::name('option')->where('meta_key', 'service')->find();
            if ($s) {
                DB::name('option')->where('id', $data['sid'])->update(['meta_value' => $data['service']]);

            } else {
                DB::name('option')->insert(['meta_key' => 'service', 'meta_value' => $data['service']]);

            }
            if ($r) {
                DB::name('option')->where('id', $data['id'])->update(['meta_value' => $data['bean']]);

            } else {
                DB::name('option')->insert(['meta_key' => 'bean', 'meta_value' => $data['bean']]);

            }
            echo 1;

        }
    }

    //用户福豆详情
    public function beandetail()
    {
        $active = "beandetail";
        $list = Db::name('member_bean_detail')->order('createtime desc')->select();
        if ($list) {
            foreach ($list as &$v) {
                $v['nickname'] = DB::name('member')->where('id', $v['uid'])->value('nickname');
                $v['d_type'] = $v['d_type'] == 1 ? '支出' : '收入';
                $v['createtime'] = date('Y-m-d H:m:s', $v['createtime']);
            }
        }
        return view('system/detailbean', ['active' => $active, 'list' => $list]);
    }

    //用福豆提现申请表
    public function applybean()
    {
        $active = "applybean";
        $list = Db::name('member_bean')->order('createtime desc')->select();
        if ($list) {
            foreach ($list as &$v) {
                $v['nickname'] = DB::name('member')->where('id', $v['uid'])->value('nickname');
                $v['bean_type'] = $v['bean_type'] == 1 ? '审核中' : '已通过';
                $v['createtime'] = date('Y-m-d H:m:s', $v['createtime']);
            }
        }
        return view('system/applybean', ['active' => $active, 'list' => $list]);
    }
    //审核提现
    public function upuser()
    {
        $id = input('param.id');
        $res = Db::name('member_bean')->where('id', $id)->update(['bean_type' => 2]);
        if ($res) {
            $msg = ['code' => 200, 'msg' => '已通过'];
            echo json_encode($msg);
            exit;
        } else {
            $msg = ['code' => 400, 'msg' => '操作失败'];
            echo json_encode($msg);
            exit;
        }
    }

    //标签设置
    /* public function settag()
    {
        if (request()->isGet()) {
            $active = "settag";
            $shop = Db::name('option')->where(['meta_key' => 'shop'])->find();
            $kowei = Db::name('option')->where(['meta_key' => 'kowei'])->find();
            $proname = Db::name('option')->where(['meta_key' => 'proname'])->find();
            return view('', ['active' => $active, 'shop' => $shop, 'kowei' => $kowei, 'proname' => $proname]);
        }
        if (request()->isPost()) {
            $data = request()->param();
            if (empty($data['proname']) && empty($data['kowei'] && empty($data['shop']))) {
                echo 2;
                exit;
            }
            $r = DB::name('option')->where('meta_key', 'shop')->find();

            if ($r) {
                DB::name('option')->where('id', $data['kid'])->update(['meta_value' => $data['shop']]);

            } else {
                DB::name('option')->insert(['meta_key' => 'shop', 'meta_value' => $data['shop']]);
            }

            $s = DB::name('option')->where('meta_key', 'proname')->find();
            if ($s) {
                DB::name('option')->where('id', $s['id'])->update(['meta_value' => $data['proname']]);
            } else {
                DB::name('option')->insert(['meta_key' => 'proname', 'meta_value' => $data['proname']]);
            }

            $e = DB::name('option')->where('meta_key', 'kowei')->find();
            if ($e) {
                DB::name('option')->where('id', $e['id'])->update(['meta_value' => $data['kowei']]);
            } else {
                DB::name('option')->insert(['meta_key' => 'kowei', 'meta_value' => $data['kowei']]);
            }

            echo 1;exit;
        }
    } */

}







