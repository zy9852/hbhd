<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.kissneck.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 可待科技-磊子 <sxfyxl@126.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use think\Controller;
use think\Session;
use think\Db;

class Base extends Controller
{
    public function _initialize()
    {
        if(!is_login()){
            $this->redirect('admin/login/index');
        }else{
            //网站常规属性
            global $uid,$realname,$power;
            $uid = session('uid');
            $realname = session('realname');

            $option  = model('option');
            $logo    = $option->alias('o')
                ->where('meta_key','web_logo')
                ->join('y_media i','o.meta_value=i.id')
                ->value('pic1');
            $title   = $option->where('meta_key','web_title')->value('meta_value');
            $this->assign('logo', $logo);
            $this->assign('title', $title);

            //分配个人权限
            $uid = Session::get('uid');
            $power = Db::name('admin')->where('id',$uid)->value('power');
            $this->assign('power', $power);
        }

    }

}
