<?php
namespace app\admin\controller;

use think\Session;
use think\Db;
use think\Env;
use think\Request;

//商家列表
class Shoplist extends Base
{
	private $appId = 'wx34e590df69bb0396';                   // 小程序---Key
	private $Scret = 'c62610b43ba0758fddf10aa6261cffe4';     // 小程序---Secret

	//商家列表
	public function index()
	{
		$active = "shoplist";
		$shop = Db::name('shop')->select();

	    //p($category);
		return view('', ['active' => $active, 'shop' => $shop]);
	}

	//更新管家状态
	public function update_shop()
	{
		$data = input();
		$shop = model('shop');
        //$shop = Db::name('shop');

		$data_array = array(
			'id' => @$data['sid'],
			'des' => @$data['des'],
			'updatetime' => time(),
			'status' => @$data['status']
		);

		$s = $shop->update($data_array);
		if ($s) {

			$msg = ['code' => 200, 'msg' => '状态更新成功', 'v' => get_status($data['status'])];
			$find = $shop->field('formid,id,uid,province,city,area')->where('id', @$data['sid'])->find();
			if ($data['status'] == 2) {
				Db::name('customers')->where('id', $find['uid'])->setField('is_proxy', 1);
				Db::name('customers')->where('id', $find['uid'])->setField('followid', $data['sid']);

				$pro = mb_substr($find['province'], 0, -1, 'utf-8');
				$cit = $find['city'];
				$are = $find['area'];

				if ($find['area'] == "市辖区") {
					$are = $cit;
				}

				$region = Db::name('area')->where('s', $pro)->where('c', $cit)->where('a', $are)->value('l');
				if (!$region) {
					$region = Db::name('area')->where('s', $pro)->where('c', $cit)->value('l');
					if (!$region) {
						$region = Db::name('area')->where('s', $pro)->value('l');
					}
				}
				Db::name('shop')->where('id', $find['id'])->update(['region' => $region]);

			}
			if ($data['status'] == 2 || $data['status'] == 3) {

			}

		} else {
			$msg = ['code' => 200, 'msg' => '状态更新失败', 'v' => get_status($data['status'])];
		}
		echo json_encode($msg);
	}

	//发送模板消息通知
	public function send_post($url, $post_data)
	{
		$options = array(
			'http' => array(
				'method' => 'POST',
				'header' => 'Content-type:application/json',
				//header 需要设置为 JSON
				'content' => $post_data,
				'timeout' => 60
				//超时时间
			)
		);

		$context = stream_context_create($options);
		$result = file_get_contents($url, false, $context);

		return $result;
	}

	/**
	 * 发起http请求
	 *
	 * @param string $url 访问路径
	 * @param array $params 参数，该数组多于1个，表示为POST
	 * @param int $expire 请求超时时间
	 * @param array $extend 请求伪造包头参数
	 * @param string $hostIp HOST的地址
	 *
	 * @return array    返回的为一个请求状态，一个内容
	 */

	function makeRequest($url, $params = array(), $expire = 0, $extend = array(), $hostIp = '')
	{
		if (empty($url)) {
			return array('code' => '100');
		}

		$_curl = curl_init();
		$_header = array(
			'Accept-Language: zh-CN',
			'Connection: Keep-Alive',
			'Cache-Control: no-cache'
		);
		// 方便直接访问要设置host的地址
		if (!empty($hostIp)) {
			$urlInfo = parse_url($url);
			if (empty($urlInfo['host'])) {
				$urlInfo['host'] = substr(DOMAIN, 7, -1);
				$url = "http://{$hostIp}{$url}";
			} else {
				$url = str_replace($urlInfo['host'], $hostIp, $url);
			}
			$_header[] = "Host: {$urlInfo['host']}";
		}

		// 只要第二个参数传了值之后，就是POST的
		if (!empty($params)) {
			curl_setopt($_curl, CURLOPT_POSTFIELDS, http_build_query($params));
			curl_setopt($_curl, CURLOPT_POST, true);
		}

		if (substr($url, 0, 8) == 'https://') {
			curl_setopt($_curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($_curl, CURLOPT_SSL_VERIFYHOST, false);
		}
		curl_setopt($_curl, CURLOPT_URL, $url);
		curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($_curl, CURLOPT_USERAGENT, 'API PHP CURL');
		curl_setopt($_curl, CURLOPT_HTTPHEADER, $_header);

		if ($expire > 0) {
			curl_setopt($_curl, CURLOPT_TIMEOUT, $expire); // 处理超时时间
			curl_setopt($_curl, CURLOPT_CONNECTTIMEOUT, $expire); // 建立连接超时时间
		}

		// 额外的配置
		if (!empty($extend)) {
			curl_setopt_array($_curl, $extend);
		}

		$result['result'] = curl_exec($_curl);
		$result['code'] = curl_getinfo($_curl, CURLINFO_HTTP_CODE);
		$result['info'] = curl_getinfo($_curl);
		if ($result['result'] === false) {
			$result['result'] = curl_error($_curl);
			$result['code'] = -curl_errno($_curl);
		}

		curl_close($_curl);

		return $result;
	}
	//批量删除操作

	public function action_shop()
	{
		$action = input();
		$shop = model('shop');
		$product_meta = Db::name('product_meta');
		if ($action['actionname'] == "del") {

			$dellist = explode(',', $action['ids']);
			$countdel = count($dellist);
			$i = 0;
			foreach ($dellist as $v) {
				Db::name('shop')->where('id', $v)->delete();
				$i++;
			}
			if ($countdel == $i) {
				echo 1;
			}

		} else if ($action['actionname'] == "doexport") {
			import('PHPExcel.PHPExcel', EXTEND_PATH);
			$objPHPExcel = new \PHPExcel();
			//$store_ids   = explode(',',$action['ids']);.
//			$store_ids = $shop->where( 'id','in', $action['ids'] )->select();
//			foreach ($store_ids as $shopinfo) {
//				p($shopinfo['city']);
//			}
			//exit;
			try {
				$objPHPExcel->getActiveSheet()->SetCellValue('A1', '商家编号');
				$objPHPExcel->getActiveSheet()->SetCellValue('B1', '商家id');
				$objPHPExcel->getActiveSheet()->SetCellValue('C1', '商家名称');
				$objPHPExcel->getActiveSheet()->SetCellValue('D1', '性别');
				$objPHPExcel->getActiveSheet()->SetCellValue('E1', '联系方式');
				$objPHPExcel->getActiveSheet()->SetCellValue('F1', '店铺人群');
				$objPHPExcel->getActiveSheet()->SetCellValue('G1', '附近设备');
				$objPHPExcel->getActiveSheet()->SetCellValue('H1', '店铺位置');
				$objPHPExcel->getActiveSheet()->SetCellValue('I1', '性格优势');
				$objPHPExcel->getActiveSheet()->SetCellValue('J1', '配合程度');
				$objPHPExcel->getActiveSheet()->SetCellValue('K1', '社交个数');
				$objPHPExcel->getActiveSheet()->SetCellValue('L1', '创建时间');
			/*	$objPHPExcel->getActiveSheet()->SetCellValue('M1', '详细地址');
				$objPHPExcel->getActiveSheet()->SetCellValue('N1', '申请日期');*/

				$cell_counter = 1;
				$store_ids = $shop->where('id', 'in', $action['ids'])->select();

				foreach ($store_ids as $shopinfo) {
					$cell_counter++;
					/*if (empty($shopinfo['followed'])) {
						$followed = 0;
					} else {
						$follow = explode(',', $shopinfo['followed']);
						$followed = sizeof(array_unique($follow));
					}*/
					//p($followed);
					//exit;
					$objPHPExcel->setActiveSheetIndex(0);
					$objPHPExcel->getActiveSheet()->SetCellValue('A' . $cell_counter, $shopinfo['id']);
					$objPHPExcel->getActiveSheet()->SetCellValue('B' . $cell_counter, $shopinfo['uid']);
					$objPHPExcel->getActiveSheet()->SetCellValue('C' . $cell_counter, $shopinfo['shopname']);
					$objPHPExcel->getActiveSheet()->SetCellValue('D' . $cell_counter, $shopinfo['gender']);
					$objPHPExcel->getActiveSheet()->SetCellValue('E' . $cell_counter, $shopinfo['phone']);
					// $objPHPExcel->getActiveSheet()->SetCellValue('F' . $cell_counter, $followed);
					$objPHPExcel->getActiveSheet()->SetCellValue('F' . $cell_counter, $shopinfo['shopers']);
					$objPHPExcel->getActiveSheet()->SetCellValue('G' . $cell_counter, $shopinfo['nearmec']);
					$objPHPExcel->getActiveSheet()->SetCellValue('H' . $cell_counter, $shopinfo['location']);
					$objPHPExcel->getActiveSheet()->SetCellValue('I' . $cell_counter, $shopinfo['g_nature']);
					$objPHPExcel->getActiveSheet()->SetCellValue('J' . $cell_counter, $shopinfo['helper']);
					$objPHPExcel->getActiveSheet()->SetCellValue('K' . $cell_counter, $shopinfo['p_nums']);
					$objPHPExcel->getActiveSheet()->SetCellValue('L' . $cell_counter, $shopinfo['createTime']);
				/*	$objPHPExcel->getActiveSheet()->SetCellValue('N' . $cell_counter, date('Y-m-d H:i:s', $shopinfo['createtime']));*/

				}

				// Set column data auto width
				for ($col = 'A'; $col !== 'L'; $col++) {
					$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
				}

				// Rename sheet
				$objPHPExcel->getActiveSheet()->setTitle("商家统计表");

				// Save Excel file
				$upload_dir = ROOT_PATH . 'public' . DS . 'excel';   //上传文件的地址
				//$objWriter  = new \PHPExcel_Writer_Excel2007( $objPHPExcel );
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				//$objWriter->save('php://output');
				$filesname = time() . '.xlsx';
				$newpath = $upload_dir . '/' . $filesname;
				// p($newpath);exit;
				$objWriter->save($newpath);

				echo '1|' . config('website') . '/excel/' . $filesname;

			} catch (Exception $e) {
				echo "导出失败";
			}
		}
	}

    //显示所有商家列表
	public function list_shop()
	{
		$data    = input();
		$jdnumber = trim(input('number')); //检索
		$filter_status = input('filter-status');
        //得到limit参数
		$limit_start = input('start');
		$limit_length = input('length');

		if (!empty($filter_status) && $filter_status != 'all') {
			$where .= ' AND status=' . $filter_status;
		}

		//高級設置
		$column  = $data['order'][0]['column'];
		$order   = $data['order'][0]['dir'];
		if($column ==10){
			$order_name    = "helper ".$order;
		}else{
			$order_name = "id desc";
		}


		if($jdnumber){
			$list = db('shop')->where('shopname',$jdnumber)->order("{$order_name}")->select();
		}else{
			$list = db('shop')->order("{$order_name}")->select();
		}

		$iTotalRecords = count($list);
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);

		$records = array();
		$records["data"] = array();

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;

		for ($i = $iDisplayStart; $i < $end; $i++) {

			$pid 		= $list[$i]['id']; //编号
			$uid 		= $list[$i]['uid']; //商家id
			$shopname 	= $list[$i]['shopname']; //店铺名称
			$gender 	= $list[$i]['gender']; //性别
			$phone 		= $list[$i]['phone']; //联系方式
			$shopers 	= $list[$i]['shopers']; //店铺人群
			$nearmec 	= $list[$i]['nearmec'];//附近设备
			$location 	= $list[$i]['location'];//店铺位置
			$g_nature 	= $list[$i]['g_nature'];//性格优势
			$helper 	= $list[$i]['helper'];//配合程度
			$p_nums 	= $list[$i]['p_nums'];/*社交个数*/
			$createTime = $list[$i]['createTime'];/*创建时间*/


			/*if ($is_proxy != 1 && $list[$i]['status'] == 2) {
				$status = '<span>审核中</span>';
			} else {
				$status = get_status($list[$i]['status']);
			}

			if ($list[$i]['status'] != 1 && $is_proxy == 1) {
				$c = 'display-hide';
			} else {
				$c = '';
			}

			if ($list[$i]['checked'] == 0) {
				$d = '';
				$e = 'display-hide';
				$f = 'display-hide';
			} else if ($list[$i]['checked'] == 1) {
				$e = '';
				$d = 'display-hide';
				$f = 'display-hide';

			} else if ($list[$i]['checked'] == 2) {

				$f = '';
				$d = 'display-hide';
				$e = 'display-hide';

			}

			if ($list[$i]['isup'] == 0) {
				$d1 = '';
				$e1 = 'display-hide';
				$f1 = 'display-hide';
			} else if ($list[$i]['isup'] == 1) {
				$e1 = '';
				$d1 = 'display-hide';
				$f1 = 'display-hide';

			} else if ($list[$i]['isup'] == 2) {

				$f1 = '';
				$d1 = 'display-hide';
				$e1 = 'display-hide';

			}*/

			$records["data"][] = array(
				'<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="' . $pid . '"/><span></span></label>',
				'<span>' . $uid . '</span>',
				'<span>' . $shopname . '</span>',
				'<span>' . $gender . '</span>',
				'<span>' . $phone . '</span>',
				'<span>' . $shopers . '</span>',
				'<span>' . $nearmec . '</span>',
				'<span>' . $location . '</span>',
				'<span>' . $g_nature . '</span>',
				'<span>' . $helper . '</span>',
				'<span>' . $p_nums . '</span>',
				'<span>' . $createTime . '</span>'
				/*'<span class="managestatus' . $pid . '">' . $status . '</span>',
				'<a href="../admin/shoplist/updatemodel/?sid=' . $pid . '" data-target="#ajax" data-toggle="modal" class="' . $c . ' action' . $pid . ' btn green btn-outline sbold">
					<i class="fa fa-pencil"></i>&nbsp;审核 &nbsp;
				</a> &nbsp;<a href="/a/shopupdate.html?sid=' . $pid . '" class="action' . $pid . ' btn red btn-outline sbold">
					<i class="fa fa-pencil"></i>&nbsp;查看
				</a>',

				// '<button  class="sjtzbtn btn red btn-outline sbold">
				// 	<i class="fa fa-pencil"></i>&nbsp;升级团长
				// </button>'
				//升级团长
				'<a href="../admin/shoplist/updateqx/?sid=' . $pid . '" data-target="#ajax" data-toggle="modal" class="' . $d . ' action' . $pid . ' btn green btn-outline sbold sjtzbtn">
					<i class="fa fa-pencil"></i>&nbsp;<span class="uptitle' . $pid . '">升级团长</span> &nbsp;
				</a>&nbsp;<a href="javascript:;" class="' . $e . ' action' . $pid . ' btn red btn-outline sbold">
					<i class="fa fa-pencil"></i>&nbsp;升级中...
				</a>&nbsp;<a href="javascript:;" class="' . $f . ' action' . $pid . ' btn red btn-outline sbold">
					<i class="fa fa-pencil"></i>&nbsp;已升级
				</a>
				<a href="../admin/shoplist/upshop/?sid=' . $pid . '" data-target="#ajax" data-toggle="modal" class="' . $d1 . ' action' . $pid . ' btn red btn-outline sbold sjtzbtn">
					<i class="fa fa-pencil"></i>&nbsp;<span class="upshop' . $pid . '">升级店长</span> &nbsp;
				</a>&nbsp;<a href="javascript:;" class="' . $e1 . ' action' . $pid . ' btn red btn-outline sbold">
					店长升级中
				</a>&nbsp;<a href="javascript:;" class="' . $f1 . ' action' . $pid . ' btn red btn-outline sbold">
					店长已升级
				</a> ',*/
			);

		}

		if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {

			$records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
			$records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)

		}

		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		echo json_encode($records);
	}

    //更新坐标
	public function changeLn()
	{
        //获取坐标地址
		$shop = Db::name('shop');
		$allshop = $shop->where('status', 2)->select();

        //p($allshop);

		$loc = $this->gettoKm();

        //p($loc);
        //exit;

		foreach ($allshop as $k => $v) {
			$latitude = $v['latitude'];
			$longitude = $v['longitude'];

			if (empty($latitude) || empty($longitude)) {

				$address = $v['province'] . $v['city'] . $v['area'] . $v['address'];
				$location = @$this->getLocation($address);
				if ($location) {
					$data_array = array(
						'id' => $v['id'],
						'latitude' => $location->lat,
						'longitude' => $location->lng
					);
					$shop->update($data_array);
                    //p($data_array);
				} else {
					$address2 = $v['province'] . $v['city'] . $v['area'];
					$location2 = @$this->getLocation($address2);
                    //p($address2);
                    //p($location2);
					if ($location2) {

						$data_array2 = array(
							'id' => $v['id'],
							'latitude' => $location2->lat,
							'longitude' => $location2->lng
						);
						$shop->update($data_array2);
                        //p($address2);
					}
				}
			}
		}
	}

    //获取坐标地址
	public function getLocation($address)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://apis.map.qq.com/ws/geocoder/v1/?address=" . $address . "&key=QK4BZ-IK3WX-KEN4Z-TSU5F-6ZICK-QDFNQ");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		$map = json_decode($data);
		return $map->result->location;
	}

	public function gettoKm()
	{
		$mapKey = config('mapKey');
		$param = 'mode=walking&from=30.59276,114.30525&to=30.593189,114.30027&key=' . $mapKey;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://apis.map.qq.com/ws/distance/v1/?' . $param);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		$map = json_decode($data);
		return $map;
	}

    //编辑管家信息

	public function shopupdate()
	{
		$active = "shoplist";
		$sid = input('sid');
		$shop = model('shop');
		$shopinfo = $shop->where('id', $sid)->find();

		$id1 = Db::name('media')->where('uid', $shopinfo['uid'])->where('type', 'ID0')->value('src');
		$id2 = Db::name('media')->where('uid', $shopinfo['uid'])->where('type', 'ID1')->value('src');
		$license1 = Db::name('media')->where('uid', $shopinfo['uid'])->where('type', 'license2')->value('src');
		$license2 = Db::name('media')->where('uid', $shopinfo['uid'])->where('type', 'license3')->value('src');

		return view('', ['active' => $active, 'shopinfo' => $shopinfo, 'id1' => $id1, 'id2' => $id2, 'license1' => $license1, 'license2' => $license2]);
		return $this->fetch();
	}

	public function updateshop()
	{
		$sid = input('sid');
		$title = input('title');
		$wx_num = input('wx_num');
		$shop = model('shop');
		$find = $shop->where('id', $sid)->find();
		if ($find) {
			$shop->where('id', $sid)->setField('name', $title);
			$shop->where('id', $sid)->setField('wx_num', $wx_num);
			$shop->where('id', $sid)->setField('updatetime', time());
			echo 1;
		} else {
			echo 2;
		}
	}


    //更新管家状态
	public function updatemodel()
	{
		$sid = input('sid');
		$shop = Db::name('shop');
		$find = $shop->field('status')->where('id', $sid)->find();

		if ($find['status'] == 1) {
			$selected1 = 'selected';
		} else {
			$selected1 = '';
		}
		if ($find['status'] == 2) {
			$selected2 = 'selected';
		} else {
			$selected2 = '';
		}
		if ($find['status'] == 3) {
			$selected3 = 'selected';
		} else {
			$selected3 = '';
		}

		$html = '
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">更新状态</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger display-hide">
                    <strong>提示：</strong> 正在处理中,请勿关闭窗口！</div>
                <div class="row">
                    <form method="post" id="changestatus">
                    	<input type="hidden" value="' . $sid . '" name="sid" id="sid">
                        <div class="col-md-12 form-group" >
                            <label>更新该管家状态</label>
                            <select class="form-control" name="status">
                                <option ' . $selected1 . ' value="1">审核中</option>
                                <option ' . $selected2 . ' value="2">已通过</option>
                                <option ' . $selected3 . ' value="3">未通过</option>
                            </select>
                        </div>
                        <div class="col-md-12 form-group" >
                            <textarea class="form-control" placeholder="请输入反馈给用户的内容，50个字之内" name="des" id="" cols="20" rows="6"></textarea>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline btn-close">关闭</button>
                <button  type="button" class="btn green btn-sure ladda-button updatebtn" data-spinner-color="#333" data-style="expand-left" >
                    <span class="ladda-label">点击更新</span>
                </button>
            </div>
            <script type="text/javascript">

			    $(document).ready(function () {
			        $(\'.updatebtn\').click(function () {
			            bootbox.confirm("您确定执行该操作吗？", function(result) {
			                if(result){
			                    var sid  = $("#sid").val();
			                    var data = $(\'#changestatus\').serialize();
			                    $.ajax({
			                        type: "POST",
			                        dataType: "json",
			                        url: "../admin/shoplist/update_shop",
			                        data: data,
			                        beforeSend:function(XMLHttpRequest){
			                            App.startPageLoading({message: \'正在更新中....\'});
			                            return true;
			                        },
			                        success: function (msg) {
			                            $(".managestatus"+sid).html(msg.v);
			                            $(".action"+sid).hide();
			                            console.log(msg);
			                            $(\'#ajax\').modal(\'hide\');
			                            App.stopPageLoading();
			                            window.location.reload();
			                        },
			                        error: function (err) {
			                            App.stopPageLoading();
			                            window.location.reload();
			                        }
			                    });
			                }
			            });
			        })
			    })

			</script>
            ';

		echo $html;

	}

	//获取管家订单信息
	public function hourdate($datetem)
	{

		$apimethod2 = 'jingdong.UnionService.queryImportOrdersWithKey'; //订单信息
		$param_json2 = array(
			'unionId' => 2010976028,
			'key' => 'e4209eac5db9f8e2e6f1056c9599afcee00568ef69e47cec38f3f5c4405f433c825f16553405a1e8',
			'time' => $datetem,
			"pageIndex" => 1,
			"pageSize" => 100
		);
		$total = 0;
		$commission = 0;
		$orderTotal = 0;
		$reg = $this->GetZeusApiData($apimethod2, $param_json2);
		$reglist = json_decode($reg, true);
		$c = $reglist['jingdong_UnionService_queryImportOrdersWithKey_responce']['queryImportOrdersWithKey_result'];
		$cc = json_decode($c, true);
		$datas = @$cc['data'];
		if ($datas) {
			$orderTotal = sizeof($cc['data']);  //订单总数
			foreach ($datas as $k => $v) {
				$total += $v['totalMoney'];
				$commission += $v['commission'];
			}
		}

		$onnhourdate = array(0, 0, 0);
		$total_ygyj = $commission;
		$total_ddl = $orderTotal;
		$total_xse = $total;

		$onnhourdate[0] = $total_ygyj;
		$onnhourdate[1] = $total_ddl;
		$onnhourdate[2] = $total_xse;

		return $onnhourdate;

	}

	//获取前后几天具体日期

	public function fdate($num)
	{

		$date = date('Ymd', time() - $num * 24 * 3600);
		return $date;

	}


	//获取某一天24小时的订单

	public function oneday($date)
	{

		$onedaydate = array(0, 0, 0);

		for ($i = 0; $i < 24; $i++) {

			if ($i < 10) {
				$h = '0' . $i;
			} else {
				$h = $i;
			}

			$date2 = $date . $h;

			$onehourdate = $this->hourdate($date2);

			$onedaydate[0] += sprintf("%.2f", $onehourdate[0]);
			$onedaydate[1] += sprintf("%.2f", $onehourdate[1]);
			$onedaydate[2] += sprintf("%.2f", $onehourdate[2]);

		}

		return $onedaydate;

	}


	//获取一个时间段的佣金

	public function money($num)
	{

		$duandate = array(0, 0, 0);
		for ($i = 0; $i <= $num; $i++) {

			$todate = $this->fdate($i);                   //获取每天的日期
			$onedaydate = $this->oneday($todate);      //获取当天的佣金

			$duandate[0] += sprintf("%.2f", $onedaydate[0]);
			$duandate[1] += sprintf("%.2f", $onedaydate[1]);
			$duandate[2] += sprintf("%.2f", $onedaydate[2]);

		}

		return $duandate;

	}

	public function updateqx()
	{

		$sid = input('sid');
		$html = '
            <div class="modal-body">
                <div class="alert alert-danger display-hide">
                    <strong>提示：</strong> 正在处理中,请勿关闭窗口！</div>
                <div class="row">
                	<input type="hidden" value="' . $sid . '" name="sid" id="sid">
                    <p>是否需要升级团长？</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline btn-close">关闭</button>
                <button  type="button" class="btn green btn-sure ladda-button updatebtn" data-spinner-color="#333" data-style="expand-left"  data-dismiss="modal" >
                    <span class="ladda-label">确定</span>
                </button>
            </div>
            <script type="text/javascript">

			    $(document).ready(function () {
			        $(\'.updatebtn\').click(function () {
	                    var sid  = $("#sid").val();
	                    $.ajax({
	                        type: "POST",
	                       	url: "../admin/shoplist/update_tuanzhang/?sid="+sid,
	                        data: "",
	                        beforeSend:function(XMLHttpRequest){
	                            App.startPageLoading({message: \'正在更新中....\'});
	                            return true;
	                        },
	                        success: function (msg) {
	                        	if(msg == 1){
	                        		$(".uptitle"+sid).text("升级中...");
	                        		window.location.reload();
	                        	}
	                            return false;
	                        },
	                        error: function (err) {
	                            alert("异常操作");
	                        },
	                        complete:function(){
	                        	App.stopPageLoading();
	                        }
	                    });
			        })
			    })

			</script>
            ';
		echo $html;

	}


	public function upshop()
	{

		$sid = input('sid');
		$html = '
            <div class="modal-body">
                <div class="alert alert-danger display-hide">
                    <strong>提示：</strong> 正在处理中,请勿关闭窗口！</div>
                <div class="row">
                	<input type="hidden" value="' . $sid . '" name="sid" id="sid">
                    <p>是否需要升级该店长？</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline btn-close">关闭</button>
                <button  type="button" class="btn green btn-sure ladda-button updateshopbtn" data-spinner-color="#333" data-style="expand-left"  data-dismiss="modal" >
                    <span class="ladda-label">确定</span>
                </button>
            </div>
            <script type="text/javascript">

			    $(document).ready(function () {
			        $(\'.updateshopbtn\').click(function () {
	                    var sid  = $("#sid").val();
	                    $.ajax({
	                        type: "POST",
	                       	url: "../admin/shoplist/update_shopss/?sid="+sid,
	                        data: "",
	                        beforeSend:function(XMLHttpRequest){
	                            App.startPageLoading({message: \'正在更新中....\'});
	                            return true;
	                        },
	                        success: function (msg) {
	                        	if(msg == 1){
	                        		$(".upshop"+sid).text("升级中...");
	                        		window.location.reload();
	                        	}
	                            return false;
	                        },
	                        error: function (err) {
	                            alert("异常操作");
	                        },
	                        complete:function(){
	                        	App.stopPageLoading();
	                        }
	                    });
			        })
			    })

			</script>
            ';
		echo $html;

	}



	//店长升级
	public function update_shopss()
	{
		$data = input();

		$sid = $data['sid'];

		$tuanzhang = Db::name('shop');
		$update = ['isup' => 1];

		$res = $tuanzhang->where('id', $sid)->update($update);

		// echo $res;die;
		if ($res) {
			echo 1;
			exit;
		}
	}


    //团长升级
	public function update_tuanzhang()
	{
		$data = input();
		$sid = $data['sid'];
        // echo $sid;die;
		$tuanzhang = DB::name('shop');
		$update = ['checked' => 1];
		$res = $tuanzhang->where('id', $sid)->update($update);
        // echo $res;die;
		if ($res) {
			echo 1;
			exit;
		}
	}



	//導入商品
	public function import_shoplist(Request $request)
	{
		// $excel = input('file.imfile');
        $execl  = $request->file('imfile'); //接收文件
		$info = $excel->validate(['ext' => 'xlsx'])->move(ROOT_PATH . 'public' . DS . 'excel');
		import('PHPExcel.PHPExcel', EXTEND_PATH);
		$table_name = Db::name('shop');

		if ($info) {

			$exclePath = $info->getSaveName();  //获取文件名
			$file_path = ROOT_PATH . 'public' . DS . 'excel' . DS . $exclePath;   //上传文件的地址

			$reader = \PHPExcel_IOFactory::createReader('Excel2007'); //读取 excel 文档
			$PHPExcel = $reader->load($file_path, 'utf-8'); // 文档名称
			$excel_array = $PHPExcel->getsheet(0)->toArray();   //转换为数组格式
			// p($excel_array);exit;
			array_shift($excel_array);  //删除第一个数组(标题);
			// p($excel_array);exit;
			for ($i=0; $i < sizeof($excel_array); $i++) {

				$data = [
                    // 'id' => $excel_array[$i][0],
					'id'           => 'null',
					'uid'          => $excel_array[$i][1],
					'shopname'     => $excel_array[$i][2],
					'gender'       => $excel_array[$i][3],
					'phone'        => $excel_array[$i][4],
					'shopers'      => $excel_array[$i][5],
					'nearmec'      => $excel_array[$i][6],
					'location'     => $excel_array[$i][7],
					'g_nature'     => $excel_array[$i][8],
					'helper'       => $excel_array[$i][9],
					'p_nums'       => $excel_array[$i][10],
					'createTime'   => date('Y-m-d H:m:s',time())
				];
				$table_name ->insert($data);
			}
			// p($data);die;
			$msg = ['code' => 200, 'msg' => '导入完成，为您刷新页面'];
			echo json_encode($msg);
		} else {
			$msg = ['code' => 400, 'msg' => '导入失败'];
			echo json_encode($msg);
		}

	}





	//更新管家列表ID
	public function update_id()
	{
		$shop = Db::name('shop')->field('uid,id')->where('status', 2)->select();
		if ($shop) {
			foreach ($shop as $v) {
				$followid = Db::name('customers')->where('id', $v['uid'])->value('followid');

				if ($v['id'] != $followid) {
					echo $v['id'] . '<==>' . $followid . '<br/>';
					Db::name('customers')->where('id', $v['uid'])->update(['followid' => $v['id']]);
				} else {

				}
			}
		}
		echo 'end';
	}

    //更新店长区域划分 --  region

	public function changeregion()
	{
		$shop = Db::name('shop')->field('uid,id,province,city,area,region')->select();
		if ($shop) {
			foreach ($shop as $v) {

				$pro = mb_substr($v['province'], 0, -1, 'utf-8');
				$cit = $v['city'];
				$are = $v['area'];

				if ($v['area'] == "市辖区") {

					$are = $cit;
				}
				$region = Db::name('area')->where('s', $pro)->where('c', $cit)->where('a', $are)->value('l');

				if ($v['region'] != "") {

				} else {
//                    if($v['city'] == "成都市" ){
//                        Db::name('shop')->where('id',$v['id'])->update(['region' => '西南']);
//                    }
					p($v['id'] . '---' . $v['province'] . $v['city'] . $v['area']);
					p($region);
                    //Db::name('shop')->where('id',$v['id'])->update(['region' => $region]);
				}

                //p($region);
			}
		}
	}


}







