<?php

/**
 * @Author: zhiwei wu
 * @Date:   2018-12-03 09:58:05
 * @Last Modified by:   可待科技
 * @Last Modified time: 2018-12-20 22:08:19
 */
namespace app\api\controller\v2;

use think\Db;
use think\Controller;
use think\Session;
use think\Request;
use think\Env;
use think\Log;

class Api extends Controller

{
    public function index()
    {
        echo "-------------------";
    }

    //登录
    public function userlogin()
    {
        $mobile     = input('phone');
        $password   = md5(input('password'));
        $data = DB::name('member')->where(['phone' => $mobile, 'password' => $password])->find();
        if($data){
        //审核通过后才可以登录
           if($data['check']==2){
              DB::name('member')->where('id', $data['id'])->update(['last_login_time' => date('Y-m-d H:i:s', time())]);
              return json(['status' => 1, 'data' => ['uid'=>$data['id']], 'msg' => '登录成功']);
            }else{
              return json(['status' => 0, 'data' => '', 'msg' => '等待管理员审核...']);
            }
        }else{
             return json(['status' => 0, 'data' => '', 'msg' => '用户名或密码错误']);
        }
    }

    //注册
    public function rege()
    {
        $data = request()->param();
        if ($data['phone']) {
            $check = db('member')->where('phone', $data['phone'])->find();
          //检测手机号
            if ($check) {
                return json(['status' => 3, 'data' => '', 'msg' => '手机号已被注册']);
            }
        }

        //接收数据
        $pwd = md5($data['password']);
        $insert = ['phone' => $data['phone'], 'password' => $pwd, 'username' => $data['username'],'region' => $data['region'], 'regester_time' => date('Y-m-d H:i:s', time())];
        $res = DB::name('member')->insert($insert);
        if ($res) {
            return json(['status' => 1, 'data' => '', 'msg' => '注册成功']);
        } else {
            return json(['status' => 0, 'data' => '', 'msg' => '注册失败']);
        }
    }


    //添加商家
    public function addshoper(Request $request)
    {
        $data    = $request->param();//接收所有数据
        $pos     = $data['location'].$data['town'];
        $type    = input('shopid');

        // if($type=='add'){
           if($pos){
           //获取店铺的经纬度
            $location = @$this->getLocation($pos);

            if ($location) {

                $latitude = $location->lat; //经度
                $longitude = $location->lng; //纬度
            }
        }
    // }


        $data_array = [
            'uid'           => $data['uid'],       //商户id
            'username'      => $data['username'],  //个人姓名
            'avatar'        => $data['avatar'],    //头像
            'gender'        => $data['gender'],    //性别
            'yjname'        => $data['yjname'],    //性别
            'shopname'      => $data['shopname'],  //店铺名称
            'phone'         => $data['phone'],     //联系方式
            'shopers'       => $data['shopers'],   //店铺人群
            'nearmec'       => $data['nearmec'],   //附近设备
            'unionid'       => $data['unionid'],   //联盟id
            'location'      => $data['location'],  //店铺位置
            'town'          => $data['town'],      //详细地址
            'region'        => $data['region'],    //所属区域
            'g_nature'      => $data['g_nature'],  //性格优势
            'b_nature'      => $data['b_nature'],  //性格劣势
            'shoptime'      => $data['shoptime'],  //经营时间
            'helper'        => $data['helper'],    //配合程度
            'peoples'       => $data['peoples'],   //社群人数
            'p_nums'        => $data['p_nums'],    //社群个数
            'pics'          => $data['pics'],      //图片
            'latitude'      => $latitude ? $latitude : '',          //经度
            'longitude'     => $longitude          //纬度
        ];
        if($type=='add'){

            $res = db('shop')->insert($data_array);
        }else{
            $res = db('shop')->where('uid',$data['uid'])->update($data_array);
        }
        if ($res) {
            return json(['status' => 1, 'msg' => '操作成功']);
        } else {
            return json(['status' => 0, 'msg' => '操作失败']);
        }

    }

    //获取店铺详情
   public function shopinfo()
{
    $lat        = input('lat');
    $lon        = input('lon');
    $shopid     = input('shopid');
    $type       = input('types');
    $shop       = DB::name('shop');
    if ($shopid && $type == 1) {

        $shopdata = $shop->field('id,unionid,yjname,location,shopname')->where('id', $shopid)->find();

    } else {

        $shopinfo = $shop->where('id', $shopid)->find();

        if ($shopinfo) {
            $shopinfo['pics']  = json_decode($shopinfo['pics'], true);
            //获取距离
            $form = $lat.','.$lon;
            $to   = $shopinfo['latitude'] . ',' . $shopinfo['longitude'];

            $km   = $this->gettoKm($form,$to);
            if($km->status == 0){
                 $d    = $km->result->elements;
                 $distance = $d[0]->distance;
            }else{
                $distance = 0;
            }

            //处理图片
            // unset($shopinfo['pics']);
            // for ($i = 0; $i < count($imgs); $i++) {
            //     $shopinfos[] = $imgs[$i];
            // }
            // foreach ($imgs as $key => $sss) {
            //     $shopinfos[] = config('website').$sss;
            // }

            // $shopinfo['avatar'] = config('website') . $shopinfo['avatar'];
            // $shopinfo['pics']   = $shopinfos;
            $cps_order  = db::name('cps_order')->where('unionid', $shopinfo['unionid'])->order('createtime asc')->select();
            $cps_pingou = db::name('cps_pingou')->where('unionid', $shopinfo['unionid'])->order('createtime asc')->select();
            $shopdata   = array_merge($shopinfo, ['cps_order' => $cps_order], ['cps_pingou' => $cps_pingou],['distance'=>$distance]);
        }
    }

    if (isset($shopdata)) {
        return json(['status' => 1, 'data' => $shopdata, 'msg' => '操作成功']);
    } else {
        return json(['status' => 0, 'data' => '', 'msg' => '操作失败']);
    }

}

    //获取附近店铺地图信息
    public function nearbyshop()
    {
        $region  = input('region');//区域
        $shop = Db::name('shop');
        $allshop = $shop->where('region',$region)->select(); //展示所有店铺
        foreach ($allshop as $k => $v) {
            //判断是否有联盟id
            $res  = DB::name('shop')->where('unionid', $v['unionid'])->find();


            if (0==$res['unionid'] || 'null'==$res['unionid']) {
                $v['iconPath']      =  "http://jd3.kissneck.com/jdcrm/public/upload/image/markers.png";
                $callout = [
                    'content'       => '' . $v['shopname'] . '-- 点击进入店铺',
                    'fontSize'      => 12,
                    'color'         => '#333333',
                    'bgColor'       => '#ffffff',
                    'padding'       => 10,
                    'borderRadius'  => 6,
                    'boxShadow'     => '4px 8px 16px 0 rgba(0,0,0,0.6)',
                    'textAlign'     => 'center',
                    'display'       => "BYCLICK"
                ];
            } else {
                $v['iconPath']      =  "http://jd3.kissneck.com/jdcrm/public/upload/image/markers12.png";
                $callout = [
                    'content'       => '' . $v['shopname'] . '-- 点击进入店铺',
                    'fontSize'      => 12,
                    'color'         => '#333333',
                    'bgColor'       => '#ffffff',
                    'padding'       => 10,
                    'borderRadius'  => 6,
                    'boxShadow'     => '4px 8px 16px 0 rgba(0,0,0,0.6)',
                    'textAlign'     => 'center',
                    'display'       => "BYCLICK"
                ];
            }

            $list[] = [
                'shopid'            => $v['id'],
                'id'                => $v['id'],
                'uid'               => $v['uid'],
                'latitude'          => $v['latitude'],
                'longitude'         => $v['longitude'],
                'iconPath'          => $v['iconPath'],
                'width'             => 40,
                'height'            => 40,
                'callout'           => $callout
            ];

        }
        echo json_encode($list);
    }


    //页面搜索
    public function searchlist()
    {
        $search = trim(input('search'));
        $region = input('region');

        if ($search) {
            $datas = Db::table('y_shop')
                ->where('unionid|shopname|yjname|location', 'like', '%' . $search . '%')
                ->select();
        }

        if ($datas) {

            $infos = Db::table('y_shop')
                ->where('region',$region)
                ->where('unionid|shopname|yjname|location', 'like', '%' . $search . '%')
                ->select();
            if(!$infos){
               return json(['status'=>2,'msg'=>'不属于该区域']);
            }else{
               $list = $infos;
            }
        }

        if(!isset($list)){
           return json(['status' => 2, 'msg' => '未搜索到任何内容']);die;
        }

        if ($list) {
            foreach ($list as $k => $v) {
            //判断是否有联盟id
                $res = Db::name('shop')->where('unionid', $v['unionid'])->find();
                if (0==$res['unionid'] || 'null'==$res['unionid']) {
                    $v['iconPath']      =  "http://jd3.kissneck.com/jdcrm/public/upload/image/markers.png";
                    $callout = [
                        'content'       => '' . $v['shopname'] . '-- 点击进入店铺',
                        'fontSize'      => 12,
                        'color'         => '#333333',
                        'bgColor'       => '#ffffff',
                        // 'iconPath'      => "http://jd3.kissneck.com/jdcrm/public/upload/image/9947.png",
                        'padding'       => 10,
                        'borderRadius'  => 6,
                        'boxShadow'     => '4px 8px 16px 0 rgba(0,0,0,0.6)',
                        'textAlign'     => 'center',
                        'display'       => "BYCLICK"
                    ];

                } else {
                    $v['iconPath']      =  "http://jd3.kissneck.com/jdcrm/public/upload/image/markers12.png";
                    $callout = [
                        'content'       => '' . $v['shopname'] . '-- 点击进入店铺',
                        'fontSize'      => 12,
                        'color'         => '#333333',
                        'bgColor'       => '#ffffff',
                        // 'iconPath'      => "http://jd3.kissneck.com/jdcrm/public/upload/image/9947.png",
                        'padding'       => 10,
                        'borderRadius'  => 6,
                        'boxShadow'     => '4px 8px 16px 0 rgba(0,0,0,0.6)',
                        'textAlign'     => 'center',
                        'display'       => "BYCLICK"
                    ];
                }
                $data[] = [
                    'shopid'            => $v['id'],
                    'id'                => $v['id'],
                    'uid'               => $v['uid'],
                    'unionid'           => $v['unionid'],
                    'latitude'          => $v['latitude'],
                    'iconPath'          => $v['iconPath'],
                    'longitude'         => $v['longitude'],
                    'width'             => 40,
                    'height'            => 40,
                    'callout'           => $callout
                ];
            }
        }

        if ($data) {
            return json($data);
        }else{
            return json([]);
        }
    }


     //获取坐标地址
    public function getLocation($address)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://apis.map.qq.com/ws/geocoder/v1/?address=" . $address . "&key=QK4BZ-IK3WX-KEN4Z-TSU5F-6ZICK-QDFNQ");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $map = json_decode($data);
        return $map->result->location;
    }

    //获取距离
    public function gettoKm($form,$to)
    {
        $mapKey = config('mapKey');
        $param = 'mode=walking&from='.$form.'&to='.$to.'&key='.$mapKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://apis.map.qq.com/ws/distance/v1/?'.$param);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $map = json_decode($data);
        return $map;
    }


    //小程序图片处理接口
    public function saveImg()
    {

        $file = request()->file('uploadimg'); //接收文件
        //创建图片存放位置
        $upload_dir = ROOT_PATH . 'public/upload/images/';
        if (!file_exists($upload_dir)) {
            mkdir($upload_dir, 0755, true);
        }
        // 移动到框架应用根目录/public/uploads/ 目录下
        if ($file) {
            $info = $file->rule('date')->move($upload_dir);
             if ($info) {
                //获取文件名
                $image_name = $info->getSaveName();
                $fileurl = config('website').'/upload/images/'. $image_name;
                return  $fileurl;
            }
        }else{
            echo "错误";
        }
    }

    //我的中心
    public function myMsg()
    {
       $info = DB::name('member')->where('id',input('uid'))->find();
       if ($info) {
            return json(['status' => 1, 'data' => $info, 'msg' => '操作成功']);
        } else {
            return json(['status' => 0, 'data' => '', 'msg' => '操作失败']);

        }
    }

    //显示商家列表
    public function shopList()
    {
        $uid = input('uid');
        $list = db::name('shop')->where('uid',$uid)->order('createTime desc')->select();
        if($list){
          return json($list);
        }else{
          return json([]);
        }
    }

    //获取区域
    public function getAccountArea()
    {
        $region = db::name('member')->where('id',input('uid'))->value('region');
        if($region){
            return json($region);
        }else{
            return json([]);
        }
    }

    public function addips()
    {
        $list = db::name('shop')->select();
        if($list){
            $list['pics']=json_decode($list['pics'],true);
            // foreach ($list as $key => $bb) {
            //     $c[] = config('website').$bb;
            // }
        }
    }



}
