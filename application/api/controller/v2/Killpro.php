<?php
/**
 * Created by PhpStorm.
 * User: fengxiaolei
 * Date: 2018/9/25
 * Time: 下午1:52
 */

namespace app\api\controller\v2;


use think\Controller;
use think\Db;


//秒杀商品接口
class Killpro extends Controller
{
    private $appKey2 = 'D427A3165B2E0DA468BDCCC913EA1E85';       //  京东联盟---Key
    private $appScret2 = '7fef4884a491445f99267868093e56e8';       //  京东联盟---Secret
    private $app_token_json2 = '{e1fa55ff-1086-427f-aaa3-840d022630ca}'; //  第一次需要手动授权获取京东联盟Token然后粘贴到这里

    public function timelist()
    {
        $hcurrTime = date('H',time());  //获取当前的小时

        if( $hcurrTime < 06 && $hcurrTime >= 01 ){
            $hcurrTime = 24;
        }
        if($hcurrTime%2 == 0){ $hTime = $hcurrTime;  }else{ $hTime = $hcurrTime-1; } //小时是否符合偶数

        //每次秒杀只显示5个，遍历判断显示
        for ( $i = 0; $i < 5; $i++ ){

            $tlist = ($hTime+2*$i);

            if( $tlist-$hcurrTime <= 0){
                $title = '抢购中';
            }else{
                $title = '即将开始';
            }

            if($tlist == 24){
                $tlist = '00';
            }else if( $tlist < 10 && $tlist > 0 ) {
                $tlist = '0'.$tlist;
            }else if($tlist > 24 && $tlist <= 32 ){
                $tlist = $tlist-20;
                if( $tlist < 10 && $tlist > 0 ) {
                    $tlist = '0'.$tlist;
                }
            }
            $datetime = date('Y/m/d',time()).' '.$tlist.':00';
            $Arg[] = array(
                'time' => $tlist.':00','datetime' => $datetime.':00', 'status' => $title, 'term_id' => $i
            );
        }
        $A['killTime'] = $Arg;
        return json($A);
    }

    public function killlists()
    {
        $tid       = input('tid');
        $paged     = input('paged');
        $datehour  = explode(':',$tid);
        $kill_product = Db::name('kill_product');
        $list = $kill_product->where('status',1)->where('killTime','=',$datehour[0])->order('createtime asc')->page($paged . ',15')->select();
        return json($list);

    }

    //普通商品列表
    public function prolists()
    {
        $tid       = input('tid');
        $paged     = input('paged');
        $datehour  = explode(':',$tid);
        $kill_product = Db::name('products_v2');
        $list = $kill_product->where('isJdSale',1)->order('inSellCount Desc')->page($paged . ',15')->select();
        return json($list);

    }


    public function formmsg()
    {
        $uid        = input('uid');
        $skuid      = input('skuid');
        $formid     = input('formid');

        $kill_product = Db::name('kill_product_formid');

        $arg = [
            'uid'    => $uid,
            'skuid'  => $skuid,
            'formid' => $formid

        ];

        $kill_product->update($arg);
    }

    //秒杀详情
    public function productdetail()
    {
        $skuId  = input('skuId');
        $shopid = input('shopid');

        $products        = Db::name('kill_product');

        $proinfo = $products->where('skuId', $skuId)->find();

        $jdid     = Db::name('shop')->where('id', $shopid)->value('jdid');
        $jdid_new = Db::name('shop')->where('id', $shopid)->value('jdid');
        $default_jdid = config('unionId');
        if (empty($jdid)) {
            $jdid = $default_jdid;
        }
        //获取推广链接
        $apimethod3      = 'jingdong.service.promotion.wxsq.getCodeByUnionId';  //推广链接
        $param_json3     = array(
            'proCont'     => 1,
            'materialIds' => $skuId,
            'unionId'     => $jdid

        );
        $reg2        = $this->GetZeusApiData( $apimethod3, $param_json3 );
        $reglist2    = json_decode( $reg2, true );
        $c3          = $reglist2['jingdong_service_promotion_wxsq_getCodeByUnionId_responce']['getcodebysubunionid_result'];
        $cc3         = json_decode( $c3, true );

        foreach ( $cc3['urlList'] as $url2 ) {
            $u2      =  $url2;
        }

        if(!empty($u2)){
            $u2      = urlencode( $u2 );
            $minUrl2 = '/pages/product/product?wareId=' . $skuId . '&spreadUrl=' . $u2 . '&customerinfo=jdsqgj180601';
            $isUser = 1;
        }else{
            $this->logss('推广链接接口信息----',$c3);
            $this->logss('推广链接用户信息----',array('newjdid'=>$jdid_new,'jdid'=>$jdid,'shopid'=>$shopid));
            $minUrl2 = $proinfo['minDetailUrl'];
            $isUser = 0; //是否是用户打标
        }


        $proinfo1 = array(
            'isuerShop'         => $isUser,
            'Shopjdid'          => $jdid,
            'minDetailUrl'      => $minUrl2,
            'wlPrice'           => $proinfo['oriPrice'],
            'secKillPrice'      => $proinfo['secKillPrice'],
            'wlCommissionShare' => $proinfo['commissionShare'],
            'makeMoney'         => $proinfo['makeMoney'],
            'qtty_30'           => $proinfo['qtty_30'],
            'goodComment'       => $proinfo['goodComments']
        );

        echo json_encode($proinfo1);
    }

    public function update_shop($time)
    {
        $uid          = input('uid');
        $skuid        = input('skuid');
        $formid       = input('formid');
        $kill_product        = Db::name('kill_product');
        $kill_product_formid = Db::name('kill_product_formid');

        if($time == "550"){
            $datehour = '06';
        }
        else if($time == "750"){
            $datehour = '08';
        }
        else if($time == "950"){
            $datehour = '10';
        }
        else if($time == "1150"){
            $datehour = '12';
        }
        else if($time == "1350"){
            $datehour = '14';
        }
        else if($time == "1550"){
            $datehour = '16';
        }
        else if($time == "1750"){
            $datehour = '18';
        }
        else if($time == "1950"){
            $datehour = '20';
        }
        else if($time == "2150"){
            $datehour = '22';
        }else if($time == "2350"){
            $datehour = '00';
        }

        $list = $kill_product->field('skuId')->where('status',1)->where('killTime','=',$datehour)->order('createtime asc')->select();

        foreach ( $list as $k => $v ){

        }

        $arg = [
            'uid'    => $uid,
            'skuid'  => $skuid,
            'formid' => $formid
        ];

        $openId = Db::name('Customers')->where('id', $find['uid'])->value('openId');
        $params  = array(
            'appid'      => $this->appId,
            'secret'     => $this->Scret,
            'grant_type' => 'client_credential'
        );
        $res     = $this->makeRequest( 'https://api.weixin.qq.com/cgi-bin/token?', $params );
        $reqData = json_decode( $res['result'], true );
        if($data['status'] == 2){
            if($data['status'] == 2){
                Db::name('customers')->where('id', $find['uid'])->setField('is_proxy', 1);
            }
            $info = "您的申请已通过";
            $link = "/pages/Mg-index/Mg-index";
        }else{
            $info = "您的申请未通过";
            $link = "/pages/user/user";
        }
        $res  = $data['des'];
        $data_arr    = array(
            'keyword1' => array( "value" => $info ),
            'keyword2' => array( "value" => $res ),
            'keyword3' => array( "value" => date('Y-m-d H:i:s') ),
            'keyword4' => array( "value" => "点击进入查看详情" ),
        );

        $send_params = array(

            'access_token' => $reqData['access_token'],
            'touser'       => $openId,
            'template_id'  => "OrwFYSL4KtVl7jf_W-Ua-8h-2UCP9lg_DUyfA6Hvyto",
            'page'         => $link,
            'form_id'      => $find['formid'],
            'data'         => $data_arr

        );

        $url       = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=" . $reqData['access_token'];
        $send_data = json_encode( $send_params, true );
        $this->send_post( $url, $send_data );

    }

    //发送模板消息通知
    public function send_post( $url, $post_data ) {
        $options = array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type:application/json',
                //header 需要设置为 JSON
                'content' => $post_data,
                'timeout' => 60
                //超时时间
            )
        );

        $context = stream_context_create( $options );
        $result = file_get_contents( $url, false, $context );

        return $result;
    }


    /**
     * 发起http请求
     *
     * @param string $url 访问路径
     * @param array $params 参数，该数组多于1个，表示为POST
     * @param int $expire 请求超时时间
     * @param array $extend 请求伪造包头参数
     * @param string $hostIp HOST的地址
     *
     * @return array    返回的为一个请求状态，一个内容
     */

    function makeRequest( $url, $params = array(), $expire = 0, $extend = array(), $hostIp = '' ) {
        if ( empty( $url ) ) {
            return array( 'code' => '100' );
        }

        $_curl   = curl_init();
        $_header = array(
            'Accept-Language: zh-CN',
            'Connection: Keep-Alive',
            'Cache-Control: no-cache'
        );
        // 方便直接访问要设置host的地址
        if ( ! empty( $hostIp ) ) {
            $urlInfo = parse_url( $url );
            if ( empty( $urlInfo['host'] ) ) {
                $urlInfo['host'] = substr( DOMAIN, 7, - 1 );
                $url             = "http://{$hostIp}{$url}";
            } else {
                $url = str_replace( $urlInfo['host'], $hostIp, $url );
            }
            $_header[] = "Host: {$urlInfo['host']}";
        }

        // 只要第二个参数传了值之后，就是POST的
        if ( ! empty( $params ) ) {
            curl_setopt( $_curl, CURLOPT_POSTFIELDS, http_build_query( $params ) );
            curl_setopt( $_curl, CURLOPT_POST, true );
        }

        if ( substr( $url, 0, 8 ) == 'https://' ) {
            curl_setopt( $_curl, CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $_curl, CURLOPT_SSL_VERIFYHOST, false );
        }
        curl_setopt( $_curl, CURLOPT_URL, $url );
        curl_setopt( $_curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $_curl, CURLOPT_USERAGENT, 'API PHP CURL' );
        curl_setopt( $_curl, CURLOPT_HTTPHEADER, $_header );

        if ( $expire > 0 ) {
            curl_setopt( $_curl, CURLOPT_TIMEOUT, $expire ); // 处理超时时间
            curl_setopt( $_curl, CURLOPT_CONNECTTIMEOUT, $expire ); // 建立连接超时时间
        }

        // 额外的配置
        if ( ! empty( $extend ) ) {
            curl_setopt_array( $_curl, $extend );
        }

        $result['result'] = curl_exec( $_curl );
        $result['code']   = curl_getinfo( $_curl, CURLINFO_HTTP_CODE );
        $result['info']   = curl_getinfo( $_curl );
        if ( $result['result'] === false ) {
            $result['result'] = curl_error( $_curl );
            $result['code']   = - curl_errno( $_curl );
        }

        curl_close( $_curl );

        return $result;
    }

//日志写入

    public function logss($title,$info)
    {
        Log::info($title.json_encode($info));
    }


    public function GetZeusApiData($apiUrl = '', $param_json = array(), $version = '1.0', $get = false)
    {
        $API['access_token'] = $this->josrefreshAccessToken(); //  生成的access_token，30天一换
        $API['app_key'] = $this->appKey2;
        $API['method'] = $apiUrl;
        $API['360buy_param_json'] = json_encode($param_json);
        $API['timestamp'] = date('Y-m-d H:i:s', time());
        $API['v'] = $version;
        ksort($API);    //  排序
        $str = '';      //  拼接的字符串
        foreach ($API as $k => $v) $str .= $k . $v;
        $sign = strtoupper(md5($this->appScret2 . $str . $this->appScret2));    //  生成签名MD5加密转大写
        if ($get) {
            //  用get方式拼接URL
            $url = "https://api.jd.com/routerjson?";
            foreach ($API as $k => $v)
                $url .= urlencode($k) . '=' . $v . '&';  //  把参数和值url编码
            $url .= 'sign=' . $sign;
            $res = self::curl_get($url);
        } else {
            //  用post方式获取数据
            $url = "https://api.jd.com/routerjson?";
            $API['sign'] = $sign;
            $res = self::curl_post($url, $API);
        }
        return $res;
    }

    //  刷新accessToken
    private function josrefreshAccessToken()
    {
        //$filePath = ROOT_PATH.'ZeusToken.config';
        $filePath = ROOT_PATH . '/ZeusToken.config';   //测试
        if (file_exists($filePath)) {
            $handle = fopen($filePath, 'r');
            $tokenJson = fread($handle, 8142);
        } else {
            //  插入默认的token
            fwrite(fopen($filePath, 'w'), $this->app_token_json2);
            $tokenJson = $this->app_token_json2;
        }

        if (substr($tokenJson, 0, 3) == pack('CCC', 0xef, 0xbb, 0xbf)) {
            $tokenJson = substr($tokenJson, 3);
        }
        $res = json_decode(trim($tokenJson), true);   //解析不了可能是文本出了问题
        //  判断
        if ($res['code'] == 0) {
            if ($res['expires_in'] * 1000 + $res['time'] < self::getMillisecond() - 86400000) {
                //access_token失效前一天
                //获取刷新token的url
                $refreshUrl = "https://oauth.jd.com/oauth/token?";
                $refreshUrl .= '&client_id=' . $this->appKey2;
                $refreshUrl .= '&client_secret=' . $this->appScret2;
                $refreshUrl .= '&grant_type=refresh_token';
                $refreshUrl .= '&refresh_token=' . $res['refresh_token'];
                //  获取新的token数据
                $newAccessTokenJson = self::curl_get($refreshUrl);
                //  写入文本
                fwrite(fopen($filePath, 'w'), $newAccessTokenJson);
                //  解析成数组
                $newAccessTokenArr = json_decode($newAccessTokenJson, true);
                $accessToken = $newAccessTokenArr['access_token'];
            } else {
                $accessToken = $res['access_token'];
            }
            return $accessToken;
        } else {
            //  如果refresh_token过期，将会返回错误码code:2011;msg:refresh_token过期
            return $res['msg'];
        }
    }

    //  get请求
    private static function curl_get($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    //  post请求
    private static function curl_post($url, $curlPost)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    //  获取13位时间戳
    private static function getMillisecond()
    {
        list($t1, $t2) = explode(' ', microtime());
        return sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }

}