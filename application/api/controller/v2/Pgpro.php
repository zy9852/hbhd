<?php
/**
 * Created by PhpStorm.
 * User: fengxiaolei
 * Date: 2018/9/26
 * Time: 2:00 PM
 */

namespace app\api\controller\v2;


use think\Controller;
use think\Db;
use think\log;

//拼购商品接口

class Pgpro extends Controller
{

    private $appKey2 = 'D427A3165B2E0DA468BDCCC913EA1E85';       //  京东联盟---Key
    private $appScret2 = '7fef4884a491445f99267868093e56e8';       //  京东联盟---Secret
    private $app_token_json2 = '{e1fa55ff-1086-427f-aaa3-840d022630ca}'; //  第一次需要手动授权获取京东联盟Token然后粘贴到这里

    //一元来拼列表
    public function onePingpro()
    {

        $paged   = input('paged');
        $shop    = Db::name('products_v2_pingou');
        $list    = $shop->order('inSellCount desc')->page($paged.',10')->select();


        $Bgoods_banner = DB::name('media')->where('type','B1')->value('src');
        $Cgoods_banner = DB::name('media')->where('type','C1')->value('src');
        $li = array(
            'list'   =>$list,
            'Bphoto' => $Bgoods_banner,
            'Cphoto' => $Cgoods_banner
        );

        return json($li);
    }

    //检测是否是店长
    public function isusershop()
    {
        $data = input();
        $uid = base64_decode($data['uid']);
        $shopinfo = Db::name('shop')->field('id')->where('status', 2)->where('uid', $uid)->find();

        if ($shopinfo) {
            echo $shopinfo['id'];
        } else {
            echo 0;
        }
    }

    public function productdetail()
    {
        $skuId  = input('skuId');
        $shopid = input('shopid');
        $isshop = input('isshop');

        $products        = Db::name('products_v2_pingou');

        $proinfo = $products->where('skuid', $skuId)->find();

        $couponUrl = $proinfo['coupon_link'];

        $jdid     = Db::name('shop')->where('id', $shopid)->value('jdid');
        $jdid_new = Db::name('shop')->where('id', $shopid)->value('jdid');
        $default_jdid = config('unionId');
        if (empty($jdid)) {
            $jdid = $default_jdid;
        }

        $apimethod2 = 'jingdong.service.promotion.coupon.getCodeByUnionId'; //二合一推广链接
        $param_json2 = array('couponUrl' => $couponUrl,
            'materialIds' => $skuId,
            'unionId' => $jdid
        );
        $reg = $this->GetZeusApiData($apimethod2, $param_json2);
        $reglist = json_decode($reg, true);
        if($reglist) {
            $c = @$reglist['jingdong_service_promotion_coupon_getCodeByUnionId_responce']['getcodebyunionid_result'];
            $cc = json_decode($c, true);

            if ($cc['resultCode'] == 0 && $cc['resultMessage'] == '获取代码成功') {
                $union_url = $cc['urlList'];
                foreach ($union_url as $u) {
                    $url = $u;
                }
            }
        }
        if (!empty($url)) {
            $u = urlencode($url);
            $minUrl2 = '/pages/jingfen_twotoone/item?spreadUrl=' . $u . '&customerinfo=jdsqgj180601';
            $isUser = 1;
        } else {
            $this->logss('优惠劵二合一接口信息----',$reglist);
            $this->logss('优惠劵二合一用户信息----',array('newjdid'=>$jdid_new,'jdid'=>$jdid,'shopid'=>$shopid,'couponUrl'=>$couponUrl));
            //第三步获取推广链接
            $apimethod3      = 'jingdong.service.promotion.wxsq.getCodeByUnionId';  //推广链接
            $param_json3     = array(
                'proCont'     => 1,
                'materialIds' => $skuId,
                'unionId'     => $jdid

            );
            $reg2        = $this->GetZeusApiData( $apimethod3, $param_json3 );
            $reglist2    = json_decode( $reg2, true );
            $c3          = $reglist2['jingdong_service_promotion_wxsq_getCodeByUnionId_responce']['getcodebysubunionid_result'];
            $cc3         = json_decode( $c3, true );

            foreach ( $cc3['urlList'] as $url2 ) {
                $u2      =  $url2;
            }

            if(!empty($u2)){
                $u2      = urlencode( $u2 );
                $minUrl2 = '/pages/product/product?wareId=' . $skuId . '&spreadUrl=' . $u2 . '&customerinfo=jdsqgj180601';
                $isUser = 1;
            }else{
                $this->logss('推广链接接口信息----',$reglist2);
                $this->logss('推广链接用户信息----',array('newjdid'=>$jdid_new,'jdid'=>$jdid,'shopid'=>$shopid));
                $minUrl2 = $proinfo['minDetailUrl'];
                $isUser = 0; //是否是用户打标
            }
        }


        $shareUrl = $this->getCompositePgImage($skuId,$isshop);

        $proinfo1 = array(
            'isuerShop'         => $isUser,
            'Shopjdid'          => $jdid,
            'minDetailUrl'      => $minUrl2,
            'wlPrice'           => $proinfo['wlPrice'],
            'pingouPrice'       => $proinfo['pingouPrice'],
            'pingouTmCount'     => $proinfo['pingouTmCount'],
            'wlCommissionShare' => $proinfo['wlCommissionShare'],
            'makeMoney'         => $proinfo['makeMoney'],
            'goodComment'       => $proinfo['goodComment'],
            'shareUrl'          => $shareUrl
        );

        echo json_encode($proinfo1);
    }


    //日志写入

    public function logss($title,$info)
    {
        Log::info($title.json_encode($info));
    }


    public function GetZeusApiData($apiUrl = '', $param_json = array(), $version = '1.0', $get = false)
    {
        $API['access_token'] = $this->josrefreshAccessToken(); //  生成的access_token，30天一换
        $API['app_key'] = $this->appKey2;
        $API['method'] = $apiUrl;
        $API['360buy_param_json'] = json_encode($param_json);
        $API['timestamp'] = date('Y-m-d H:i:s', time());
        $API['v'] = $version;
        ksort($API);    //  排序
        $str = '';      //  拼接的字符串
        foreach ($API as $k => $v) $str .= $k . $v;
        $sign = strtoupper(md5($this->appScret2 . $str . $this->appScret2));    //  生成签名MD5加密转大写
        if ($get) {
            //  用get方式拼接URL
            $url = "https://api.jd.com/routerjson?";
            foreach ($API as $k => $v)
                $url .= urlencode($k) . '=' . $v . '&';  //  把参数和值url编码
            $url .= 'sign=' . $sign;
            $res = self::curl_get($url);
        } else {
            //  用post方式获取数据
            $url = "https://api.jd.com/routerjson?";
            $API['sign'] = $sign;
            $res = self::curl_post($url, $API);
        }
        return $res;
    }

    //  刷新accessToken
    private function josrefreshAccessToken()
    {
        //$filePath = ROOT_PATH.'ZeusToken.config';
        $filePath = ROOT_PATH . '/ZeusToken.config';   //测试
        if (file_exists($filePath)) {
            $handle = fopen($filePath, 'r');
            $tokenJson = fread($handle, 8142);
        } else {
            //  插入默认的token
            fwrite(fopen($filePath, 'w'), $this->app_token_json2);
            $tokenJson = $this->app_token_json2;
        }

        if (substr($tokenJson, 0, 3) == pack('CCC', 0xef, 0xbb, 0xbf)) {
            $tokenJson = substr($tokenJson, 3);
        }
        $res = json_decode(trim($tokenJson), true);   //解析不了可能是文本出了问题
        //  判断
        if ($res['code'] == 0) {
            if ($res['expires_in'] * 1000 + $res['time'] < self::getMillisecond() - 86400000) {
                //access_token失效前一天
                //获取刷新token的url
                $refreshUrl = "https://oauth.jd.com/oauth/token?";
                $refreshUrl .= '&client_id=' . $this->appKey2;
                $refreshUrl .= '&client_secret=' . $this->appScret2;
                $refreshUrl .= '&grant_type=refresh_token';
                $refreshUrl .= '&refresh_token=' . $res['refresh_token'];
                //  获取新的token数据
                $newAccessTokenJson = self::curl_get($refreshUrl);
                //  写入文本
                fwrite(fopen($filePath, 'w'), $newAccessTokenJson);
                //  解析成数组
                $newAccessTokenArr = json_decode($newAccessTokenJson, true);
                $accessToken = $newAccessTokenArr['access_token'];
            } else {
                $accessToken = $res['access_token'];
            }
            return $accessToken;
        } else {
            //  如果refresh_token过期，将会返回错误码code:2011;msg:refresh_token过期
            return $res['msg'];
        }
    }

    //  get请求
    private static function curl_get($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    //  post请求
    private static function curl_post($url, $curlPost)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    //  获取13位时间戳
    private static function getMillisecond()
    {
        list($t1, $t2) = explode(' ', microtime());
        return sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }


    //获取分享卡片合成图
    public function getCompositePgImage($skuID = '32912943324',$isshop = 1)
    {
        $file       = 'pg'.$skuID;
        //判断文件是否存在
        $upload_dir = ROOT_PATH . 'public/upload/share/' . $file;
        if (!file_exists($upload_dir)) {
            mkdir($upload_dir, 0755, true);
        }

        //商品独立文件夹
        $path   = ROOT_PATH . 'public/upload/share/'.$file;
        $url    = config('website').'/upload/share/'.$file;

        //判断是不是店长
        if($isshop == 1){

            $image = \think\Image::open('./imageShop1.png');

        }else{

            $image = \think\Image::open('./imageUser1.png');
        }

        //查询是否存在该商品
        $proinfo = Db::name('products_v2_pingou')->where('skuid', $skuID)->find();

        if( $proinfo ){

            //判断原价和劵后价长度
            $factprice = str_replace('.','',$proinfo['factprice']);
            if(strlen($factprice) >= 4){
                $factprice = $proinfo['factprice'];
            }else{
                $factprice = number_format($proinfo['factprice'],2);
            }

            $wlPrice = str_replace('.','',$proinfo['wlPrice']);
            if(strlen($wlPrice) >= 4){
                $wlPrice = $proinfo['wlPrice'];
            }else{
                $wlPrice = number_format($proinfo['wlPrice'],2);
            }
            $imgURl            = $proinfo['imageurl'];
            $goodCommentsShare = $proinfo['goodCommentsShare'].'%';
            $goodComment       = $proinfo['goodComment'];
            $this->createPriceProductImage('原价: '.$wlPrice,$wlPrice,$path); //创建原价合成图+删除线
        }

        $filename = $path.'/'.$skuID.'.png'; //商品图
        $content  = file_get_contents($imgURl);
        $r        = file_put_contents($filename, $content);

        if($r){

            //中心等比例裁剪商品图
            $Proimage = \think\Image::open($filename);
            $Proimage->thumb(159,160,\think\Image::THUMB_CENTER,'')->save($path.'/thumb.png','png','100',true);

            //合成图一：添加图片

            $img_xy = array(55,67);
            $image->water($path.'/thumb.png',$img_xy)->save($path.'/share.png','png','100',true);

            //合成图二：添加劵后价

            $fprice_xy = array(255,88);
            $image2 = \think\Image::open($path.'/share.png');

            $strArray=str_split($factprice);
            $str=join("",$strArray);

            $image2->text($str,'msyhbd.ttf',30,'#F51B51',$fprice_xy)->save($path.'/share.png','png','100',true);

            //合成图三：添加原价

            $image3 = \think\Image::open($path.'/share.png');
            $wlprice_xy = array(245,128);
            $image3->water($path.'/price.png',$wlprice_xy)->save($path.'/share.png','png','100',true);

            //合成图四：添加好评率

            $goodc_xy = array(160,324);
            $image4 = \think\Image::open($path.'/share.png');
            $image4->text($goodCommentsShare,'msyh.ttf',15,'#ffffff',$goodc_xy)->save($path.'/share.png','png','100',true);

            //合成图五：添加好评数

            $goodn_xy = array(305,324);
            $image5 = \think\Image::open($path.'/share.png');
            $image5->text($goodComment,'msyh.ttf',15,'#ffffff',$goodn_xy)->save($path.'/share.png','png','100',true);

            return $url.'/share.png';
        }else{
            return 2;
        }
    }

    //合成删除线
    public function createPriceProductImage($price,$len,$path)
    {
        //header('Content-Type: image/png');
        $text = $price;

        // 合成删除线
        $im = imagecreatetruecolor(135 , 30);
        $white = imagecolorallocate($im, 248, 248, 248);
        $red = imagecolorallocate($im, 189, 189, 189);
        imagefilledrectangle($im, 0, 0, 180, 40, $white);
        $font = ROOT_PATH.'public/msyh.ttf';
        imagettftext($im, 15, 0, 1, 22, $red, $font, $text);
        if (!empty($price)) {
            $this->addStrikethrough($im, $font, $this->getUnderline($len));
        }
        $path = $path . '/price.png';
        imagepng($im,$path);
        imagedestroy($im);
    }

    //创建删除线
    public function addStrikethrough($image, $font, $underline)
    {
        $grey = imagecolorallocate($image, 189, 189, 189);
        imagettftext($image, 13, 0, 1, 13, $grey, $font, $underline);
    }

    //添加横线
    public function getUnderline($len)
    {
        $wl = strlen($len);
        if($wl == 4){
            $len = 12;
        }
        else if($wl == 5){
            $len = 14;
        }else if($wl == 6){
            $len = 15;
        }
        $underline = '&#95;'; // '&#95;' symbol equal '_'
        $length = $len;
        $complexUnderline = '';
        for ($i = 0; $i < $length; $i++) {
            $complexUnderline = $complexUnderline . $underline;
        }
        return $complexUnderline;
    }
}