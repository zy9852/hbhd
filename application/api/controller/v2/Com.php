<?php

/**
 * Created by PhpStorm.
 * User: fengxiaolei
 * Date: 2018/9/28
 * Time: 10:39 AM
 */

namespace app\api\controller\v2;


use think\Controller;
use think\Db;
use think\log;

class Com extends Controller
{

    private $appKey2 = 'D427A3165B2E0DA468BDCCC913EA1E85';       //  京东联盟---Key
    private $appScret2 = '7fef4884a491445f99267868093e56e8';       //  京东联盟---Secret
    private $app_token_json2 = '{e1fa55ff-1086-427f-aaa3-840d022630ca}'; //  第一次需要手动授权获取京东联盟Token然后粘贴到这里

    public function index()
    {


        echo '=========';

    }

    //搜索商品池
    public function prolist()
    {

        $paged = input('paged');
        $sort = input('sort');
        $by = input('by');
        $shop = Db::name('products_v2');
        if ($by == 5) {
            $list = $shop->where('discount', '>', '0')->order('inSellCount desc')->page($paged . ',10')->select();
        } else {

            if ($by == 1) {
                $order = 'inSellCount';
            } else if ($by == 2) {
                $order = 'factprice';
            } else if ($by == 3) {
                $order = 'views';
            } else if ($by == 4) {
                $order = 'goodComment';
            }

            if ($sort == "true") {
                $so = 'asc';
            } else {
                $so = 'desc';
            }
            $list = $shop->order($order . ' ' . $so)->page($paged . ',10')->select();
        }
        $li = array(
            'list' => $list
        );
        return json($li);

    }

    //B端搜索商品池
    public function Bprolist()
    {

        $paged = input('paged');
        $sort = input('sort');
        $by = input('by');
        $uid = base64_decode(input('uid'));

        $shop = Db::name('products_v2');
        $product_meta = Db::name('product_meta');
        $order = 'inSellCount';
        if ($by == 5) {
            $list = $shop->where('discount', '>', '0')->order('inSellCount desc')->page($paged . ',10')->select();
        } else {

            if ($by == 1) {

                $order = 'inSellCount';

            } else if ($by == 6) {
                $order = 'wlCommissionShare';

            } else if ($by == 7) {

                $order = 'makeMoney';

            } else if ($by == 4) {
                $order = 'goodComment';
            }

            if ($sort == "true") {
                $so = 'asc';
            } else {
                $so = 'desc';
            }
            $list = $shop->order($order . ' ' . $so)->page($paged . ',10')->select();
        }

        if ($list) {

            foreach ($list as $k => $v) {
                $pro_metainfo = $product_meta->where('pid', $v['id'])->where('key', 'isselect2')->value('values');
                if ($pro_metainfo) {
                    $isselect2 = explode(',', $pro_metainfo);

                    if (in_array($uid, $isselect2)) {
                        $select['addBefore'] = false;
                    } else {
                        $select['addBefore'] = true;
                    }
                } else {
                    $select['addBefore'] = true;
                }

                $lists[] = array_merge($list[$k], $select);
            }

            $li = array(
                'list' => $lists
            );

            return json($li);

        } else {
            echo 0;
        }

    }

    //B端关键词搜索
    public function Bsearchclick()
    {
        $products = Db::name('products_v2');
        $product_meta = Db::name('product_meta');

        $search = input('search');
        $uid = base64_decode(input('uid'));
        $paged = input('spaged');
        $sort = input('sort');
        $by = input('by');
        //$shop = Db::name('products_v2');
        if (!empty($search)) {
            //搜索历史记录 (统计店铺访问量);
            $searchinfo = Db::name('search');
            $searchname = $searchinfo->where('sname', $search)->where('uid', $uid)->find();
            if ($searchname) {
                $views = $searchname['views'];
                $searchinfo->where('id', $searchname['id'])->setField('views', $views + 1);
            } else {

                $data = [
                    'uid' => $uid,
                    'sname' => $search,
                    'views' => 1,
                    'isHot' => 0
                ];
                $searchinfo->insert($data);
            }
            //模糊查询(检索商品)

            if ($by == 5) {
                $shopoption = $products->where('discount', '>', '0')->order('inSellCount desc')->page($paged . ',10')->select();
                exit;

            } else {

                if ($by == 1) {
                    $order = 'inSellCount';
                } else if ($by == 2) {
                    $order = 'factprice';
                } else if ($by == 3) {
                    $order = 'views';
                } else if ($by == 4) {
                    $order = 'goodComment';
                } else if ($by == 6) {
                    $order = "wlCommissionShare";
                } else if ($by == 7) {
                    $order = "makeMoney";
                }

                if ($sort == "true") {
                    $so = 'asc';
                } else {
                    $so = 'desc';
                }
                // $list = $products->order($order . ' ' . $so)->page($paged . ',10')->select();
                $shopoption = $products->where('skuName', 'like', '%' . $search . '%')->page($paged . ',10')->order($order . ' ' . $so)->select();

            }

            if ($shopoption) {

                foreach ($shopoption as $k => $v) {
                    $pro_metainfo = $product_meta->where('pid', $v['id'])->where('key', 'isselect2')->value('values');
                    if ($pro_metainfo) {
                        $isselect2 = explode(',', $pro_metainfo);

                        if (in_array($uid, $isselect2)) {
                            $select['addBefore'] = false;
                        } else {
                            $select['addBefore'] = true;
                        }
                    } else {
                        $select['addBefore'] = true;
                    }

                    $lists[] = array_merge($shopoption[$k], $select);
                }

                return json($lists);

            } else {
                echo 0;
            }

        }
    }

    //B端关键词搜索-测试
    public function Bsearchclick_test()
    {
        $products = Db::name('products_v2');
        $product_meta = Db::name('product_meta');
        $cn = input('iscoupon');
        $search = input('search');
        $uid = base64_decode(input('uid'));
        $paged = input('spaged');
        $sort = input('sort');
        $by = input('by');
        //$shop = Db::name('products_v2');
        if ($by == 1) {
            $order = 'inSellCount';
        } else if ($by == 2) {
            $order = 'factprice';
        } else if ($by == 3) {
            $order = 'views';
        } else if ($by == 4) {
            $order = 'goodComment';
        } else if ($by == 6) {
            $order = "wlCommissionShare";
        } else if ($by == 7) {
            $order = "makeMoney";
        }

        if ($sort == "true") {
            $so = 'asc';
        } else {
            $so = 'desc';
        }
        if (!empty($search)) {
            //搜索历史记录 (统计店铺访问量);
            $searchinfo = Db::name('search');
            $searchname = $searchinfo->where('sname', $search)->where('uid', $uid)->find();
            if ($searchname) {
                $views = $searchname['views'];
                $searchinfo->where('id', $searchname['id'])->setField('views', $views + 1);
            } else {

                $data = [
                    'uid' => $uid,
                    'sname' => $search,
                    'views' => 1,
                    'isHot' => 0
                ];
                $searchinfo->insert($data);
            }
            //模糊查询(检索商品)

//            if ($by == 5) {
//                $shopoption = $products->where('discount', '>', '0')->order('inSellCount desc')->page($paged . ',10')->select();
//                exit;
//
//            } else {
//
//                // $list = $products->order($order . ' ' . $so)->page($paged . ',10')->select();
//                $shopoption = $products->where('skuName', 'like', '%' . $search . '%')->page($paged . ',10')->order($order . ' ' . $so)->select();
//
//            }

            if( $cn == "false" ){
                $shopoption = $products->where('skuName', 'like', '%' . $search . '%')->order($order . ' ' . $so)->page($paged . ',10')->select();
            }else{
                $shopoption = $products->where('skuName', 'like', '%' . $search . '%')->where('link','<>','')->page($paged . ',10')->order($order . ' ' . $so)->select();
            }

            if ($shopoption) {

                foreach ($shopoption as $k => $v) {
                    $pro_metainfo = $product_meta->where('pid', $v['id'])->where('key', 'isselect2')->value('values');
                    if ($pro_metainfo) {
                        $isselect2 = explode(',', $pro_metainfo);

                        if (in_array($uid, $isselect2)) {
                            $select['addBefore'] = false;
                        } else {
                            $select['addBefore'] = true;
                        }
                    } else {
                        $select['addBefore'] = true;
                    }

                    $lists[] = array_merge($shopoption[$k], $select);
                }

                return json($lists);

            } else {
                echo 0;
            }

        }
    }
    //删除坑位商品
    public function delPro()
    {

        $uid   = base64_decode(input('uid'));
        $pid   = input('pid');
        $skuid = input('skuid');

        $product_meta = Db::name('product_meta');
        $shop_option = Db::name('shop_option');

        if ($pid != 0) {
            $metainfo = $product_meta->where('pid', $pid)->where('key', 'isselect2')->value('values');
            $uidarg = explode(',', $metainfo);
            $key = array_search($uid, $uidarg);
            array_splice($uidarg, $key, 1);
            if ($uidarg) {
                $changeMeta = [
                    'values' => implode(',', $uidarg)
                ];
                $product_meta->where('pid', $pid)->where('key', 'isselect2')->update($changeMeta);
            } else {
                $product_meta->where('pid', $pid)->where('key', 'isselect2')->delete();
            }
            $shop_option->where('sku', $skuid)->where('uid', $uid)->delete();
        }
    }
    //京东联盟分类获取
    public function cat3()
    {
        //$category = Db::name('product_category')->field('name,term_id')->where('is_delete',1)->select();
        echo '[{"name":"优选","term_id":12},{"name":"生鲜","term_id":16},{"name":"家居日用","term_id":13},{"name":"护肤美妆","term_id":14},{"name":"服饰鞋靴","term_id":15},{"name":"食品饮料","term_id":17},{"name":"母婴&图书","term_id":18},{"name":"出行&户外","term_id":19},{"name":"家用电器","term_id":20},{"name":"数码3C","term_id":21},{"name":"汽车用品","term_id":22},{"name":"宠物生活","term_id":23},{"name":"医药保健","term_id":24},{"name":"钟表","term_id":25}]';
    }


    //检测用户是否关注自己店铺
    public function isfollowinfo()
    {
        $uid = base64_decode(input('uid'));
        $shopid = input('shopid');

        $customers = Db::name('customers');
        $followid = $customers->where('id', $uid)->value('followid');

        $is_proxy = $customers->where('id', $uid)->value('is_proxy');


        if ($followid == $shopid && $is_proxy == 1) {

            echo 'true';

        } else {
            echo $followid;
        }

    }


    //产品列表展示
    public function productlist()
    {
        $paged = input('paged');
        $cids = input('cid');
        $uid = base64_decode(input('uid'));
        $products = Db::name('products_v2');
        if ($cids == 12 || $cids == 0) {
            $list = $products->where('cidname=12')->order('createtime desc')->page($paged . ',10')->select();
        } else {

            //循环显示不同内容5次一轮
            $option   = Db::name('option');
            $nowShow  = $option->where('meta_key','countTime')->value('meta_value');

            if($nowShow == 1){

                $paged = $paged;

            }else if( $nowShow == 2 ){

                $paged = $paged+1;
            }else if( $nowShow == 3 ){

                $paged = $paged+2;

            }else if( $nowShow == 4 ){

                $paged = $paged+3;

            }else if( $nowShow == 5 ){

                $paged = $paged+4;
            }


            $cid = $cids;
            $list = $products->where('cidname=' . $cid . ' AND wlCommissionShare >= 20 AND discount <> 0')->order('inSellCount desc')->page($paged . ',10')->select();



        }
        echo json_encode($list);
    }


    //用来测试数据显示
    public function productlist2()
    {
        $paged = input('paged');
        $cids = input('cid');
        $uid = base64_decode(input('uid'));
        $products = Db::name('products_v2');
        if ($cids == 12 || $cids == 0) {
            $list = $products->where('cidname=12')->order('createtime desc')->page($paged . ',10')->select();
        } else {

            //循环显示不同内容5次一轮
            $option = Db::name('option');
            $nowShow = $option->where('meta_key', 'countTime')->value('meta_value');

            if ($nowShow == 1) {

                $paged = $paged;

            } else if ($nowShow == 2) {

                $paged = $paged + 1;
            } else if ($nowShow == 3) {

                $paged = $paged + 2;

            } else if ($nowShow == 4) {

                $paged = $paged + 3;

            } else if ($nowShow == 5) {

                $paged = $paged + 4;
            }


            $cid = $cids;
            $list = $products->where('cidname=' . $cid . ' AND wlCommissionShare >= 20 AND discount <> 0')->order('inSellCount desc')->page($paged . ',10')->select();

        }
        echo json_encode($list);
    }

    //装修店铺

    public function SelectProduct()
    {

        $paged = input('paged');
        $cids = input('cid');
        $uid = base64_decode(input('uid'));
        $products = Db::name('products_v2');
        $product_meta = Db::name('product_meta');
        if ($cids == 12 || $cids == 0) {
            $list = $products->where('cidname', 12)->order('createtime desc')->page($paged . ',10')->select();
        } else {
            $cid = $cids;
            $list = $products->where('cidname=' . $cid . ' AND wlCommissionShare > 20 AND discount <> 0')->order('inSellCount desc')->page($paged . ',10')->select();
        }

        if ($list) {

            foreach ($list as $k => $v) {
                $pro_metainfo = $product_meta->where('pid', $v['id'])->where('key', 'isselect2')->value('values');
                if ($pro_metainfo) {
                    $isselect2 = explode(',', $pro_metainfo);

                    if (in_array($uid, $isselect2)) {
                        $select['addBefore'] = false;
                    } else {
                        $select['addBefore'] = true;
                    }
                } else {
                    $select['addBefore'] = true;
                }

                $lists[] = array_merge($list[$k], $select);
            }

            return json($lists);

        } else {
            echo $lists = 0;
        }
    }

    //B端搜索

    public function bsearchpro()
    {
        $search = input('search');
        $uid = base64_decode(input('uid'));
        $spaged = input('spaged');

        if (empty($spaged)) {
            $paged = input('searchpaged');
        } else {
            $paged = $spaged;
        }
        if (!empty($search)) {

            $searchinfo = Db::name('search');
            $searchname = $searchinfo->where('sname', $search)->where('uid', $uid)->find();
            if ($searchname) {
                $views = $searchname['views'];
                $searchinfo->where('id', $searchname['id'])->setField('views', $views + 1);
            } else {

                $data = [
                    'uid' => $uid,
                    'sname' => $search,
                    'views' => 1,
                    'isHot' => 0
                ];
                $searchinfo->insert($data);
            }

            $products = Db::name('products_v2');
            $shopoption = $products->where('skuName', 'like', '%' . $search . '%')->page($paged . ',10')->select();

            if ($shopoption) {
                echo json_encode($shopoption);
            } else {
                echo json_encode(array(0));
            }

        }

    }

    //详情页面
    public function productdetail()
    {
        $skuId = input('skuId');
        $shopid = input('shopid');
        $isshop = input('isshop');

        $products = Db::name('products_v2');

        $proinfo = $products->where('skuid', $skuId)->where('cidname', '26')->find();
        if (empty($proinfo['link'])) {
            $proinfo = $products->where('skuid', $skuId)->where('cidname', '12')->find();
            if (empty($proinfo)) {
                $proinfo = $products->where('skuid', $skuId)->find();
            }
        }
        //$proinfo = $products->where('skuid', $skuId)->find();
        $couponUrl = $proinfo['link'];

        $jdid     = Db::name('shop')->where('id', $shopid)->value('jdid');
        $jdid_new = Db::name('shop')->where('id', $shopid)->value('jdid');
        $default_jdid = config('unionId');
        if (empty($jdid)) {
            $jdid = $default_jdid;
        }

        $apimethod2 = 'jingdong.service.promotion.coupon.getCodeByUnionId'; //二合一推广链接
        $param_json2 = array(
            'couponUrl' => $couponUrl,
            'materialIds' => $skuId,
            'unionId' => $jdid
        );
        $reg = $this->GetZeusApiData($apimethod2, $param_json2);
        $reglist = json_decode($reg, true);
        $c = $reglist['jingdong_service_promotion_coupon_getCodeByUnionId_responce']['getcodebyunionid_result'];
        $cc = json_decode($c, true);

        if ($cc['resultCode'] == 0 && $cc['resultMessage'] == '获取代码成功') {
            $union_url = $cc['urlList'];
            foreach ($union_url as $u) {
                $url = $u;
            }
        }

        if (!empty($url)) {
            $u = urlencode($url);
            $minUrl2 = '/pages/jingfen_twotoone/item?spreadUrl=' . $u . '&customerinfo=jdsqgj180601';
            $isUser = 1;
        } else {
            $this->logss('优惠劵二合一接口信息----', $c);
            $this->logss('优惠劵二合一用户信息----', array('newjdid' => $jdid_new, 'jdid' => $jdid, 'shopid' => $shopid, 'couponUrl' => $couponUrl));
            //第三步获取推广链接
            $apimethod3 = 'jingdong.service.promotion.wxsq.getCodeByUnionId';  //推广链接
            $param_json3 = array(
                'proCont' => 1,
                'materialIds' => $skuId,
                'unionId' => $jdid

            );
            $reg2 = $this->GetZeusApiData($apimethod3, $param_json3);
            $reglist2 = json_decode($reg2, true);
            $c3 = $reglist2['jingdong_service_promotion_wxsq_getCodeByUnionId_responce']['getcodebysubunionid_result'];
            $cc3 = json_decode($c3, true);

            foreach ($cc3['urlList'] as $url2) {
                $u2 = $url2;
            }

            if (!empty($u2)) {
                $u2 = urlencode($u2);
                $minUrl2 = '/pages/product/product?wareId=' . $skuId . '&spreadUrl=' . $u2 . '&customerinfo=jdsqgj180601';
                $isUser = 1;
            } else {
                $this->logss('推广链接接口信息----', $c3);
                $this->logss('推广链接用户信息----', array('newjdid' => $jdid_new, 'jdid' => $jdid, 'shopid' => $shopid));
                $minUrl2 = $proinfo['minDetailUrl'];
                $isUser = 0; //是否是用户打标
            }
        }

        $shareUrl = $this->getCompositeImage($skuId,$isshop);

        $proinfo1 = array(
            'isuerShop'         => $isUser,
            'Shopjdid'          => $jdid,
            'minDetailUrl'      => $minUrl2,
            'wlPrice'           => $proinfo['wlPrice'],
            'discount'          => $proinfo['discount'],
            'factprice'         => $proinfo['factprice'],
            'wlCommissionShare' => $proinfo['wlCommissionShare'],
            'makeMoney'         => $proinfo['makeMoney'],
            'goodComment'       => $proinfo['goodComment'],
            'goodCommentsShare' => $proinfo['goodCommentsShare'],
            'shareUrl'          => $shareUrl
        );

        echo json_encode($proinfo1);
    }

    //添加坑位
    public function addproduct()
    {
        $uid = base64_decode(input('uid'));
        $kwId = input('kwId');
        $skuid = input('skuid');
        $pid = input('pid');

        $shop = Db::name('shop');
        $shop_option = Db::name('shop_option');

        $shopid = $shop->where('uid', $uid)->value('id');
        $shopoption = $shop_option->where('uid', $uid)->find();

        $products = Db::name('products_v2');
        $product_meta = Db::name('product_meta');
        $proinfo = $products->field('id,minDetailUrl,link')->where('skuid', $skuid)->find();

        if ($shopoption) {

            $counts = sizeof($shopoption);
            if ($counts > 10) {
                echo $msg = '商品添加超出限制';
                exit;
            }

            $data = [
                'shopid' => $shopid,
                'uid' => $uid,
                'sku' => $skuid,
                'kw' => $kwId,
                'minUrl' => ''
            ];
            $shop_option->insert($data);
            echo 1;

        } else {

            $data = [
                'shopid' => $shopid,
                'uid'    => $uid,
                'sku'    => $skuid,
                'kw'     => $kwId,
                'minUrl' => ''
            ];
            $shop_option->insert($data);
            echo 1;

        }

        $pro_metainfo = $product_meta->where('pid', $proinfo['id'])->where('key', 'isselect2')->value('values');

        if (!empty($pro_metainfo)) {

            $productMeta = [
                'values' => $uid . ',' . $pro_metainfo,
                'key' => "isselect2"
            ];
            $product_meta->where('pid', $proinfo['id'])->where('key', 'isselect2')->update($productMeta);

//            if( $pid != 0 ){
//
//                $metainfo  = $product_meta->where('pid',$pid)->where('key','isselect2')->value('values');
//                if($metainfo){
//
//                    $uidarg = explode(',',$metainfo);
//                    $key = array_search($uid ,$uidarg);
//                    array_splice($uidarg,$key,1);
//                    if($uidarg){
//                        $changeMeta = [
//                            'values'   => implode(',',$uidarg)
//                        ];
//                        $product_meta->where('pid',$pid)->where('key','isselect2')->update($changeMeta);
//                    }else{
//                        $product_meta->where('pid',$pid)->where('key','isselect2')->delete();
//                    }
//
//                }
//            }

        } else {

            $productMeta = [
                'pid' => $proinfo['id'],
                'key' => "isselect2",
                'values' => $uid
            ];
            $product_meta->insert($productMeta);
        }

    }


    //测试添加坑位

    public function addproduct_test()
    {
        $uid = base64_decode(input('uid'));
        $kwId = input('kwId');
        $skuid = input('skuid');
        $pid = input('pid');

        $shop = Db::name('shop');
        $shop_option = Db::name('shop_option');

        $shopid = $shop->where('uid', $uid)->value('id');
        $shopoption = $shop_option->where('uid', $uid)->find();

        $products = Db::name('products_v2');
        $product_meta = Db::name('product_meta');
        $proinfo = $products->field('id,minDetailUrl,link')->where('skuid', $skuid)->find();

        if ($shopoption) {

            $counts = sizeof($shopoption);
            if ($counts > 10) {
                echo $msg = '商品添加超出限制';
                exit;
            }

            $data = [
                'shopid' => $shopid,
                'uid' => $uid,
                'sku' => $skuid,
                'kw' => $kwId,
                'minUrl' => ''
            ];
            $shop_option->insert($data);
            echo 1;

        } else {

            $data = [
                'shopid' => $shopid,
                'uid'    => $uid,
                'sku'    => $skuid,
                'kw'     => $kwId,
                'minUrl' => ''
            ];
            $shop_option->insert($data);
            echo 1;

        }

        $pro_metainfo = $product_meta->where('pid', $proinfo['id'])->where('key', 'isselect2')->value('values');

        if (!empty($pro_metainfo)) {
            //p($pro_metainfo);
            //p($proinfo['id']);
            $productMeta = [
                'values' => $uid . ',' . $pro_metainfo,
                'key' => "isselect2"
            ];
            $product_meta->where('pid', $proinfo['id'])->where('key', 'isselect2')->update($productMeta);

//            if( $pid != 0 ){
//
//                $metainfo  = $product_meta->where('pid',$pid)->where('key','isselect2')->value('values');
//                if($metainfo){
//
//                    $uidarg = explode(',',$metainfo);
//                    $key = array_search($uid ,$uidarg);
//                    array_splice($uidarg,$key,1);
//                    if($uidarg){
//                        $changeMeta = [
//                            'values'   => implode(',',$uidarg)
//                        ];
//                        $product_meta->where('pid',$pid)->where('key','isselect2')->update($changeMeta);
//                    }else{
//                        $product_meta->where('pid',$pid)->where('key','isselect2')->delete();
//                    }
//
//                }
//            }

        } else {

            $productMeta = [
                'pid' => $proinfo['id'],
                'key' => "isselect2",
                'values' => $uid
            ];
            $product_meta->insert($productMeta);
            //p($productMeta);
        }

    }

    //C端坑位显示
    public function Cshopoptioninfo()
    {
        $shopid = input('sid');
        $uid = base64_decode(input('uid'));
        if (empty($shopid)) {
            echo 0;
            exit;
        }


        $shop_option = Db::name('shop_option');
        $products = Db::name('products_v2');

        $sku1 = $shop_option->field('sku')->where('shopid', $shopid)->where('uid', $uid)->page('1,10')->select();

        //检索是否有商品
        if ($sku1) {
            $haveN = array();
            foreach ($sku1 as $sk) {
                $proinfo = $products->where('skuid', $sk['sku'])->find();
                if ($proinfo) {
                    $haveN[] = $sk['sku'];
                    $listOld[] = array('sku' => $sk['sku']);
                }
            }
            $nums = count($haveN);

        } else {
            $nums = 0;
        }

        $hotLists = array();
        if ($nums < 10) {
            //剩余商品填满
            $leftNums = 10 - $nums;
            $list = $products->where('cidname', 12)->order('inSellCount desc')->page('1,' . $leftNums)->select();
            if ($list) {
                foreach ($list as $k2 => $proinfo2) {
                    $sku[] = array('sku' => $proinfo2['skuid']);
                    $lists = array_merge($sku1, $sku);
                }
            } else {
                $lists[] = $sku1;
            }

        } else {

            $lists = $listOld;
        }
        if ($lists) {

            foreach ($lists as $k => $v) {

                $proinfo = $products->where('skuid', $v['sku'])->find();
                //p(sizeof($proinfo));
                if ($proinfo) {

                    $hotLists[] = array(
                        'proImgUrl' => $proinfo['imageurl'],
                        'top' => ($k + 1),
                        'skuid' => $proinfo['skuid'],
                        'proName' => $proinfo['skuName'],
                        'afterPrice' => $proinfo['factprice'],
                        'beforePrice' => $proinfo['wlPrice'],
                        'coupon' => $proinfo['discount'],
                        'commission' => $proinfo['makeMoney'],
                        'commRatio' => $proinfo['wlCommissionShare'],
                        'uid' => $uid,
                        'id' => $proinfo['id'],
                        'deleteSta' => true
                    );
                }

            }

            $isempty = 1;
        } else {
            $isempty = 0;
        }

        $list = array(
            'hotLists' => $hotLists,
            'isempty' => $isempty
        );

        return json($list);

    }

    //C端坑位显示-测试
    public function CshopoptioninfoTest()
    {
        $shopid = input('sid');
        $uid = base64_decode(input('uid'));
        if (empty($shopid)) {
            echo 0;
            exit;
        }


        $shop_option = Db::name('shop_option');
        $products = Db::name('products_v2');

        $sku1 = $shop_option->field('sku')->where('shopid', $shopid)->where('uid', $uid)->page('1,10')->select();

        //检索是否有商品
        if ($sku1) {
            $haveN = array();
            foreach ($sku1 as $sk) {
                $proinfo = $products->where('skuid', $sk['sku'])->find();
                if ($proinfo) {
                    $haveN[] = $sk['sku'];
                    $listOld[] = array('sku' => $sk['sku']);
                }
            }
            $nums = count($haveN);

        } else {
            $nums = 0;
        }



        $hotLists = array();
        if ($nums < 10) {
            //剩余商品填满
            $leftNums = 10 - $nums;
            $list = $products->where('cidname', 12)->order('inSellCount desc')->page('1,' . $leftNums)->select();
            if ($list) {
                foreach ($list as $k2 => $proinfo2) {
                    $sku[] = array('sku' => $proinfo2['skuid']);
                    $lists = array_merge($sku1, $sku);
                }
            } else {
                $lists[] = $sku1;
            }

        } else {

            $lists = $listOld;
        }


        //p($lists);
        //exit;

        if ($lists) {

            foreach ($lists as $k => $v) {

                $proinfo = $products->where('skuid', $v['sku'])->find();
                //p(sizeof($proinfo));
                if ($proinfo) {

                    $hotLists[] = array(
                        'proImgUrl' => $proinfo['imageurl'],
                        'top' => ($k + 1),
                        'skuid' => $proinfo['skuid'],
                        'proName' => $proinfo['skuName'],
                        'afterPrice' => $proinfo['factprice'],
                        'beforePrice' => $proinfo['wlPrice'],
                        'coupon' => $proinfo['discount'],
                        'commission' => $proinfo['makeMoney'],
                        'commRatio' => $proinfo['wlCommissionShare'],
                        'uid' => $uid,
                        'id' => $proinfo['id'],
                        'deleteSta' => true
                    );
                }

            }

            $isempty = 1;
        } else {
            $isempty = 0;
        }

        $list = array(
            'hotLists' => $hotLists,
            'isempty' => $isempty
        );

        return json($list);

    }

    //坑位显示
    public function shopoptioninfo()
    {
        $shopid = input('sid');
        $uid = base64_decode(input('uid'));
        if (empty($shopid)) {
            echo 0;
            exit;
        }
        $shop_option = Db::name('shop_option');
        $products    = Db::name('products_v2');
        $hotLists    = array();

        $sku1 = $shop_option->field('sku')->where('shopid', $shopid)->where('uid', $uid)->page('1,10')->select();

        if ($sku1) {

            foreach ($sku1 as $k => $v) {

                $proinfo = $products->where('skuid', $v['sku'])->find();

                if ($proinfo) {

                    $hotLists[] = array(
                        'proImgUrl' => $proinfo['imageurl'],
                        'top' => ($k + 1),
                        'skuid' => $proinfo['skuid'],
                        'proName' => $proinfo['skuName'],
                        'afterPrice' => $proinfo['factprice'],
                        'beforePrice' => $proinfo['wlPrice'],
                        'coupon' => $proinfo['discount'],
                        'commission' => $proinfo['makeMoney'],
                        'commRatio' => $proinfo['wlCommissionShare'],
                        'uid' => $uid,
                        'id' => $proinfo['id'],
                        'deleteSta' => true
                    );

                }
            }

            $isempty = 1;
        } else {
            $isempty = 0;
        }

        if(!$hotLists){
            $isemp = 0;
            $shop_option->where('uid', $uid)->delete();
        }else{
            $isemp = 1;
        }

        $list = array(
            'hotLists' => $hotLists,
            'isempty' => $isempty,
            'status'  => $isemp
        );

        return json($list);
    }

    //分享来的店铺默认关注
    public function setDefaultshop()
    {

        $uid = base64_decode(input('uid'));
        $shopid = input('shopid');
        $customers = Db::name('customers');
        $followid = $customers->where('id', $uid)->value('followid');
        $is_proxy = $customers->where('id', $uid)->value('is_proxy');

        if ($followid == 0 && $is_proxy == 0) {

            if ($uid) {

                $customers->where('id', $uid)->setField('followid', $shopid);
            }

            if ($shopid) {
                $shop = Db::name('shop');
                $follow = $shop->where('id', $shopid)->value('followed');
                if (empty($follow)) {
                    $shop->where('id', $shopid)->setField('followed', $uid);
                } else {
                    $newid = $uid . ',' . $follow;
                    $shop->where('id', $shopid)->setField('followed', $newid);
                }
            }
        }

    }

    //日志写入

    public function logss($title, $info)
    {
        Log::info($title . json_encode($info));
    }

    //检测是否是店长
    public function isusershop()
    {
        $data = input();
        $uid = base64_decode($data['uid']);
        $shopinfo = Db::name('shop')->field('id')->where('status', 2)->where('uid', $uid)->find();

        if ($shopinfo) {
            echo $shopinfo['id'];
        } else {
            echo 0;
        }

    }


    //推荐商品转链
    public function createProLink()
    {

        $data = input('');

        $id = $data['hotid'];

        $tb = Db::name('hot_products');

        $shop = Db::name('shop');

        $shopid = $data['shopid'];

        $s = $tb->where('id', $id)->find();
        $jdid = $shop->where('id', $shopid)->value('jdid');

        if (!empty($jdid) && !empty($s)) {
            $apimethod2 = 'jingdong.service.promotion.coupon.getCodeByUnionId'; //二合一推广链接
            $link = $s['link'];
            $sku = $s['sku'];

            $param_json2 = array(
                'couponUrl' => $link,
                'materialIds' => $sku,
                'unionId' => $jdid
            );
            $reg = $this->GetZeusApiData($apimethod2, $param_json2);
            $reglist = json_decode($reg, true);
            $c = $reglist['jingdong_service_promotion_coupon_getCodeByUnionId_responce']['getcodebyunionid_result'];

            $cc = json_decode($c, true);
            if ($cc['resultCode'] == 0 && $cc['resultMessage'] == '获取代码成功') {
                $union_url = $cc['urlList'];
                foreach ($union_url as $u) {
                    $url = $u;
                }
            }

            if (!empty($url)) {
                $u = $url;
                echo $u;
            } else {

                echo 1;
            }
        } else {

            echo 2;
        }
    }


    public function GetZeusApiData($apiUrl = '', $param_json = array(), $version = '1.0', $get = false)
    {
        $API['access_token'] = $this->josrefreshAccessToken(); //  生成的access_token，30天一换
        $API['app_key'] = $this->appKey2;
        $API['method'] = $apiUrl;
        $API['360buy_param_json'] = json_encode($param_json);
        $API['timestamp'] = date('Y-m-d H:i:s', time());
        $API['v'] = $version;
        ksort($API);    //  排序
        $str = '';      //  拼接的字符串
        foreach ($API as $k => $v) $str .= $k . $v;
        $sign = strtoupper(md5($this->appScret2 . $str . $this->appScret2));    //  生成签名MD5加密转大写
        if ($get) {
            //  用get方式拼接URL
            $url = "https://api.jd.com/routerjson?";
            foreach ($API as $k => $v)
                $url .= urlencode($k) . '=' . $v . '&';  //  把参数和值url编码
            $url .= 'sign=' . $sign;
            $res = self::curl_get($url);
        } else {
            //  用post方式获取数据
            $url = "https://api.jd.com/routerjson?";
            $API['sign'] = $sign;
            $res = self::curl_post($url, $API);
        }
        return $res;
    }

    //  刷新accessToken
    private function josrefreshAccessToken()
    {
        //$filePath = ROOT_PATH.'ZeusToken.config';
        $filePath = ROOT_PATH . '/ZeusToken.config';   //测试
        if (file_exists($filePath)) {
            $handle = fopen($filePath, 'r');
            $tokenJson = fread($handle, 8142);
        } else {
            //  插入默认的token
            fwrite(fopen($filePath, 'w'), $this->app_token_json2);
            $tokenJson = $this->app_token_json2;
        }

        if (substr($tokenJson, 0, 3) == pack('CCC', 0xef, 0xbb, 0xbf)) {
            $tokenJson = substr($tokenJson, 3);
        }
        $res = json_decode(trim($tokenJson), true);   //解析不了可能是文本出了问题
        //  判断
        if ($res['code'] == 0) {
            if ($res['expires_in'] * 1000 + $res['time'] < self::getMillisecond() - 86400000) {
                //access_token失效前一天
                //获取刷新token的url
                $refreshUrl = "https://oauth.jd.com/oauth/token?";
                $refreshUrl .= '&client_id=' . $this->appKey2;
                $refreshUrl .= '&client_secret=' . $this->appScret2;
                $refreshUrl .= '&grant_type=refresh_token';
                $refreshUrl .= '&refresh_token=' . $res['refresh_token'];
                //  获取新的token数据
                $newAccessTokenJson = self::curl_get($refreshUrl);
                //  写入文本
                fwrite(fopen($filePath, 'w'), $newAccessTokenJson);
                //  解析成数组
                $newAccessTokenArr = json_decode($newAccessTokenJson, true);
                $accessToken = $newAccessTokenArr['access_token'];
            } else {
                $accessToken = $res['access_token'];
            }
            return $accessToken;
        } else {
            //  如果refresh_token过期，将会返回错误码code:2011;msg:refresh_token过期
            return $res['msg'];
        }
    }

    //  get请求
    private static function curl_get($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    //  post请求
    private static function curl_post($url, $curlPost)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    //  获取13位时间戳
    private static function getMillisecond()
    {
        list($t1, $t2) = explode(' ', microtime());
        return sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }

    //c端搜索商品列表
    public function test_prolist()
    {
       
        $cn = input('iscoupon'); //是否点击了仅看有券
        // dump($cn);exit;
        $paged = input('paged');
        $sort  = input('sort');
        $by    = input('by');
        $shop = Db::name('products_v2');
        if( $by == 1 ){
            $order = 'inSellCount';
        }else if( $by ==2 ){
            $order = 'factprice';
        }else if( $by ==3 ){
            $order = 'views';
        }else if( $by ==4 ){
            $order = 'goodComment';
        }

        if ($sort == "true") {
            $so = 'asc';
        } else {
            $so = 'desc';
        }
        if( $cn == "false" ){
            // dump($cn);exit;
            $list = $shop->order($order . ' ' . $so)->page($paged . ',10')->select();
        }else{

            
            $list = $shop->where('link','<>','')->order($order . ' ' . $so)->page($paged . ',10')->select();
        }
        $li = array(
            'list' => $list
        );
        return json($li);
    }
    
     //B端搜索商品池
    public function test_Bprolist()
    {
        $cn = input('iscoupon');
        $paged = input('paged');
        $sort  = input('sort');
        $by    = input('by');
        $uid = base64_decode(input('uid'));

        $shop = Db::name('products_v2');
        $product_meta = Db::name('product_meta');
        $order = 'inSellCount';
        if ($by == 1) {

            $order = 'inSellCount';

        } else if ($by == 6) {
            $order = 'wlCommissionShare';

        } else if ($by == 7) {

            $order = 'makeMoney';

        } else if ($by == 4) {
            $order = 'goodComment';
        }

        if ($sort == "true") {
            $so = 'asc';
        } else {
            $so = 'desc';
        }
        if ($cn == "false" )
        {
            $list = $shop->order($order . ' ' . $so)->page($paged . ',10')->select();
        } else {
            $list = $shop->where('link','<>','')->order($order . ' ' . $so)->page($paged . ',10')->select();
        }

        if ($list)
        {

            foreach ($list as $k => $v) {
                $pro_metainfo = $product_meta->where('pid', $v['id'])->where('key', 'isselect2')->value('values');
                if ($pro_metainfo) {
                    $isselect2 = explode(',', $pro_metainfo);

                    if (in_array($uid, $isselect2)) {
                        $select['addBefore'] = false;
                    } else {
                        $select['addBefore'] = true;
                    }
                } else {
                    $select['addBefore'] = true;
                }

                $lists[] = array_merge($list[$k], $select);
            }

            $li = array(
                'list' => $lists
            );

            return json($li);

        } else {
            echo 0;
        }

    }

    //获取分享卡片合成图
    public function getCompositeImage($skuID = '32912943324',$isshop = 1)
    {

        //判断文件是否存在
        $upload_dir = ROOT_PATH . 'public/upload/share/' . $skuID;
        if (!file_exists($upload_dir)) {
            mkdir($upload_dir, 0755, true);
        }

        //商品独立文件夹
        $path   = ROOT_PATH . 'public/upload/share/'.$skuID;
        $url    = config('website').'/upload/share/'.$skuID;

        //判断是不是店长
        if($isshop == 1){

            $image = \think\Image::open('./imageShop1.png');

        }else{

            $image = \think\Image::open('./imageUser1.png');
        }

        //查询是否存在该商品
        $proinfo = Db::name('products_v2')->where('skuid', $skuID)->find();

        if( $proinfo ){

            //判断原价和劵后价长度
            $factprice = str_replace('.','',$proinfo['factprice']);
            if(strlen($factprice) >= 4){
                $factprice = $proinfo['factprice'];
            }else{
                $factprice = number_format($proinfo['factprice'],2);
            }

            $wlPrice = str_replace('.','',$proinfo['wlPrice']);
            if(strlen($wlPrice) >= 4){
                $wlPrice = $proinfo['wlPrice'];
            }else{
                $wlPrice = number_format($proinfo['wlPrice'],2);
            }
            $imgURl            = $proinfo['imageurl'];
            $goodCommentsShare = $proinfo['goodCommentsShare'].'%';
            $goodComment       = $proinfo['goodComment'];
            $this->createPriceProductImage('原价: '.$wlPrice,$wlPrice,$path); //创建原价合成图+删除线
        }

        $filename = $path.'/'.$skuID.'.png'; //商品图
        $content  = file_get_contents($imgURl);
        $r        = file_put_contents($filename, $content);

        if($r){

            //中心等比例裁剪商品图
            $Proimage = \think\Image::open($filename);
            $Proimage->thumb(159,160,\think\Image::THUMB_CENTER,'')->save($path.'/thumb.png','png','100',true);

            //合成图一：添加图片

            $img_xy = array(55,67);
            $image->water($path.'/thumb.png',$img_xy)->save($path.'/share.png','png','100',true);

            //合成图二：添加劵后价

            $fprice_xy = array(255,88);
            $image2 = \think\Image::open($path.'/share.png');

            $strArray=str_split($factprice);
            $str=join("",$strArray);

            $image2->text($str,'msyhbd.ttf',30,'#F51B51',$fprice_xy)->save($path.'/share.png','png','100',true);

            //合成图三：添加原价

            $image3 = \think\Image::open($path.'/share.png');
            $wlprice_xy = array(245,128);
            $image3->water($path.'/price.png',$wlprice_xy)->save($path.'/share.png','png','100',true);

            //合成图四：添加好评率

            $goodc_xy = array(160,324);
            $image4 = \think\Image::open($path.'/share.png');
            $image4->text($goodCommentsShare,'msyh.ttf',15,'#ffffff',$goodc_xy)->save($path.'/share.png','png','100',true);

            //合成图五：添加好评数

            $goodn_xy = array(305,324);
            $image5 = \think\Image::open($path.'/share.png');
            $image5->text($goodComment,'msyh.ttf',15,'#ffffff',$goodn_xy)->save($path.'/share.png','png','100',true);

            return $url.'/share.png';
        }else{
            return 2;
        }
    }

    //合成删除线
    public function createPriceProductImage($price,$len,$path)
    {
        //header('Content-Type: image/png');
        $text = $price;

        // 合成删除线
        $im = imagecreatetruecolor(135 , 30);
        $white = imagecolorallocate($im, 248, 248, 248);
        $red = imagecolorallocate($im, 189, 189, 189);
        imagefilledrectangle($im, 0, 0, 180, 40, $white);
        $font = ROOT_PATH.'public/msyh.ttf';
        imagettftext($im, 15, 0, 1, 22, $red, $font, $text);
        if (!empty($price)) {
            $this->addStrikethrough($im, $font, $this->getUnderline($len));
        }
        $path = $path . '/price.png';
        imagepng($im,$path);
        imagedestroy($im);
    }

    //创建删除线
    public function addStrikethrough($image, $font, $underline)
    {
        $grey = imagecolorallocate($image, 189, 189, 189);
        imagettftext($image, 13, 0, 1, 13, $grey, $font, $underline);
    }

    //添加横线
    public function getUnderline($len)
    {
        $wl = strlen($len);
        if($wl == 4){
            $len = 12;
        }
        else if($wl == 5){
            $len = 14;
        }else if($wl == 6){
            $len = 15;
        }
        $underline = '&#95;'; // '&#95;' symbol equal '_'
        $length = $len;
        $complexUnderline = '';
        for ($i = 0; $i < $length; $i++) {
            $complexUnderline = $complexUnderline . $underline;
        }
        return $complexUnderline;
    }

    //获取店长排行展示

    public function getRank()
    {
        $table_name = Db::name('shop');
        $user_table = Db::name('customers');

        $list = $table_name->order('follow_nums desc')->page('1,10')->select();

        foreach ($list as $k=>$v){

            $uinfo   = $user_table->field('nickName,avatarUrl')->where('id',$v['uid'])->find();
            $data[]  = array(

               'sid'  => $k+1,
               'userImg' => $uinfo['avatarUrl'],
               'storeName'  => $v['name'],
               'storeAds'   => $v['province'].''.$v['city'].''.$v['area'],
               'rankNum'   => $v['follow_nums']

           );
//            $follows = explode(',',$v['followed']);
//            $follows = array_unique($follows);
//            $nums    = sizeof($follows);
//            $data = [
//               'follow_nums' => $nums
//            ];
//            $table_name->where('id',$v['id'])->update($data);

        }
        return json($data);
        //p($data);
    }


    //添加店长升级判断

    public function isUpstore()
    {
        $uid      = base64_decode(input('uid'));
        $shop     = Db::name('shop');
        $customer = Db::name('customers');

        $isup = $shop->where('uid', $uid)->value('isup');

        if($isup == 1){

            $jdid = $shop->where('uid', $uid)->value('jdid');

            if(!$jdid){
                $followid = $customer->where('id', $uid)->value('followid');
                $jdid1    = $shop->where('id', $followid)->value('jdid');
            }else{
                $jdid1 = $jdid;
            }

            $data = [
                'jdid '=> $jdid1,
                'isup' => 1
            ];
            return json($data);
        }else{

            $followid = $customer->where('id', $uid)->value('followid');
            $isup = $shop->where('id', $followid)->value('isup');
            if($isup == 1){
                $jdid = $shop->where('id', $followid)->value('jdid');
                if(!$jdid){
                    $jdid1    = $shop->where('id', $followid)->value('jdid');
                }else{
                    $jdid1 = $jdid;
                }

                $data = [
                    'jdid '=> $jdid1,
                    'isup' => 1
                ];
            }else{
                if($isup == 2){
                    $isups = 1;
                }else{
                    $isups = 0;
                }
                $data = [
                    'jdid '=> 0,
                    'isup' => $isups
                ];

            }
            return json($data);

        }


    }

    public function setUpshop()
    {

        $uid      = base64_decode(input('uid'));
        $shop     = Db::name('shop');
        $customer = Db::name('customers');

        $shop->where('uid',$uid)->update(['isup' =>2]);
        $jdid = $shop->where('uid', $uid)->value('jdid');

        if(!$jdid){

            $followid = $customer->where('id', $uid)->value('followid');
            $jdid1    = $shop->where('id', $followid)->value('jdid');

            //C端
            $data = [
                'appid'  => 'wxe8e6b9bf45e660a6',
                'jumpUrl'=> '/pages/index/index?unionId='.$jdid1,
                'isshop' => 0
            ];

        }else{

            //B端

            $data = [
                'appid'  => 'wx0de1a40953182cb1',
                'jumpUrl'=> '/pages/index/index',
                'isshop' => 1
            ];
        }

        return json($data);

    }
}