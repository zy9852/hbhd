<?php

namespace app\api\controller\v1;

use think\Controller;
use think\Db;

//use app\api\model\Member as UserModel;

class User extends Controller
{
    public function changeUser()
    {
        $data = input('post.');
        $id = base64_decode($data['id']);
        unset($data['id']);
        $cust = Db::name('customers')->where('id', $id)->update($data);
        if ($cust) {
            $res = ['data' => 1, 'msg' => '修改成功'];
            echo json_encode($res);
            exit;
        } else {
            $res = ['data' => 2, 'msg' => '数据无变化'];
            echo json_encode($res);
            exit;
        }
    }

    public function checkUser()
    {
        $id = base64_decode(urldecode(input('id')));  //后期直接获取$id
        $user = Db::name('customers')->where('id', $id)->select();
        echo json_encode($user);
    }

    public function checkHome()
    {
        $mid      = urldecode(input('mid'));
        $gym      = model('gym');
        if($mid)
        {
            $name = $gym->where('id',$mid)->value('name');
        }else{
            $name = $gym->where('is_acquiesce',1)->value('name');
            $mid  = $gym->where('is_acquiesce',1)->value('id');
        }
        $price    = Db::name('option')->where('meta_key', 'price')->value('meta_value');
        $desc     = Db::name('option')->where('meta_key', 'desc')->value('meta_value');
        $banner   = Db::name('option')->where('meta_key', 'banner')->value('meta_value');
        $advtitle = Db::name('option')->where('meta_key', 'advtitle')->value('meta_value');
        $advdesc  = Db::name('option')->where('meta_key', 'advdesc')->value('meta_value');
        $res      = array("price" => $price, "desc" => $desc, "banner" => $banner, "advtitle" => $advtitle, "advdesc" => $advdesc,'name'=>$name,'mid'=>$mid);
        echo json_encode($res);
    }

    public function checkRule()
    {
        $list = Db::name('rule')->where('is_delete', 1)->select();
        echo json_encode($list);
    }
}