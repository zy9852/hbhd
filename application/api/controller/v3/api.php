<?php
namespace app\api\controller\v3;

use app\common\model\Order;
use think\Controller;
use think\Db;
use think\Request;
use app\config;
//error_reporting(E_ALL^E_NOTICE);

class Api extends Controller
{
    //curl的get请求
    //http_get
    //curl的post请求
    //postCurl
    //POST方式调用微信小程序 支付/提现 接口
    //curl_post_ssl
    //商户系统内部订单号，要求32个字符内
    //getNonceStr
    //获取IP地址
    //getip
    //生成签名
    //makeSign
    //数组转XML
    //array2xml
    //XML转数组
    //xml2array
    //获取openid和session_key（微信小程序登录）
    //wxlogin
    //获取access_token
    //returnAsskey
    //模板消息
    //temMsg
    //微信小程序支付
    //wxPay
    //微信小程序回调
    //wxNotify
    //微信小程序提现到零钱
    //wxWithdraw
    //获取用户信息
    //getUserInfo
    //返回banner图片，福豆记录，福豆汇率
    //other
    //帮带订单录入
    //bdai
    //帮带插入图片
    //bdai2
    //上传图片方法
    //wxImg
    //上传语音方法
    //wxMp3
    //查询订单信息
    //orderlist
    //生成唯一订单号
    //create_order_no
    //帮带接单
    //receive
    //订单状态更改
    //orderChange
    //兑现福豆1
    //bean
    //地址相关操作
    //locationList
    //清除自己订单信息
    //deleteMe
    //测试
    //cs

    public function http_get($url){
        $ch = curl_init($url);
        curl_setopt($ch,CURLOPT_HEADER,0);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($ch,CURLOPT_TIMEOUT,60);
        $data = curl_exec($ch);
        curl_close($ch);
        return json_decode($data,true);
    }

    //curl的post请求
    public function postCurl($url,$data,$type){
        if($type == 'json'){
            $data = json_encode($data);//对数组进行json编码
            $header= array("Content-type: application/json;charset=UTF-8","Accept: application/json","Cache-Control: no-cache", "Pragma: no-cache");
        }
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL,$url);
        curl_setopt($curl,CURLOPT_POST,1);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,false);
        if(!empty($data)){
            curl_setopt($curl,CURLOPT_POSTFIELDS,$data);
        }
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
        $res = curl_exec($curl);
        if(curl_errno($curl)){
            echo 'Error+'.curl_error($curl);
        }
        curl_close($curl);
        return $res;
    }

    //POST方式调用微信小程序 支付/提现 接口
    public function curl_post_ssl($url, $xmldata, $second=30,$aHeader=array()){
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_TIMEOUT,$second);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        //这里设置代理，如果有的话
        //curl_setopt($ch,CURLOPT_PROXY, '10.206.30.98');
        //curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $aHeader);
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$xmldata);
        $data = curl_exec($ch);
        if(curl_errno($ch)){
            echo 'Error+'.curl_error($ch);
        }
        curl_close($ch);
        return $data;
    }

    public function curl_post_ssl2($url, $xmldata, $second=30,$aHeader=array()){
        $isdir = str_replace('Api.php','',__FILE__) . 'cert/';

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_TIMEOUT,$second);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        //这里设置代理，如果有的话
        //curl_setopt($ch,CURLOPT_PROXY, '10.206.30.98');
        //curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $aHeader);

        curl_setopt($ch, CURLOPT_SSLCERTTYPE, 'PEM');//证书类型
        curl_setopt($ch, CURLOPT_SSLCERT, $isdir . 'apiclient_cert.pem');//证书位置
        curl_setopt($ch, CURLOPT_SSLKEYTYPE, 'PEM');//CURLOPT_SSLKEY中规定的私钥的加密类型
        curl_setopt($ch, CURLOPT_SSLKEY, $isdir . 'apiclient_key.pem');//证书位置
        curl_setopt($ch, CURLOPT_CAINFO, 'PEM');
        //curl_setopt($ch, CURLOPT_CAINFO, $isdir . 'rootca.pem');这个是可选的

        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$xmldata);
        $data = curl_exec($ch);
        if(curl_errno($ch)){
            echo 'Error+'.curl_error($ch);
        }
        curl_close($ch);
        return $data;
    }

    //商户系统内部订单号，要求32个字符内
    public function getNonceStr($length = 32) {
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str   = "";
        for ( $i = 0; $i < $length; $i++ )  {
            $str .= substr($chars, mt_rand(0, strlen($chars)-1), 1);
        }
        return $str;
    }

    //获取IP地址
    public function getip() {
        static $ip = '';
        $ip = $_SERVER['REMOTE_ADDR'];
        if(isset($_SERVER['HTTP_CDN_SRC_IP'])) {
            $ip = $_SERVER['HTTP_CDN_SRC_IP'];
        } elseif (isset($_SERVER['HTTP_CLIENT_IP']) && preg_match('/^([0-9]{1,3}\.){3}[0-9]{1,3}$/', $_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR']) AND preg_match_all('#\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}#s', $_SERVER['HTTP_X_FORWARDED_FOR'], $matches)) {
            foreach ($matches[0] AS $xip) {
                if (!preg_match('#^(10|172\.16|192\.168)\.#', $xip)) {
                    $ip = $xip;
                    break;
                }
            }
        }
        return $ip;
    }

    //生成签名
    public function makeSign($data){
        //获取微信支付秘钥
        $key = "huangbhdktufk2314bdjkdkissneckco";
        // 去空
        $data=array_filter($data);
        //签名步骤一：按字典序排序参数
        ksort($data);
        $string_a=http_build_query($data);
        $string_a=urldecode($string_a);
        //签名步骤二：在string后加入KEY
        $string_sign_temp=$string_a."&key=".$key;
        //签名步骤三：MD5加密
        $sign = md5($string_sign_temp);
        // 签名步骤四：所有字符转为大写
        $result=strtoupper($sign);
        return $result;
    }

    /**
     * 将一个数组转换为 XML 结构的字符串
     * @param array $arr 要转换的数组
     * @param int $level 节点层级, 1 为 Root.
     * @return string XML 结构的字符串
     */
    public function array2xml($arr, $level = 1) {
        $s = $level == 1 ? "<xml>" : '';
        foreach($arr as $tagname => $value) {
            if (is_numeric($tagname)) {
                $tagname = $value['TagName'];
                unset($value['TagName']);
            }
            if(!is_array($value)) {
                $s .= "<{$tagname}>".(!is_numeric($value) ? '<![CDATA[' : '').$value.(!is_numeric($value) ? ']]>' : '')."</{$tagname}>";
            } else {
                $s .= "<{$tagname}>" . $this->array2xml($value, $level + 1)."</{$tagname}>";
            }
        }
        $s = preg_replace("/([\x01-\x08\x0b-\x0c\x0e-\x1f])+/", ' ', $s);
        return $level == 1 ? $s."</xml>" : $s;
    }

    //将xml转为array
    public function xml2array($xml){
        //禁止引用外部xml实体
        libxml_disable_entity_loader(false);
        $result= json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        return $result;
    }

    //获取OpenID和session_key
    public function wxlogin(Request $request){
        $data = $request->param();
        $code = $data['code'];

        if (empty($data['code'])) {
            return json(['status'=>0,'data'=>'','msg'=>'没有code值']);
        }

        $appid  = 'wxb4db99c1b0c279e4';
        $secret = '56d38c12c50535095ca061e7c8879c24';
        $url    = 'https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code';

        // 请求地址，用sprinty将appid和secret替换掉地址中的占位符
        $url  = sprintf($url,$appid,$secret,$code);
        $arr = $this->http_get($url);

        //如果是新用户，将其openid存入数据库
        $openid = $arr['openid'];
        $res    = Db::name('member')->where('wxopenid',$openid)->find();

        $msg = [
            'nickname'    => $data['name'],
            'avatar'      => $data['avatarUrl'],
            'gender'      => $data['gender'],
            'create_time' => time()
        ];

        if (empty($res)){
            $msg['wxopenid'] = $openid;
            $uid = Db::name('member')->insertGetId($msg);
        }else{
            $msg['last_login_time'] = time();
            Db::name('member')->where('wxopenid',$openid)->update($msg);
            $uid = $res['id'];
        }

        return $uid ? json(['status'=>1,'data'=>$uid,'msg'=>'登录成功']) : json(['status'=>0,'data'=>'','msg'=>'登录失败']);
    }

    //返回access_token
    public function returnAsskey(){
        $appid  = 'wxb4db99c1b0c279e4';
        $secret = '56d38c12c50535095ca061e7c8879c24';
        $url    = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s';

        // 请求地址，用sprinty将appid和secret替换掉地址中的占位符
        $url     = sprintf($url,$appid,$secret);

        $ass_key = $this->http_get($url);
        $a1      = $ass_key['access_token'];
        return $a1;
    }

    //模板消息
    public function temMsg(Request $request){
        //1订单状态2订单内容3订单编号4联系人姓名
        $data   = $request->param();
        $field  = Db::name('order')->where('id',$data['zjid'])->find();

        //$n_type = $field['name_type'];
        $key1   = $field['nickname'] ?? '匿名';
        $key2   = $field['order_num'];
        $key3   = str_replace(',',"\n",$field['content']);
        //$key4   = $field["avatar"];
        $formid = $data['formid'];
        $temid  = 'I5euBiEWIYaTinzd7Xmdq9SRjxCIhY_Bvi77sBahiaI';

        $openid = Db::name('member')->where('id',$data['uid'])->value('wxopenid');

        $access_token = $this->returnAssKey();
        $url  = 'https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token='.$access_token;
        $data = array(
            "touser"=>$openid,
            "template_id"=>$temid,
            // "page"=>$page,
            "form_id"=>$formid,
            "data"=>array(
                "keyword1"=>array(
                    "value"=>$key1,
                    "color"=>"#173177"
                ),
                "keyword2"=>array(
                    "value"=>$key2,
                    "color"=>"#173177"
                ),
                "keyword3"=>array(
                    "value"=>$key3,
                    "color"=>"#173177"
                )
            ),
//            "emphasis_keyword"=>"keyword1.DATA",//需要进行加大的消息
        );
        $res = $this->postCurl($url,$data,'json');//将data数组转换为json数据

        return $res ? json(['status'=>1,'data'=>$res,'msg'=>'成功']) : json(['status'=>0,'data'=>'','msg'=>'失败']);
    }

    //微信小程序支付
    public function wxPay(Request $request){
        $uid       = $request->param('uid');
        $openid    = Db::name('member')->where('id',$uid)->value('wxopenid');
        //$openid = 'otALc4hn3idQLPMubP3Jh68_vG8o';

        $data = [
            'appid'            => 'wxb4db99c1b0c279e4',
            'mch_id'           => '1518776121',//商户号
            'nonce_str'        => self::getNonceStr(),//随机字符串，长度要求在32位以内
            'body'             => '互帮互带_福豆充值',//商品描述
            'out_trade_no'     => self::create_order_no(),//商户订单号
            'total_fee'        => 1,//标价金额，单位分
            'spbill_create_ip' => self::getip(),//终端IP
            'notify_url'       => config('website').'/api/v3.api/wxNotify',//通知地址
            'trade_type'       => 'JSAPI',//交易类型
            'openid'           => $openid
        ];

        $data['sign'] = self::makeSign($data);

        $xmldata = self::array2xml($data);
        $url     = "https://api.mch.weixin.qq.com/pay/unifiedorder";
        $res     = self::curl_post_ssl($url, $xmldata);

        if (empty($res)) {
            return json(['status'=>0,'data'=>'','msg'=>'连接失败']);
        }

        $content = self::xml2array($res);
//        p($content);die;

        $return_code = strval($content['return_code']);
        $result_code = strval($content['result_code']);

        if ($return_code == 'FAIL') {
            return json(['status'=>0,'data'=>'','msg'=>$return_code]);
        }

        if ($result_code == 'FAIL') {
            return ['status' => 1, 'msg' => strval($content['err_code']), ':' . strval($content['err_code_des'])];
        }

        //第二次签名
        $timest = (string)time();

        $resdata = [
            'appId'     => 'wxb4db99c1b0c279e4',
            'package'   => 'prepay_id='.strval($content['prepay_id']),
            'nonceStr'  => self::getNonceStr(),
            'timeStamp' => $timest,
            'signType'  => 'MD5'
        ];

        $resdata['paySign'] = self::makeSign($resdata);

        return json(['status'=>1,'data'=>$resdata,'msg'=>'查询支付参数成功']);
    }

    //回调
    public function wxNotify()
    {
        //真的成功支付之后，才会触发这个回调方法
        $notify = file_get_contents("php://input");
        $data   = self::xml2array($notify);
        Db::name('member')->where('id',59)->setField('remark',$data);die;

        if($data['result_code'] == 'SUCCESS'){
            //增加福豆
            $oldBean = Db::name('member')->where('wxopenid',$data['openid'])->Field('bean');
            $beanBl  = Db::name('option')->where('id',9)->value('meta_value');
            $money   = $data['total_fee'] / 100;
            $addBean = $money / $beanBl;
            $newBean = $oldBean + $addBean;
            Db::name('member')->where()->setField('bean',$newBean);

            //添加福豆收支记录
            $uid  = Db::name('member')->where('wxopenid',$data['openid'])->value('id');
            $add = [
                'uid'        => $uid,
                'd_type'     => 2,
                'source'     => 1,
                'd_money'    => $addBean,
                'createtime' => time()
            ];

            Db::name('member_bean_detail')->insert($add);
        }
    }

    //微信小程序提现到零钱
    public function wxWithdraw(Request $request){
        //$openid = $request->param('wxopenid');
        $openid = 'otALc4nRsUqC_dR0zZzm9MYKJ_cY';
        $amount = $request->param('w_money');

        $data = [
            'mch_appid'        => 'wxb4db99c1b0c279e4',
            'mchid'            => '1518776121',//商户号
            'nonce_str'        => self::getNonceStr(),//随机字符串，长度要求在32位以内
            'partner_trade_no' => self::create_order_no(),//商户订单号
            'openid'           => $openid,
            'check_name'       => 'NO_CHECK',
            'amount'           => 30,//标价金额，单位分
            'desc'             => '福豆提现到微信零钱',
            'spbill_create_ip' => self::getip()//终端IP
        ];

        $data['sign'] = self::makeSign($data);

        $xmldata = self::array2xml($data);

        $url     = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers";
        $res     = self::curl_post_ssl2($url, $xmldata);

        if (empty($res)) {
            return json(['status'=>0,'data'=>'','msg'=>'连接失败']);
        }

        $content = self::xml2array($res);
        p($content);
    }

    //微信提现申请接口
    public function sqwxWithdraw(Request $request){
        //uid,福豆数量,金额
        $data = $request->param();

        //提现申请之后，将用户的福豆立即减少
        $oldBean = Db::name('member')->where('id',$data['uid'])->value('bean');
        $newBean = $oldBean - $data['bean'];
        Db::name('member')->where('id',$data['uid'])->setField('bean',$newBean);

        $data['openid'] = Db::name('member')->where('id',$data['uid'])->value('wxopenid');
        $data['bean_type'] = 1;//待审核

        $res = Db::name('member_bean')->insert($data);
        return $res ? json(['status'=>1,'data'=>'','msg'=>'提现申请成功']) : json(['status'=>0,'data'=>'','msg'=>'提现申请失败']);
    }

    //获取用户信息
    public function getUserInfo(Request $request){
        //查询用户信息
        $uid  = $request->param('uid');
        $data = Db::name('member')->where('id',$uid)->find();

        return $data ? json(['status'=>1,'data'=>$data,'msg'=>'获取用户信息成功']) : json(['status'=>0,'data'=>'','msg'=>'获取用户信息失败']);
    }

    //1banner图2福豆记录3福豆汇率4地址
    public function other(Request $request){

        //1banner图
        $data1 = Db::name('media')->value('pic1');

        //2福豆记录
        $uid   = $request->param('uid');
        $data2 = Db::name('member_bean_detail')->field('d_type,d_money,createtime')->where('uid',$uid)->order('createtime desc')->select();

        foreach ($data2 as $k=>$v){
            if ($v['d_type'] == 1){
                $data2[$k]['d_type'] = '赠送福豆';
            }else if($v['d_type'] == 2){
                $data2[$k]['d_type'] = '收入福豆';
            }
            $data2[$k]['createtime'] = date('Y-m-d H:i:s',$v['createtime']);
        }

        //3福豆汇率
        $data3[0] = Db::name('option')->where('id',9)->value('meta_value');
        $data3[1] = Db::name('option')->where('id',10)->value('meta_value');

        //4地址
        $add     = Db::name('member_location')->where('uid',$uid)->field('add_type,province,city,district,name')->select() ?? 0;
        $member  = Db::name('member')->where('id',$uid)->find() ?? 0;

        if ($member) {
            foreach ($add as $k => $v){
                $address[] = [
                    'id'       => '1'.($k+1),
                    'name'     => $v['add_type'],
                    'address'  => $v['province'].$v['city'].$v['district'],
                    'avatar'   => $member['avatar'],
                    'nickname' => $v['name'],
                    'isCheck'  => false
                ];
            }
        }

        //5将banner图,福豆记录,福豆汇率,地址，组成数组返回
        $data = [
            'banner'   => $data1,
            'beanLog'  => $data2,
            'bean'     => $data3,
            'address'  => $address ?? ''
        ];

        return $data ? json(['status'=>1,'data'=>$data,'msg'=>'查询信息成功']) : json(['status'=>0,'data'=>'','msg'=>'查询信息失败']);
    }

    //帮我带订单录入
    public function bdai(Request $request){
        //通过type确定插入7大表哪一个，通过pro_status确定帮我带/帮你带
        $table_type = $request->param('table_type');//1判断帮带属于哪一个表,插入信息到对应表中
        $pro_status = $request->param('pro_status');//2帮我带/帮你带
        $data = $request->except('type,table_type,pid,order_id');

        //下单时间/订单自动关闭时间/到达时间==>>时间戳
        $data['createTime']     = time();
        //$data['order_end_time'] = strtotime(strchr(date('Y-m-d H:i:s'),' ',true).' '.$data['order_end_time']);
        $demo = explode(':', $data['order_end_time']);
        $data['order_end_time'] = strtotime(date('Y-m-d H:i:s',strtotime("+$demo[0] hour +$demo[1] minute")));

        //根据不同帮带类型进行添加
        $table_type == 1 && $table_name = 'food';
        $table_type == 2 && $table_name = 'trip';
        $table_type == 3 && $table_name = 'express';
        $table_type == 4 && $table_name = 'market_shopping';
        $table_type == 5 && $table_name = 'bookstore';
        $table_type == 8 && $table_name = 'money';
        if ($table_type == 6 || $table_type == 7 || $table_type == 9){
            $table_name = 'shopping';
        }

        if ($pro_status == 1){
            //帮我带下单需要判断福豆数量是否足够
            $bean    = $data['bean'];
            $uid     = $data['uid'];
            $beanOld = Db::name('member')->where('id',$uid)->value('bean');
            if ($bean > $beanOld){
                return json(['status'=>0,'data'=>'','msg'=>'福豆数量不足哟!']);
            }

            $order['title']    = $data['pro_text'];
            $order['bean']     = $data['bean'];

            if ($table_type == 1){

                $order['table_type']    = $request->param('table_type');
                $data['arrive_time']    = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['arrive_time']);
                $order['proprice']      = $data['proprice'];
                $order['receivingtime'] = $data['arrive_time'];
                $order['content']       = $data['pro_text'].','.'送达时间：'.date('Y-m-d H:i:s',$data['arrive_time']);

            }elseif($table_type == 2){

                $order['table_type']    = $request->param('table_type');
                $order['content']       = $data['pro_text'];
                if ($data['isComback'] == 1){

                    $data['back_time']  = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['back_time']);
                    $order['back_time'] = $data['back_time'];
                    $order['content']   = $data['pro_text'].','.'最晚返回时间：'.date('Y-m-d H:i:s',$data['back_time']);
                }
                $order['isComback']     = $data['isComback'];


            }elseif($table_type == 3){

                $order['table_type']        = $request->param('table_type');
                $order['proprice']          = $data['proprice'];
                $order['express_status']    = $data['express_status'];

                if ($data['express_status'] == 1){

                    $data['arrive_time']    = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['arrive_time']);
                    $order['receivingtime'] = $data['arrive_time'];
                    $order['content']       = $data['pro_text'].','.'送达时间：'.date('Y-m-d H:i:s',$data['arrive_time']);

                }elseif($data['express_status'] == 2){
                    $data['take_time']      = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['take_time']);
                    $order['begin_time']    = $data['take_time'];
                    $order['content']       = $data['pro_text'].','.'取货时间：'.date('Y-m-d H:i:s',$data['take_time']);
                }

            }elseif($table_type == 4){

                $order['table_type']    = $request->param('table_type');
                $order['proprice']      = $data['proprice'];
                $data['arrive_time']    = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['arrive_time']);
                $order['receivingtime'] = $data['arrive_time'];
                $order['content']       = $data['pro_text'].','.'送达时间：'.date('Y-m-d H:i:s',$data['arrive_time']);

            }elseif($table_type == 5){

                $order['table_type']         = $request->param('table_type');
                $order['book_status'] = $data['book_status'];
                if ($data['book_status'] == 1){

                    $order['proprice']       = $data['proprice'];
                    $data['arrive_time']     = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['arrive_time']);
                    $order['receivingtime']  = $data['arrive_time'];
                    $order['content']        = $data['pro_text'].','.'送达时间：'.date('Y-m-d H:i:s',$data['arrive_time']);

                }elseif($data['book_status'] == 2){

                    $data['takebook_time']   = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['takebook_time']);
                    $data['backbook_time']   = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['backbook_time']);
                    $order['begin_time']     = $data['takebook_time'];
                    $order['back_time']      = $data['backbook_time'];
                    $order['content']        = $data['pro_text'].','.'取书时间：'.date('Y-m-d H:i:s',$data['takebook_time']).','.'最晚还书时间：'.date('Y-m-d H:i:s',$data['backbook_time']);

                }elseif($data['book_status'] == 3){

                    $order['proprice']       = $data['proprice'];
                    $data['arrive_time']     = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['arrive_time']);
                    $order['receivingtime']  = $data['arrive_time'];
                    $order['content']        = $data['pro_text'].','.'送达时间：'.date('Y-m-d H:i:s',$data['arrive_time']);
                }

            }elseif($table_type == 8){

                $order['table_type']    = $request->param('table_type');
                $order['money_status']  = $data['shopping_status'];
                $data['collect_time']   = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['collect_time']);
                $order['content']       = $data['pro_text'].','.'最晚取钱时间：'.date('Y-m-d H:i:s',$data['collect_time']);

                if ($data['shopping_status'] == 2) {
                    $order['content']   = $data['pro_text'].','.'最晚存钱时间：'.date('Y-m-d H:i:s',$data['collect_time']);
                }
                $order['collect_time']  = $data['collect_time'];

            }elseif($table_type == 6 || $table_type == 7 || $table_type == 9){

                $order['table_type']         = $request->param('table_type');
                $order['proprice']           = $data['proprice'];
                $data['arrive_time']         = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['arrive_time']);
                $order['receivingtime']      = $data['arrive_time'];
                $order['content']            = $data['pro_text'].','.'送达时间：'.date('Y-m-d H:i:s',$data['arrive_time']);

                //1:旅行代购  2:其它代购 3:化妆品代购 4:租借需求 5:帮我办事
                $order['shopping_status']    = $data['shopping_status'];

            }
            $res = Db::name($table_name)->insertGetId($data);

        }elseif($pro_status == 2){
            if ($table_type == 1){

                $order['table_type'] = $request->param('table_type');
                $order['arrive_pos'] = $data['arrive_pos'];
                $order['back_pos']   = $data['back_pos'];
                $data['back_time']   = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['back_time']);
                $order['back_time']  = $data['back_time'];
                $order['title']      = $data['arrive_pos'].' → '.$data['back_pos'];
                $order['content']    = '带购地点：'.$data['arrive_pos'].','.'返回地点：'.$data['back_pos'].','.'返回时间：'.date('Y-m-d H:i:s',$data['back_time']);

            }elseif($table_type == 2){

                $order['table_type'] = $request->param('table_type');
                $order['begin_pos']  = $data['begin_pos'];
                $order['arrive_pos'] = $data['arrive_pos'];
                $data['trip_time']   = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['trip_time']);
                $order['trip_time']  = $data['trip_time'];
                $order['trip_num']   = $data['trip_num'];
                $order['isComback']  = $data['isComback'];
                $order['title']      = $data['begin_pos'].' → '.$data['arrive_pos'];
                $order['content']    = '出发地点：'.$data['begin_pos'].','.'到达地点：'.$data['arrive_pos'].','.'出行时间：'.date('Y-m-d H:i:s',$data['trip_time']).','.'人数：'.$data['trip_num'];

                if ($order['isComback']  == 1){
                    $data['back_time']  = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['back_time']);
                    $order['back_time'] = $data['back_time'];
                    $order['content']   = '出发地点：'.$data['begin_pos'].','.'到达地点：'.$data['arrive_pos'].','.'出行时间：'.date('Y-m-d H:i:s',$data['trip_time']).','.'返回时间：'.date('Y-m-d H:i:s',$data['back_time']).','.'人数：'.$data['trip_num'];
                }

            }elseif($table_type == 3){

                $order['table_type']     = $request->param('table_type');
                $order['express_status'] = $data['express_status'];
                if ( $data['express_status'] == 1){

                    $order['arrive_pos'] = $data['arrive_pos'];
                    $order['back_pos']   = $data['back_pos'];
                    $data['back_time']   = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['back_time']);
                    $order['back_time']  = $data['back_time'];
                    $order['title']      = $data['arrive_pos'].' → '.$data['back_pos'];
                    $order['content']    = '到达地点：'.$data['arrive_pos'].','.'返回地点：'.$data['back_pos'].','.'返回时间：'.date('Y-m-d H:i:s',$data['back_time']);

                }else{

                    $order['begin_pos']  = $data['begin_pos'];
                    $order['arrive_pos'] = $data['arrive_pos'];
                    $data['begin_time']  = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['begin_time']);
                    $order['begin_time'] = $data['begin_time'];
                    $order['title']      = $data['begin_pos'].' → '.$data['arrive_pos'];
                    $order['content']    = '出发地点：'.$data['begin_pos'].','.'到达地点：'.$data['arrive_pos'].','.'出发时间：'.date('Y-m-d H:i:s',$data['begin_time']);

                }
            }elseif($table_type == 4){

                $order['table_type'] = $request->param('table_type');
                $order['arrive_pos'] = $data['arrive_pos'];
                $order['back_pos']   = $data['back_pos'];
                $data['back_time']   = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['back_time']);
                $order['back_time']  = $data['back_time'];
                $order['title']      = $data['arrive_pos'].' → '.$data['back_pos'];
                $order['content']    = '带购地点：'.$data['arrive_pos'].','.'返回地点：'.$data['back_pos'].','.'返回时间：'.date('Y-m-d H:i:s',$data['back_time']);

            }elseif($table_type == 5){

                $order['table_type']  = $request->param('table_type');
                $order['book_status'] = $data['book_status'];
                if ($data['book_status'] == 1){

                    $order['arrive_pos'] = $data['arrive_pos'];
                    $order['back_pos']   = $data['back_pos'];
                    $data['back_time']   = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['back_time']);
                    $order['back_time']  = $data['back_time'];
                    $order['title']      = $data['arrive_pos'].' → '.$data['back_pos'];
                    $order['content']    = '借书地点：'.$data['arrive_pos'].','.'返回地点：'.$data['back_pos'].','.'返回时间：'.date('Y-m-d H:i:s',$data['back_time']);

                }elseif($data['book_status'] == 2){

                    $order['begin_pos']  = $data['begin_pos'];
                    $order['arrive_pos'] = $data['arrive_pos'];
                    $data['begin_time']  = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['begin_time']);
                    $order['begin_time'] = $data['begin_time'];
                    $order['title']      = $data['begin_pos'].' → '.$data['arrive_pos'];
                    $order['content']    = '出发地点：'.$data['begin_pos'].','.'还书地点：'.$data['arrive_pos'].','.'出发时间：'.date('Y-m-d H:i:s',$data['begin_time']);

                }else{

                    $order['arrive_pos'] = $data['arrive_pos'];
                    $order['back_pos']   = $data['back_pos'];
                    $data['begin_time']  = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['begin_time']);
                    $order['begin_time'] = $data['begin_time'];
                    $order['title']      = $data['arrive_pos'].' → '.$data['back_pos'];
                    $order['content']    = '带购地点：'.$data['arrive_pos'].','.'返回地点：'.$data['back_pos'].','.'送书时间：'.date('Y-m-d H:i:s',$data['begin_time']);

                }
            }elseif($table_type == 8){

                $order['table_type']   = $request->param('table_type');
                $order['money_status'] = $data['shopping_status'];
                $order['proprice']     = $data['proprice'];
                $data['collect_time']  = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['collect_time']);
                $order['take_time']    = $data['collect_time'];
                $order['title']        = $data['collectpos'];
                $order['begin_pos']    = $data['collectpos'];
                $order['content']      = '交易地点：'.$data['collectpos'].','.'交易时间：'.date('Y-m-d H:i:s',$data['collect_time']);

            }elseif($table_type == 6 || $table_type == 7 || $table_type == 9){

                $order['table_type']      = $request->param('table_type');
                $order['shopping_status'] = $data['shopping_status'];
                $data['back_time']        = strtotime(strchr(date('Y-m-d H:i:s'),'-',true).'-'.$data['back_time']);
                $order['back_time']       = $data['back_time'];
                $order['title']           = $data['arrive_pos'].' → '.$data['back_pos'];
                $order['arrive_pos']      = $data['arrive_pos'];
                $order['back_pos']        = $data['back_pos'];
                $order['content']         = '带购地点：'.$data['arrive_pos'].','.'返回地点：'.$data['back_pos'].','.'返回时间：'.date('Y-m-d H:i:s',$data['back_time']);
            }
            $res = Db::name("$table_name")->insertGetId($data);
        }

        //要添加进订单表的信息,帮我带/帮你带，都要添加的
        $order['order_num']      = $this->create_order_no();
        $order['order_end_time'] = $data['order_end_time'];
        $order['pro_status']     = $data['pro_status'];
        $order['nickname']       = $data['nickname'];
        $order['mp3_len']        = $data['mp3_len'] ?? 0;
        $order['avatar']         = $data['avatar'];
        $order['uid']            = $data['uid'];
        $order['create_time']    = time();
        $order['name_type']      = 1;
        $order['status']         = 1;

        //如果存在pid，代表是通过接受帮你带订单跳转，下的帮我带订单,订单状态自动改为已接单,接收pid和order_id
        $pid = $request->param('pid');
        if ($pid !== 'undefined' && !empty($pid)){
            $order['pid']      = $pid;
            $order['status']   = 2;
            $order['order_id'] = $request->param('order_id');
        }

        $res2 = Db::name('order')->insertGetId($order);

        $id = $res.','.$res2;
        return $res && $res2 ? json(['status'=>1,'data'=>$id,'msg'=>'下单成功']) : json(['status'=>0,'data'=>'','msg'=>'下单失败']);
    }

    //帮带插入图片地址
    public function bdai2(Request $request){
        //type插入哪张表中,proid判断将图片和MP3插入哪条数据中
        $type  = $request->param('type');
        $img   = request()->file('img'); //接收图片
        $mp3   = request()->file('mp3'); //接收图片

        $idArg = explode(',',$request->param('proid'));
        $pid   = $idArg[0];
        $oid   = $idArg[1];

        if($img){
            //如果图片存在返回图片网络地址
            $imgUrl = $this->wxImg($img);

            //1美食饮品4超市代购5书店/图书馆7其他,插入图片地址
            $type == 1 && $table_name = 'food';
            $type == 4 && $table_name = 'market_shopping';
            $type == 5 && $table_name = 'bookstore';
            $type == 7 && $table_name = 'shopping';

            //对应表插入图片地址
            $isimg  = Db::name("$table_name")->where('id',$pid)->value("img");
            $imgids = $isimg ? $isimg.','.$imgUrl : $imgUrl;
            Db::name("$table_name")->where('id',$pid)->update(["img" => $imgids]);

            //订单表插入图片地址
            $isimg  = Db::name('order')->where('id',$oid)->value("img");
            $imgids = $isimg ? $isimg.','.$imgUrl : $imgUrl;
            Db::name('order')->where('id',$oid)->update(["img" => $imgids]);
        }

        if ($mp3){
            //如果语音存在返回语音网络地址
            $mp3Url = $this->wxMp3($mp3);

            //1美食饮品4超市代购5书店/图书馆7其他,插入图片地址
            $type == 1 && $table_name = 'food';
            $type == 2 && $table_name = 'trip';
            $type == 3 && $table_name = 'express';
            $type == 4 && $table_name = 'market_shopping';
            $type == 5 && $table_name = 'bookstore';
            $type == 6 && $table_name = 'money';
            $type == 7 && $table_name = 'shopping';

            //对应表插入语音地址
            $isMp3  = Db::name("$table_name")->where('id',$pid)->value("pro_mp3");
            $mp3ids = $isMp3 ? $isMp3.','.$mp3Url : $mp3Url;
            Db::name("$table_name")->where('id',$pid)->update(["pro_mp3" => $mp3ids]);

            //订单表插入语音地址
            $isMp3  = Db::name('order')->where('id',$oid)->value("mp3");
            $imgids = $isMp3 ? $isMp3.','.$mp3Url : $mp3Url;
            Db::name('order')->where('id',$oid)->update(["mp3" => $imgids]);
        }
    }

    //微信上传图片处理方法
    public function wxImg($file){
        $upload_dir = ROOT_PATH.'public/upload/img/';
        file_exists($upload_dir) || mkdir($upload_dir, 0755, true);

        $info = $file->rule('date')->move($upload_dir);
        return $info ? '/upload/img/'.$info->getSaveName() : false;
    }

    //微信mp3处理方法
    public function wxMp3($file2){
        $upload_dir = ROOT_PATH.'public/upload/mp3/';
        file_exists($upload_dir) || mkdir($upload_dir, 0755, true);

        $info = $file2->rule('date')->move($upload_dir);
        return $info ? '/upload/mp3/'.$info->getSaveName() : false;
    }

    //查询订单信息
    public function orderlist(Request $request){
        //1接受用户uid，订单状态name_type，1发布/2接受
        $name_stype = $request->param('name_type');
        $uid        = $request->param('uid');

        //2根据status状态查询订单0查所有1查未接单2查已接单3查已完成4查单个订单
        $status = $request->param('status');
        if ($status == 0){
            $where = [
                $name_stype == 1 ? 'uid' : 'pid' => $uid,
                'status' => array('in','1,2,3')
            ];

            $data = (new Order())->where($where)->select();//查所有

            //当查所有的时候，检测订单时间是否过期，如果过期，则将订单状态改成已取消
            foreach ($data as $k => $v){
                //如果存在订单自动关闭时间，则判断订单是否过期，如果过期且订单状态为未接单，则将订单状态改为已取消
                $v['order_end_time'] && $v['order_end_time'] <= time() && $v['status'] == '未接单' && Db::name('order')->where('id',$v['id'])->setField('status',4);
            }
            //数据有延迟，再查一遍数据库
            $data = (new Order())->where($where)->order('create_time desc')->select();//查所有

        }elseif($status == 1 || $status == 2 || $status == 3){
            $where = [$name_stype == 1 ? 'uid' : 'pid' => $uid];

            $status == 1 && $where['status'] = 1;
            $status == 2 && $where['status'] = 2;
            $status == 3 && $where['status'] = 3;

            $data = (new Order())->where($where)->order('create_time desc')->select();//1查未接单2查已接单3查已完成

        }elseif($status == 4){
            //如果是查询单个订单，则只接受订单的主键ID进行查询即可
            $id = $request->param('id');
            $order = (new Order())->find($id);

            //将img,mp3前面，都加上网站域名
            if ($order['img']){
                $img = explode(',','https://a.kissneck.com/hybd/public'.$order['img']);
                $order['img'] = $img[0];
            }else{
                $order['img'] = 'https://a.kissneck.com/hybd/public/upload/img/mr_img.jpg';
            }

            if ($order['mp3']){
                $mp3 = explode(',','https://a.kissneck.com/hybd/public'.$order['mp3']);
                $order['mp3'] = $mp3[0];
            }else{
                $order['mp3'] = 0;
            }

            //将时间转成YmdHis格式显示
            $order['shiptime']       = $order['shiptime'] ? date('Y-m-d H:i:s',$order['shiptime']) : '';
            $order['finishtime']     = $order['finishtime'] ? date('Y-m-d H:i:s',$order['finishtime']) : '';
            $order['receivingtime']  = $order['receivingtime'] ? date('Y-m-d H:i:s',$order['receivingtime']) : '';
            $order['order_end_time'] = $order['order_end_time'] ? date('Y-m-d H:i:s',$order['order_end_time']) : '';
            $order['back_time']      = $order['back_time'] ? date('Y-m-d H:i:s',$order['back_time']) : '';
            $order['begin_time']     = $order['begin_time'] ? date('Y-m-d H:i:s',$order['begin_time']) : '';
            $order['collect_time']   = $order['collect_time'] ? date('Y-m-d H:i:s',$order['collect_time']) : '';
            $order['trip_time']      = $order['trip_time'] ? date('Y-m-d H:i:s',$order['trip_time']) : '';
            $order['take_time']      = $order['take_time'] ? date('Y-m-d H:i:s',$order['take_time']) : '';

            //如果订单关闭时间小于当前时间，代表订单已经过期(只针对订单状态为未接单的情况，才会显示订单已过期)
            $guoqi = Db::name('order')->where('id',$id)->value('status');

            //将content变成数组,传给前端，前端进行迭代，换行显示
            $order['content'] = explode(',',$order['content']);

            if ($order['order_end_time'] <= time() && $guoqi == '未接单'){
                //如果订单关闭时间已到，而且订单是未接单状态，将订单状态改为已取消
                Db::name('order')->where('id',$id)->setField('status',4);
                return json(['status' => 0, 'data' => '', 'msg' => '订单已过期']);
            }
            //福豆比率
            if ($order['pro_status'] == 1){
                $order['bean_rate'] = Db::name('option')->where('id',9)->value('meta_value');
            }

            return $order ? json(['status'=>1,'data'=>$order,'msg'=>'查询订单信息成功']) : json(['status'=>0,'data'=>'','msg'=>'查询订单信息失败']);
        }

        //给img地址加上域名
        if ($data){
            foreach ($data as $k=>$v){
                if ($v['img']){
                    $v['img'] = explode(',','https://a.kissneck.com/hybd/public'.$v['img']);
                    $v['img'] = $v['img'][0];
                }else{
                    $v['img'] = 'https://a.kissneck.com/hybd/public/upload/img/mr_img.jpg';
                }
            }
        }

        return $data ? json(['status'=>1,'data'=>$data,'msg'=>'查询订单信息成功']) : json(['status'=>0,'data'=>'','msg'=>'查询订单信息失败']);
    }

    //生成唯一订单号
    public function create_order_no() {
        $order_no = substr(date('YmdHis'),2).rand(10000, 99999);
        return $order_no;
    }

    //帮带接单
    public function receive(Request $request){
        //接收订单主键ID,uid
        $id  = $request->param('id');
        $uid = $request->param('uid');

        if (empty($uid) || $uid == 'undefined'){
            return json(['status'=>0,'data'=>'','msg'=>'请登录后下单!']);
        }

        //不能接自己发布的订单
        $u_id = Db::name('order')->where('id',$id)->value('uid');
        if ($u_id == $uid){
            return json(['status'=>0,'data'=>'','msg'=>'不能接自己的单哟!']);
        }

        //接单人接单复制下单人的数据
        $add = Db::name('order')->find($id);

        //如果已接单/订单已完成/订单已取消，不能再接单
        if ($add['status'] == 2 || $add['status'] == 3 || $add['status'] == 4){
            return json(['status'=>1,'data'=>'','msg'=>'订单状态不可接!']);
        }

        //更改订单状态为已接单，将pid改成接单人的uid,插入下单时间
        $update = [
            'pid'      => $uid,
            'status'   => 2,
            'shiptime' => time(),
        ];

        //如果pro_status=2,代表是帮你带跳转的帮我带订单
        $add['pro_status'] == 2 && $update['status'] = 3;

        $res = Db::name('order')->where('id',$id)->update($update);

        return $res ? json(['status'=>1,'data'=>$res,'msg'=>'接单成功']) : json(['status'=>0,'data'=>'','msg'=>'接单失败']);
    }

    //订单状态更改
    public function orderChange(Request $request){
        //订单状态status：1未接单,2已接单，3已完成，4已取消
        $id     = $request->param('id');//接受订单主键ID
        $status = $request->param('status');//订单更改成什么状态

        //判断订单状态，如果已取消或已完成，则订单状态不可更改
        $status_old = Db::name('order')->where('id',$id)->value('status');
        if ($status_old == 3 || $status_old == 4){
            return json(['status'=>0,'data'=>'','msg'=>'订单已完成或已取消!']);
        }

        if ($status == 3){
            //确认收货
            $update = [
                'status'=>3,
                'finishtime'=>time()
            ];
            //下单人的订单状态改为已完成
            Db::name('order')->where('id',$id)->update($update);

            //减少下单人的福豆，生成福豆记录
            $msg = Db::name('order')->where('id',$id)->field('uid,pid,bean')->find();

            //减少下单人福豆
            $beanOld = Db::name('member')->where('id',$msg['uid'])->value('bean');
            $beanNew = $beanOld - $msg['bean'];
            Db::name('member')->where('id',$msg['uid'])->setField('bean',$beanNew);

            //生成下单人的福豆记录
            $add = [
                'uid'        => $msg['uid'],
                'd_type'     => 1,
                'source'     => 3,
                'd_money'    => $msg['bean'],
                'createtime' => time()
            ];
            Db::name('member_bean_detail')->insert($add);

            //增加下单人的福豆，生成福豆记录
            $oldBean = Db::name('member')->where('id',$msg['pid'])->value('bean');
            $newBean = $oldBean + $msg['bean'];
            Db::name('member')->where('id',$msg['pid'])->setField('bean',$newBean);

            //生成接单人福豆记录
            $add2 = [
                'uid'        => $msg['pid'],
                'd_type'     => 2,
                'source'     => 2,
                'd_money'    => $msg['bean'],
                'createtime' => time()
            ];

            $res = Db::name('member_bean_detail')->insert($add2);

        }else if($status == 4){
            $order_id = $request->param('order_id');
            //取消订单
            $update = [
                'status'=>4,
                'finishtime'=>time(),
            ];
            if ($order_id){
                $title = Db::name('order')->where('id',$id)->value('title').'(已拒接)';
                Db::name('order')->where('id',$id)->setField('title',$title);
            }

            $res = Db::name('order')->where('id',$id)->update($update);

            //如果存在order_id,代表是通过帮你带下的帮我带订单，取消的时候，还要把之前的帮你带订单状态改为未接单
            if($order_id){
                $update = [
                    'status' => 1,
                    'pid'    => null
                ];
                Db::name('order')->where('id',$order_id)->update($update);
            }
        }

        return $res ? json(['status'=>1,'data'=>'','msg'=>'更改订单状态成功']) : json(['status'=>0,'data'=>'','msg'=>'更改订单状态失败']);
    }

    //兑现福豆1/充值福豆2
    public function bean(Request $request){
        $data       = $request->param();
        $beanStatus = $data['status'];//福豆进/出
        $beanChange = $data['bean'];//变化福豆数量
        $uid        = $data['uid'];//用户
        $beanOld    = Db::name('member')->where('id',$uid)->value('bean');

        //1兑现福豆2充值福豆
        $bean = $beanStatus == 1 ? $beanOld - $beanChange : $beanChange + $beanOld;
        $res  = Db::name('member')->where('id',$uid)->setField('bean',$bean);

        //添加福豆收/支记录
        $addLog = [
            'uid'        => $uid,
            'd_type'     => $beanStatus,
            'd_money'    => $beanChange,
            'createtime' => time()
        ];
        $log = Db::name('member_bean_detail')->insert($addLog);

        return $res && $log ? json(['status'=>1,'data'=>'','msg'=>'充值兑现成功']) : json(['status'=>0,'data'=>'','msg'=>'充值兑现失败']);
    }

    //地址相关操作
    public function locationList(Request $request){
        //1查询地址信息2更改地址为默认状态3地址编辑4删除地址5新增地址6查询单个地址信息
        $type = $request->param('type');
        $uid  = $request->param('uid');

        if ($type == 1) {
            $data   = Db::name('member_location')->where('uid',$uid)->select();
        }else if($type == 2){
            $uid    = $request->param('uid');
            $id     = $request->param('id');
            Db::name('member_location')->where('uid',$uid)->setField('is_default',0);
            $data   = Db::name('member_location')->where('id',$id)->setField('is_default',1);
        }else if($type == 3){
            $where  = ['id'=>$request->param('id')];
            $updata = $request->except('id,type,uid');
            $data   = Db::name('member_location')->where($where)->update($updata);
        }else if($type == 4){
            $where  = ['id' => $request->param('id')];
            $data   = Db::name('member_location')->where($where)->delete();
        }else if($type == 5){
            $data   = $request->except('type');
            $data   = Db::name('member_location')->insertGetId($data);
        }else if($type == 6){
            $id     = $request->param('id');
            $data   = Db::name('member_location')->where('id',$id)->find();
        }

        return $data ? json(['status'=>1,'data'=>$data,'msg'=>'操作成功']) : json(['status'=>0,'data'=>'','msg'=>'操作失败']);
    }

    //清除自己的订单信息
    public function deleteMe(Request $request){
        $uid   = $request->param('uid');
        $table = ['food','trip','express','market_shopping','bookstore','money','shopping','member_bean_detail','order'];

        foreach($table as $k => $v){
            $res = Db::name("$v")->where('uid',$uid)->delete();
        }

        Db::name('order')->where('pid',$uid)->delete();
        return $res ? json(['status'=>1,'data'=>'','msg'=>'清除成功']) : json(['status'=>0,'data'=>'','msg'=>'清除失败']);
    }

    public function cs(){
        $isdir = str_replace('Api.php','',__FILE__) . 'cert/';
        $dir = $isdir . 'apiclient_cert.pem';
        if (file_exists($dir)){
            echo $dir;
        }else{
            echo 0;
        }
    }
}




