<?php
namespace app\api\exception\alipayaop;
use app\api\exception\alipayaop\AopClient;
use app\api\exception\alipayaop\AlipayFundTransToaccountTransferRequest;

class AlipayAop{
  private $appId; 
  private $rsaPrivateKey; 
  private $alipayrsaPublicKey; 
  function __construct(){ 
      $this->appId = '2016033001252670';
	  $this->rsaPrivateKey = 'MIIEpAIBAAKCAQEA4aaYTRZ9dII93nx/WlCE9QyucwuJvc93TMIHKXD7lHl9QdXv9d++teMbCNjIxV7pzvc7lwAvL5qB7MBmCaTRrTnMl2izQmct+fCJGJ9l0HkOLn17gobVn8avJEe26lFpwp7Yq90CS6s93MvfWLWzpvfeCVZsZYIZamMvpnYYySxJlHvWbRSYDwfFhtrYsFhMdcU82fJyJivqMQv4DiZEjNdvItu60/YmrTx3R3S58++homIF8+/uAw2n/oHmog6ertMy3IzyPbUrxKgv+jPl/oQSbv07s5EHWVBj+qkII+90cYXEbw7xvCl6LCHIpi0b1rY/2+U8DIR3gTQIYT7wFQIDAQABAoIBABvWZpGUBbQa4TESYM+ugUiN4POWZ86/ynLwuNt28cJ2XmqBzxGhIbDkrJAh1lrrA6Mhwvs07Z1MzVx8u9nSuH9LUx8Wro6xle1wrCkaeQAH/yTjHBqVMcN9SlwKcNllY8u7tvU01bSqEn6rtAkImXout/ik7ch0DVifleBOT255yd84fqLptVKO7pIJ94X4vlQWArqb4H0opaNQgUD2nV6BZRoUBf5oFGdZPgsJc7FlsrA1bF0ZyUaEeBODjjr9yyuLM233HRlwsj1AebBZPSyPS14uE317iPBJy6EEnZ6XaRyyG/1ZfZZieI6hf/k5nK5DbKKU27MeOktqIADfwWECgYEA+QIqf+sBMforyalQneIcPrzKk0pvA9M3MZz3M0d6bXT+3T/3sGMoN78dhNDQwZG11ox7sUAaPCeRVbp6f/W/gvs8P3mWB7RnmgHr2DxudH1Odg6VQOuEdOivFGkWWi0cK7YBg/fRZ5WytocXSocJOHjxSuInhDRzLYagQrsGF40CgYEA5/yJk7NtqgXn8teaE0qZxK4bmSvBA3VWmXxshafd48lT6XPn7uE1MboLps9w7+cS4EV8jnC0BkNJc2sEt78FfDuq/p8z/j7F/wBs5pTNBq2eWzKrfy5KPUVCau6Ppr0Btlkxtv/qtbENtNKqmKH8uSml1bkx8jt+U0R/ltGj9KkCgYEAk9muZG+kMQmK8F1hKa20+addXOFoU0wZTJMX0FHTpqQ+JB60Mf5eReNrYo4eNaEP6AlX9GHMcgVIbS+hRtMi2GHWXxYReBetICud3rHbH727VavJAVgWONhkBGl1RSvduzKBads+chPXnjtOIAlX9zkQhFcOAZim/tQd2iF0N40CgYASx013qEmTPsEk1jEBqMNDuXhAcNtB5WiqKfk65HjArJZfSmOW4aIFsXKqARa6RLExeE551UOtUhsK/bm3Cf5urQHNdKOny/sJBkbKb3cdOEaeST66n8ugpA+ZSlBUhe4yjYvJrXOZW4ipf3gRvOMRNf7XixMw8YHrSZsuYS/taQKBgQDU9s7EPFW6t3G7MAuVBJmAXedDQogaWQo0gM0cAFOvrnJKOUkSjZAOvRdKMJ3IVh93gbIr5T5tCy91UAuZbv04lizwmY50jMTXyIIJnIwoAvRaxaMdc4ZPp0Dv4w/Ns+X5F0L8zrOnk0GE1WGL6mTVRF7ECIJ+ud6n4WZZWTAWZw==';
	  $this->alipayrsaPublicKey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzJ+DtUZnBnF6jJbWvxuEKpatoQmXiLvrBJVnbscAQb36tAATNVDy9+JSN03VbU8bCYjsXZh9YR6LTI6q13zYe3T1ZlOeqPkU+NqdX6U4QjfMDUWsBs9q9qFEDyvcppFNjeb8mAhDNdJZKKLa+kDLTi+4Sh1iXpH3Zn5d8XNgFxCSfFTWpvhZvHgC6WVwVD261DOqR7aXHkjI6fKfu6rx66nuAD7OPxgFHtiuPDypS6txtRlqTigf7JKsMB09FpR1mUDFB2a2LfB9KNnSl4aXL3CUNUxOmSt3ypO9XOVgjDdPju3VStMvYBv0J4rxwZHFwIWBjy9I2gmoDc1fJ19niQIDAQAB';
  }
  /*
  *该方法为支付宝转账方法
  *osn 交易单号
  *payee_account 收款人帐号
  *amount 转账金额
  *payee_real_name 收款方真实姓名
  */
  public function AlipayFundTransToaccountTransfer($order_no,$payee_account,$amount,$payee_real_name = ""){
	    $msg = array();
        $aop = new AopClient();
        $aop->gatewayUrl         = 'https://openapi.alipay.com/gateway.do';
        $aop->appId              = $this->appId;
        $aop->rsaPrivateKey      = $this->rsaPrivateKey;
        $aop->alipayrsaPublicKey = $this->alipayrsaPublicKey;
        $aop->apiVersion         = '1.0';
        $aop->signType           = 'RSA2';
        $aop->postCharset        = 'UTF-8';
        $aop->format             = 'json';
        $request = new AlipayFundTransToaccountTransferRequest();
        $request->setBizContent("{" .
        "\"out_biz_no\":\"".$order_no."\"," .
        "\"payee_type\":\"ALIPAY_LOGONID\"," .
        "\"payee_account\":\"".$payee_account."\"," .
        "\"amount\":\"".$amount."\"," .
        "\"payer_show_name\":\"可待科技\"," .
        "\"payee_real_name\":\"".$payee_real_name."\"," .
        "\"remark\":\"可以付平台账户提现通知\"" .
        "  }");
        $result = $aop->execute ( $request);
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if(!empty($resultCode)&&$resultCode == 10000){
            $msg['success']  = 1;
            $msg['pay_date'] = $result->$responseNode->pay_date;
            $msg['order_id'] = $result->$responseNode->order_id;
            return $msg;
        }else{
            $resultMsg  = $result->$responseNode->sub_msg;
            $msg['success'] = 2;
            $msg['text']    = $resultMsg;
            return $msg;
        }
  }
}
?>