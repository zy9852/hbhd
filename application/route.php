<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------


//Route::rule(':version/user','api/:version.User/info');
//Route::rule(':version/user/list','api/:version.User/list');

//
return [
    //全局变量定义
    '__pattern__' => [
        'name' => '\w+',
    ],

    //后台部分

    'a/user_list' => ['admin/user/index', ['ext' => 'html']], //管理员列表页面
    'a/cust_list' => ['admin/customers/index', ['ext' => 'html']], //用户列表页面
    'a/u_address' => ['admin/customers/location', ['ext' => 'html']], //用户地址详情

    'a/changeln' => ['admin/shoplist/changeLn', ['ext' => 'html']],  //管家列表

    'a/shoplist' => ['admin/shoplist/index', ['ext' => 'html']],  //商家列表
    'a/shopupdate' => ['admin/shoplist/shopupdate', ['ext' => 'html']],  //商家查看&更新

    //登录
    'a/login' => ['admin/login/index', ['ext' => 'html']],// 登录页面
    'a/index' => ['admin/index/index', ['ext' => 'html']], //后台首页
    'a/user_list' => ['admin/user/index', ['ext' => 'html']], //管理员列表页面

    'a/photo' => ['admin/system/photo', ['ext' => 'html']], //上传banner
    'a/bean' => ['admin/system/bean', ['ext' => 'html']],   //设置福豆
    'a/beandetail' => ['admin/system/beandetail', ['ext' => 'html']], //用户福豆详情
    'a/applybean' => ['admin/system/applybean', ['ext' => 'html']], //用户福豆提现申请
    'a/settag' => ['admin/system/settag', ['ext' => 'html']], //标签设置

    //帮我带商品
    'a/prolist' => ['admin/probangdai/index', ['ext' => 'html']],
    //帮你带商品
    'a/uprolist' => ['admin/proubangdai/index', ['ext' => 'html']],
    //帮我带订单
    'a/orderlist' => ['admin/orderlist/index', ['ext' => 'html']],
    //编辑.查看订单
    'a/editord' => ['admin/Orderlist/editord', ['ext' => 'html']],

];
