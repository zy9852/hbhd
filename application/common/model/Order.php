<?php
/**
 * Created by PhpStorm.
 * User: xl
 * Date: 2019/1/19
 * Time: 18:11
 */

namespace app\common\model;


use think\Model;

class Order extends Model
{
    public function getStatusAttr($value){
        $status = [1=>'未接单',2=>'已接单',3=>'已完成',4=>'已取消'];
        return $status[$value];
    }
}