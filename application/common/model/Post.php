<?php

namespace app\common\model;

use think\Model;

class Post extends Model
{
	public function thumb()
    {
        return $this->hasOne('Image','pid');
    }

}
