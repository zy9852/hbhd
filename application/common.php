<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.kissneck.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 可待科技-磊子 <sxfyxl@126.com>
// +----------------------------------------------------------------------

// 应用公共文件

use think\Request;
use think\Db;
use phpmailer\phpmailer;
use think\Session;
/**
 * 检测用户是否登录
 * @author 可待科技 <sxfyxl@126.com>
 */

function is_login()
{
	$uid = Session('uid');
	if ($uid == null) {
		return 0;
	} else {
		return 1;
	}
}

//rand password
function generateRandomString($length = 10)
{
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}

//打印数据
function p($attr)
{
	echo '<pre>';
	print_r($attr);
	echo '</pre>';
}

//密码HASH
function do_hash($psw)
{
	$salt = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";  //定义一个salt值，最好够长，或者随机
	return sha1(md5($psw . $salt)); //返回加salt后的散列
}

//获取当前用户信息
function get_current_user_info()
{
	$request = Request::instance();
	$user_token = $request->cookie('user_auth');
	$data = Db::name('member')->where('login_auth', $user_token)->find();
	return $data;
}

//获取当前用户ID
function get_current_user_id()
{
	$request = Request::instance();
	$user_token = $request->cookie('user_auth');
	$data = Db::name('member')->where('login_auth', $user_token)->find();
	return $data['id'];
}

//邮件发送
function get_send_email($body, $subject, $toemail)
{
	$body = $body;
	$mail = new PHPMailer();
	$mail->ClearAllRecipients();
	$mail->CharSet = "UTF-8";
	$mail->IsSMTP();
	$mail->SMTPAuth = true;
	$mail->Port = 25;
	$mail->Host = "smtp.126.com";
	$mail->Username = "sxfyxl@126.com";
	$mail->Password = "19870822@tt";
	$mail->SetFrom("sxfyxl@126.com", "可待科技系统邮件");
	$mail->AddAddress($toemail, "");
	$mail->Subject = $subject;
	$mail->MsgHTML($body);
	$rel = $mail->Send();
	if ($rel) {
		return true;
	} else {
		return false;
	}
}

function makeRequest($url, $params = array(), $expire = 0, $extend = array(), $hostIp = '')
{
	if (empty($url)) {
		return array('code' => '100');
	}

	$_curl = curl_init();
	$_header = array(
		'Accept-Language: zh-CN',
		'Connection: Keep-Alive',
		'Cache-Control: no-cache'
	);
	// 方便直接访问要设置host的地址
	if (!empty($hostIp)) {
		$urlInfo = parse_url($url);
		if (empty($urlInfo['host'])) {
			$urlInfo['host'] = substr(DOMAIN, 7, -1);
			$url = "http://{$hostIp}{$url}";
		} else {
			$url = str_replace($urlInfo['host'], $hostIp, $url);
		}
		$_header[] = "Host: {$urlInfo['host']}";
	}

	// 只要第二个参数传了值之后，就是POST的
	if (!empty($params)) {
		curl_setopt($_curl, CURLOPT_POSTFIELDS, http_build_query($params));
		curl_setopt($_curl, CURLOPT_POST, true);
	}

	if (substr($url, 0, 8) == 'https://') {
		curl_setopt($_curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($_curl, CURLOPT_SSL_VERIFYHOST, false);
	}
	curl_setopt($_curl, CURLOPT_URL, $url);
	curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($_curl, CURLOPT_USERAGENT, 'API PHP CURL');
	curl_setopt($_curl, CURLOPT_HTTPHEADER, $_header);

	if ($expire > 0) {
		curl_setopt($_curl, CURLOPT_TIMEOUT, $expire); // 处理超时时间
		curl_setopt($_curl, CURLOPT_CONNECTTIMEOUT, $expire); // 建立连接超时时间
	}

	// 额外的配置
	if (!empty($extend)) {
		curl_setopt_array($_curl, $extend);
	}

	$result['result'] = curl_exec($_curl);
	$result['code'] = curl_getinfo($_curl, CURLINFO_HTTP_CODE);
	$result['info'] = curl_getinfo($_curl);
	if ($result['result'] === false) {
		$result['result'] = curl_error($_curl);
		$result['code'] = -curl_errno($_curl);
	}

	curl_close($_curl);

	return $result;
}

function api_notice_increment($url, $data)
{
	$ch = curl_init();
	$header = "Accept-Charset: utf-8";
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	@curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$tmpInfo = curl_exec($ch);
	if (curl_errno($ch)) {
		return false;
	} else {
		return $tmpInfo;
	}
}


	// @param  int $uid 用户id
	// @param  int $uid 套餐id
	// @param  int $rid 推荐人id,若没有则为空
	// @param  int $transaction_id 订单号


function guard_api($data, $apiurl, $acsurl = "https://jd3.kissneck.com")
{
	$data = http_build_query($data);

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch, CURLOPT_POST, 1);

	curl_setopt($ch, CURLOPT_URL, $apiurl);

	curl_setopt($ch, CURLOPT_REFERER, $acsurl);

	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	$response = curl_exec($ch);

	return $response;

	curl_close($ch);
}

function get_trim_words($text, $num_words = 55, $more = null)
{
	if (null === $more) {
		$more = ('&hellip;');
	}

	/*
	 * translators: If your word count is based on single characters (e.g. East Asian characters),
	 * enter 'characters_excluding_spaces' or 'characters_including_spaces'. Otherwise, enter 'words'.
	 * Do not translate into your own language.
	 */
	if (preg_match('/^utf\-?8$/i', 'utf-8')) {
		$text = trim(preg_replace("/[\n\r\t ]+/", ' ', $text), ' ');
		preg_match_all('/./u', $text, $words_array);
		$words_array = array_slice($words_array[0], 0, $num_words + 1);
		$sep = '';
	} else {
		$words_array = preg_split("/[\n\r\t ]+/", $text, $num_words + 1, PREG_SPLIT_NO_EMPTY);
		$sep = ' ';
	}

	if (count($words_array) > $num_words) {
		array_pop($words_array);
		$text = implode($sep, $words_array);
		$text = $text . $more;
	} else {
		$text = implode($sep, $words_array);
	}

	return $text;
}
// 获取正式表
function get_checktable()
{
	$general = Db::name('general');
	$value = $general->where('option_key', 'temptable')->value('option_val');
	return $value;
}

//保存图片(新增部分)
if (!function_exists('base64_to_img')) {
	function base64_to_img($data, $file = 'images')
	{
		$image_arr = explode(',', $data);
		$img_data = $image_arr[1];
		$img = str_replace(' ', '+', $img_data);
		$fileData = base64_decode($img);
		$image_name = uniqid() . '.png';
		$upload_dir = ROOT_PATH . 'public/upload/' . $file . '/';
		if (!file_exists($upload_dir)) {
			mkdir($upload_dir, 0755, true);
		}
		$filedir = $upload_dir . '/' . $image_name;
		$src = '/upload/' . $file . '/' . $image_name;
		$status = file_put_contents($filedir, $fileData);
		if ($status) {
			return $src;
		} else {
			return 0;
		}
	}
}

//获取服务器ip地址
if (!function_exists('getIp')) {
	function getIp()
	{
		if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
			$cip = $_SERVER["HTTP_CLIENT_IP"];
		} else if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
			$cip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		} else if (!empty($_SERVER["REMOTE_ADDR"])) {
			$cip = $_SERVER["REMOTE_ADDR"];
		} else {
			$cip = '';
		}
		preg_match("/[\d\.]{7,15}/", $cip, $cips);
		$cip = isset($cips[0]) ? $cips[0] : 'unknown';
		unset($cips);

		return $cip;
	}
}
    //获取区域
function getRegion($s, $c, $a)  //省.市.区
{
	$region = Db::name('area')->where('s', $s)->where('c', $c)->where('a', $a)->value('l');
	if ($region) {
		return $region;
	} else {
		return "/";
	}
}

function get_fee_Status($v)
{
	switch ($v) {
		case '1':
			return '取快递';
			break;
		case '2':
			return '寄快递';
			break;
	}
}


function get_book_Status($v)
{
	switch ($v) {
		case '1':
			return '借书';
			break;
		case '2':
			return '还书';
			break;
		case '3':
			return '买书';
			break;
	}
}

function get_money_Status($v)
{
	switch ($v) {
		case '1':
			return '取钱';
			break;
		case '2':
			return '存钱';
			break;
	}
}

/* 订单状态 */
function get_order_Status($v)
{
	switch ($v) {
		case '1':
			return '未接单';
			break;
		case '2':
			return '已接单';
			break;
		case '3':
			return '已完成';
			break;
		case '4':
			return '已取消';
			break;
	}
}

