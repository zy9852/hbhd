<?php
date_default_timezone_set( 'Asia/Shanghai' );
error_reporting(E_ERROR);

require_once "../lib/WxPay.Api.php";
require_once '../lib/WxPay.Notify.php';
require_once 'log.php';
//require_once '../../../thinkphp/library/think/Loader.php';
use think\Controller;
use think\Request;
use think\Db;
//初始化日志
$logHandler= new CLogFileHandler("../logs/".date('Y-m-d').'.log');
$log = Log::Init($logHandler, 15);


$save = ['uid'=>11,'fee'=>22,'transaction_id'=>315463,'status'=>0];

$a = Db::table('y_recharge_log')->insert($save);echo $a;exit;

class PayNotifyCallBack extends WxPayNotify
{
	//查询订单
	public function Queryorder($transaction_id)
	{
		$input = new WxPayOrderQuery();
		$input->SetTransaction_id($transaction_id);
		$result = WxPayApi::orderQuery($input);
		//Log::DEBUG("query:" . json_encode($result));
		if(array_key_exists("return_code", $result)
			&& array_key_exists("result_code", $result)
			&& $result["return_code"] == "SUCCESS"
			&& $result["result_code"] == "SUCCESS")
		{
			return true;
		}
		return false;
	}
	
	//重写回调处理函数
	public function NotifyProcess($data, &$msg)
	{	
		date_default_timezone_set( 'Asia/Shanghai' );
		//Log::DEBUG("call back:" . json_encode($data));

		$notfiyOutput = array();
		if(!array_key_exists("transaction_id", $data)){
			$msg = "输入参数不正确";
			return false;
		}
		//查询订单，判断订单真实性
		if(!$this->Queryorder($data["transaction_id"])){
			$msg = "订单查询失败";
			return false;
		}
		if(array_key_exists("return_code", $data)
			&& array_key_exists("result_code", $data)
			&& $data["return_code"] == "SUCCESS"
			&& $data["result_code"] == "SUCCESS")
		{	
			$arg   = explode('_',$data["attach"]);

			$uid   = $arg[0];

			$tid   = $arg[1];

			$rid   = $arg[2];

			if($tid=='null'){

				$recharge = "null";

			}else{

				$recharge = Db::table('y_plan')->where('id',$tid)->find();
			}

			if($recharge!=='null'){
		
				$save = ['uid'=>$uid,'fee'=>$recharge['get_fee'],'transaction_id'=>$data['transaction_id'],'tid'=>$rid,'status'=>0];

			}else{
			
				$save = ['uid'=>$uid,'fee'=>$data['total_fee'],'transaction_id'=>$data['transaction_id'],'status'=>0];

			}

			$log = Db::table('y_recharge_log')->insertGetId($save);

			if($log){

				$user = Db::table('y_customers')->where('id',$uid)->find();
				
				$fee  = $user['total_fee'];

				if($recharge!=='null'){

					$is_activity = $recharge['is_activity'];
					
					$add_fee     = $recharge['get_fee'];

					if($is_activity==1){

						$user_data = ['total_fee'=>$fee+$add_fee,'is_new'=>0];

						Db::table('y_customers')->where('id',$uid)->update($user_data);

					}else{

						$user_data = ['total_fee'=>$fee+$add_fee,'is_vip'=>1];

						Db::table('y_customers')->where('id',$uid)->update($user_data);
					}

			}else{

				$add_fee   = $recharge['get_fee'];
				
				$user_data = ['total_fee'=>$fee+$add_fee];

				Db::table('y_customers')->where('id',$uid)->update($user_data);

			}
				Log::DEBUG("callback:" .$data["attach"].'ppppppp'.$status.'==================='.date("Y-m-d H:i:s",time()).'--------------');

				Db::table('y_recharge_log')->where('id',$log)->update(['status'=>1]);
			}

			

			//$res = pay_notify($uid,$tid,$rid,$data['transaction_id']);

		}
		return true;
	}
}

//Log::DEBUG("123123");
$notify = new PayNotifyCallBack();
$notify->Handle(false);
